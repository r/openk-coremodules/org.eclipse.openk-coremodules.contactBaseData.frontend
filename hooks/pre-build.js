/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
var fs = require('fs-extra');
var jsonConcat = require('json-concat');

var localizationSourceFilesDE = [
  "./i18n/general.de.json",
  "./i18n/components.de.json",
  "./i18n/contacts.de.json",
  "./i18n/persons.de.json",
  "./i18n/salutations.de.json",
  "./i18n/company.de.json",
  "./i18n/admin.de.json",
  "./i18n/addresses.de.json",
  "./i18n/communications-data.de.json",
  "./i18n/user-module-assignment.de.json",
  "./i18n/logout.de.json"
];

function mergeAndSaveJsonFiles(src, dest) {
  jsonConcat({ src: src, dest: dest }, function(res) {
    console.log('Localization files successfully merged!');
  });
}

function setEnvironment(configPath, environment) {
  fs.writeJson(configPath, { env: environment }, function(res) {
    console.log('Environment variable set to ' + environment);
  });
}

// Set environment variable to "production"
setEnvironment('./config/env.json', 'production');

// Merge all localization files into one
mergeAndSaveJsonFiles(localizationSourceFilesDE, './i18n/de.json');
