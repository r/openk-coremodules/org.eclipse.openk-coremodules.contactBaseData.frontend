/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { InternalPersonDetailsSandBox } from '@pages/persons/internal-person/internal-person-details/internal-person-details.sandbox';
import { Component, OnInit, Input } from '@angular/core';
import { AddressTypesSandbox } from '@pages/admin/address-types/address-types.sandbox';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionsSubject } from '@ngrx/store';
import * as internalPersonActions from '@shared/store/actions/persons/internal-person.action';
import { ofType } from '@ngrx/effects';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-internal-person-address-details',
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.scss'],
})
export class InternalPersonAddressDetailsComponent implements OnInit {
  @Input() createOrEditAddressData: string;

  constructor(
    public internalPersonDetailsSandbox: InternalPersonDetailsSandBox,
    public addressTypesSandbox: AddressTypesSandbox,
    private modalService: NgbModal,
    private actionsSubject: ActionsSubject
  ) {}

  ngOnInit() {
    this.internalPersonDetailsSandbox.registerAddressEvents();
    this.internalPersonDetailsSandbox.newAddressData();
  }

  showDialog(): void {
    if (
      this.internalPersonDetailsSandbox.addressDetailsCurrentFormState.value.isMainAddress &&
      this.internalPersonDetailsSandbox.existMainAddress &&
      !this.internalPersonDetailsSandbox.isCurrentAddressMainAddress
    ) {
      const modalRef = this.modalService.open(SafetyQueryDialogComponent);
      modalRef.componentInstance.title = 'ConfirmDialog.Action.changeMainAddress';
      modalRef.componentInstance.body = 'ConfirmDialog.MainAddressModification';
      modalRef.result.then(
        () => {
          this.actionsSubject.pipe(ofType(internalPersonActions.persistAddressDetailSuccess), take(1)).subscribe(() => {
            this.internalPersonDetailsSandbox.persistAddress();
          });
          this.internalPersonDetailsSandbox.removeCurrentMainAddress();
        },
        () => {}
      );
    } else {
      this.internalPersonDetailsSandbox.persistAddress();
    }
  }
}
