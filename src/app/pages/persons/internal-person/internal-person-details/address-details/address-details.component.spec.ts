/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { InternalPersonAddressDetailsComponent } from '@pages/persons/internal-person/internal-person-details/address-details/address-details.component';
import { of } from 'rxjs';

describe('AddressDetailsComponent', () => {
  let component: InternalPersonAddressDetailsComponent;
  let internalPersonSandbox: any;
  let addressTypesSandbox: any;
  let modalService: any;
  let actionsSubject: any;

  beforeEach(async(() => {
    internalPersonSandbox = {
      registerAddressEvents() {},
      registerExternalPersonEvents() {},
      endSubscriptions() {},
      newAddressData() {},
      persistAddress() {},
      addressDetailsCurrentFormState: { value: {} },
      existMainAddress: null,
      isCurrentAddressMainAddress: null,
    } as any;

    modalService = { open() {} } as any;

    addressTypesSandbox = {};
  }));

  beforeEach(() => {
    component = new InternalPersonAddressDetailsComponent(internalPersonSandbox, addressTypesSandbox, modalService, actionsSubject);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call registerAddressEvents and newAddressData onInit', () => {
    const spy1 = spyOn(internalPersonSandbox, 'registerAddressEvents');
    const spy2 = spyOn(internalPersonSandbox, 'newAddressData');
    component.ngOnInit();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });

  it('check if showDialog() works', () => {
    const spy1 = spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    internalPersonSandbox.addressDetailsCurrentFormState = { value: { isMainAddress: true } };
    internalPersonSandbox.existMainAddress = true;
    internalPersonSandbox.isCurrentAddressMainAddress = true;
    component.showDialog();
    expect(spy1).not.toHaveBeenCalled();

    internalPersonSandbox.isCurrentAddressMainAddress = false;
    component.showDialog();
    expect(spy1).toHaveBeenCalled();
  });

  it('should open modal before persist an address', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    internalPersonSandbox.addressDetailsCurrentFormState.value.isMainAddress = true;
    internalPersonSandbox.existMainAddress = true;
    internalPersonSandbox.isCurrentAddressMainAddress = false;

    component.showDialog();
    expect(modalService.open).toHaveBeenCalled();
  });
});
