/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { InternalPersonCommunicationsDataListComponent } from '@pages/persons/internal-person/internal-person-details/communications-data-list/communications-data-list.component';
import { of } from 'rxjs/observable/of';

describe('InternalPersonCommunicationsDataListComponent', () => {
  let component: InternalPersonCommunicationsDataListComponent;
  let internalPersonSandbox: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi =  { setDomLayout() {} }
    internalPersonSandbox = {
      registerCommunicationsDataEvents() {},
      registerInternalPersonEvents() {},
      endSubscriptions() {},
      deleteCommunicationsData() {},
      newCommunicationsData() {},
    } as any;
    internalPersonSandbox.communicationsAgApi = agApi;
  }));

  beforeEach(() => {
    component = new InternalPersonCommunicationsDataListComponent(internalPersonSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define gridOptions onInit', () => {
    component.ngOnInit();

    expect(component.gridOptions).toBeDefined();
    expect(component.gridOptions.context).toBeDefined();
  });

  it('should emit if BusEvents is edit ', () => {
    const spy = spyOn(component.internalPersonDetailsId, 'emit');
    const event: any = { type: 'edit', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should deleteCommunicationsData if BusEvents is delete', () => {
    const spy = spyOn(internalPersonSandbox, 'deleteCommunicationsData');
    const event: any = { type: 'delete', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should newCommunicationsData and close edit area if callcreateNewCommunicationsDataForm', () => {
    const spy = spyOn(internalPersonSandbox, 'newCommunicationsData');
    const spyEmit = spyOn(component.createNewInternalPerson, 'emit');
    component.createNewCommunicationsDataForm();

    expect(spy).toHaveBeenCalled();
    expect(spyEmit).toHaveBeenCalled();
  });
});
