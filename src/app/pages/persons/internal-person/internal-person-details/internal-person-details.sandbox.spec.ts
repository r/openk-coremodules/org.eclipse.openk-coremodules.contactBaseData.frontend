/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { InternalPersonDetailsSandBox } from '@pages/persons/internal-person/internal-person-details/internal-person-details.sandbox';
import { of } from 'rxjs';
import * as internalPersonActions from '@shared/store/actions/persons/internal-person.action';
import { InternalPerson, CommunicationsData, Address, LdapUser } from '@shared/models';

describe('InternalPersonDetailsSandBox', () => {
  let component: InternalPersonDetailsSandBox;
  let utilService: any;
  let appState: any;
  let actionSubject: any;
  let router: any;
  let modalService: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi =  { setDomLayout() {} }
    router = { navigateByUrl() {} } as any;
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionSubject = { pipe: () => of([]) } as any;
    utilService = { displayNotification: () => {} } as any;
    modalService = { open() {} } as any;
  }));

  beforeEach(() => {
    component = new InternalPersonDetailsSandBox(appState, utilService, actionSubject, router, modalService);
    component.addressAgApi = agApi;
    component.communicationsAgApi = agApi;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dispatch if load an internal person', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadInternalPerson('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: internalPersonActions.loadInternalPersonDetail.type }));
  });

  it('should call dispatch 5 times if persist an existing internal person with unique uid and no userref', () => {
    const spy = spyOn(appState, 'dispatch');
    const routerSpy = spyOn(router, 'navigateByUrl');
    const notificationSpy = spyOn(utilService, 'displayNotification');
    const actionSubjectSpy = spyOn(actionSubject, 'pipe').and.callThrough();

    const internalPerson = new InternalPerson();
    // its in edit mode because an existing id
    internalPerson.contactId = 'id';
    internalPerson.lastName = 'test';
    internalPerson.uid = 'uid';
    internalPerson.userRef = undefined;
    component.internalPersonDetailsCurrentFormState = { ...component.internalPersonDetailsCurrentFormState, isValid: true, value: internalPerson };
    component.persistInternalPerson();
    expect(spy).toHaveBeenCalledTimes(6);
    expect(routerSpy).toHaveBeenCalled();
    expect(notificationSpy).toHaveBeenCalledTimes(0);
    expect(actionSubjectSpy).toHaveBeenCalledTimes(3);
  });

  it('should call dispatch 4 times if persist an existing internal person with unique userref and no uid', () => {
    const spy = spyOn(appState, 'dispatch');
    const routerSpy = spyOn(router, 'navigateByUrl');
    const notificationSpy = spyOn(utilService, 'displayNotification');
    const actionSubjectSpy = spyOn(actionSubject, 'pipe').and.callThrough();

    const internalPerson = new InternalPerson();
    // its in edit mode because an existing id
    internalPerson.contactId = 'id';
    internalPerson.lastName = 'test';
    internalPerson.uid = undefined;
    internalPerson.userRef = 'userref';
    component.internalPersonDetailsCurrentFormState = { ...component.internalPersonDetailsCurrentFormState, isValid: true, value: internalPerson };
    component.persistInternalPerson();
    expect(spy).toHaveBeenCalledTimes(5);
    expect(routerSpy).toHaveBeenCalled();
    expect(notificationSpy).toHaveBeenCalledTimes(0);
    expect(actionSubjectSpy).toHaveBeenCalledTimes(3);
  });

  it('should call error with displayNotification if for not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.internalPersonDetailsCurrentFormState = { ...component.internalPersonDetailsCurrentFormState, isValid: false };
    component.persistInternalPerson();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should can register events and set the currentFormState', () => {
    component.registerInternalPersonEvents();
    expect(component.internalPersonDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if load an internal person communications Data', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadCommunicationsData('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: internalPersonActions.loadInternalPersonDetailCommunicationsData.type }));
  });

  it('should call dispatch if load an internal person communicationsData details', () => {
    const spy = spyOn(appState, 'dispatch');
    component.internalPersonContactId = 'contactId';
    component.loadCommunicationsDataDetails('ID');
    expect(spy).toHaveBeenCalledWith(
      Object({
        payload_contactId: 'contactId',
        payload_communicationsId: 'ID',
        type: internalPersonActions.loadInternalPersonDetailCommunicationsDataDetails.type,
      })
    );
  });

  it('should can register communicationsData events and set the communicationsDataDetailsCurrentFormState', () => {
    component.registerCommunicationsDataEvents();
    expect(component.communicationsDataDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if newInternalPerson', () => {
    const spy = spyOn(appState, 'dispatch');
    component.newInternalPerson();
    expect(spy).toHaveBeenCalledTimes(3);
  });

  it('should call dispatch if clearCommunicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearCommunicationsData();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if persist an internal person communicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearCommunicationsData');
    const internalPersonCommunicationsData = new CommunicationsData();
    internalPersonCommunicationsData.communicationTypeType = 'test';
    internalPersonCommunicationsData.contactId = 'id';

    component.communicationsDataDetailsCurrentFormState = {
      ...component.communicationsDataDetailsCurrentFormState,
      isValid: true,
      value: internalPersonCommunicationsData,
    };
    component.persistCommunicationsData();
    expect(spy).toHaveBeenCalledWith(Object({ payload: internalPersonCommunicationsData, type: internalPersonActions.persistCommunicationsDataDetail.type }));
    expect(spy1).toHaveBeenCalled();
  });

  it('should call error with displayNotification if communicationsData form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.communicationsDataDetailsCurrentFormState = { ...component.communicationsDataDetailsCurrentFormState, isValid: false };
    component.persistCommunicationsData();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call clearCommunicationsData and negate isCommunicationsDataDetailViewVisible after call closeCommunicationsDataDetail', () => {
    const spy = spyOn(component, 'clearCommunicationsData');
    component.isCommunicationsDataDetailViewVisible = true;
    component.closeCommunicationsDataDetail();
    expect(spy).toHaveBeenCalled();
    expect(component.isCommunicationsDataDetailViewVisible).toBe(false);
  });

  it('should open modal before deleting an communicationsData', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.deleteCommunicationsData(new CommunicationsData());
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should call dispatch if load an internal person addresses', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadInternalPersonAddresses('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: internalPersonActions.loadInternalPersonDetailAddresses.type }));
  });

  it('should call dispatch if load an internal person address details', () => {
    const spy = spyOn(appState, 'dispatch');
    component.internalPersonContactId = 'contactId';
    component.loadInternalPersonDetailsAddressDetails('ID');
    expect(spy).toHaveBeenCalledWith(
      Object({ payload_contactId: 'contactId', payload_addressId: 'ID', type: internalPersonActions.loadInternalPersonDetailAddressDetails.type })
    );
  });

  it('should can register address events and set the addressDetailsCurrentFormState', () => {
    component.registerAddressEvents();
    expect(component.addressDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if clearAddressData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearAddressData();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if persist an internal person address', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearAddressData');
    const internalPersonAddress = new Address();
    internalPersonAddress.addressTypeType = 'test';
    internalPersonAddress.contactId = 'id';

    component.addressDetailsCurrentFormState = { ...component.addressDetailsCurrentFormState, isValid: true, value: internalPersonAddress };
    component.persistAddress();
    expect(spy).toHaveBeenCalledWith(Object({ payload: internalPersonAddress, type: internalPersonActions.persistAddressDetail.type }));
    expect(spy1).toHaveBeenCalled();
  });

  it('should call error with displayNotification if address form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.addressDetailsCurrentFormState = { ...component.addressDetailsCurrentFormState, isValid: false };
    component.persistAddress();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call clearAddressData and negate isDetailViewVisible after call closeAddressDataDetail', () => {
    const spy = spyOn(component, 'clearAddressData');
    component.isAddressDataDetailViewVisible = true;
    component.closeAddressDataDetail();
    expect(spy).toHaveBeenCalled();
    expect(component.isAddressDataDetailViewVisible).toBe(false);
  });

  it('should open modal before deleting an address', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.deleteAddress(new Address());
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should go in for-loop and set property "existMainAddress"', () => {
    let address: Address = {
      id: '',
      contactId: '',
      isMainAddress: false,
      addressTypeId: '',
      addressTypeType: '',
      addressTypeDescription: '',
      postcode: '',
      mainAddress: '',
      community: '',
      communitySuffix: '',
      street: '',
      housenumber: '',
      wgs84Zone: '',
      latitude: '',
      longitude: '',
      urlMap: '',
      note: '',
    };
    const addresses: Array<Address> = [address];
    (component as any)._checkIfMainAddressExist(addresses);
    expect(component.existMainAddress).toBeFalsy();

    address.isMainAddress = true;
    addresses.push(address);
    (component as any)._checkIfMainAddressExist(addresses);
    expect(component.existMainAddress).toBeTruthy();
  });

  it('should call clear and markastouched if newAddressData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.newAddressData();
    expect(spy).toHaveBeenCalledTimes(3);
    expect(component.isCurrentAddressMainAddress).toBeFalsy();
  });

  it('should call clear and markastouched if newCommunicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.newCommunicationsData();
    expect(spy).toHaveBeenCalledTimes(3);
  });

  it('should call dispatch if clearInternalPerson', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearInternalPerson();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if loadKeycloakUsers', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadKeycloakUsers();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(Object({ type: '[Users] Load KeycloakUsers' }));
  });

  it('should call dispatch and clear the userref if setUserRefValue called with null', () => {
    component.internalPersonDetailsCurrentFormState = {
      controls: { userRef: { id: 'test' } },
      value: { userRef: 'test1' },
    } as any;
    const spy = spyOn(appState, 'dispatch').and.callThrough();
    component.setUserRefValue(null);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should call dispatch and set the userref if setUserRefValue called with a value', () => {
    component.internalPersonDetailsCurrentFormState = {
      controls: { userRef: { id: 'test' } },
      value: { userRef: 'test1' },
    } as any;
    const spy = spyOn(appState, 'dispatch').and.callThrough();
    component.setUserRefValue({ item: 'Muster Administrator (admin)' } as any);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should call dispatch if loadLdapUsers', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadLdapUsers();
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(Object({ type: '[Users] Load LdapUsers' }));
  });

  it('should call dispatch and clear the userref if setLdapUidValue called with null', () => {
    component.internalPersonDetailsCurrentFormState = {
      controls: { uid: { id: 'test1' }, firstName: { id: 'test2' }, lastName: { id: 'test3' } , isSyncError: { id: 'test4' }, syncNote: { id: 'test5' }},
      value: { uid: 'test1', firstName: 'test2', lastName: 'test3' },
    } as any;
    const spy = spyOn(appState, 'dispatch').and.callThrough();
    component.setLdapUidValue(null);
    expect(spy).toHaveBeenCalledTimes(5);
  });

  it('should call dispatch and set the userref if setLdapUidValue called with a value', () => {
    component.internalPersonDetailsCurrentFormState = {
      controls: { uid: { id: 'test1' }, firstName: { id: 'test2' }, lastName: { id: 'test3' } , isSyncError: { id: 'test4' }, syncNote: { id: 'test5' }},
      value: { uid: 'test1', firstName: 'test2', lastName: 'test3' },
    } as any;
    let ldapUser = { firstName: 'Muster', lastName: 'Administrator', uid: 'admin' };
    const spy = spyOn(appState, 'dispatch').and.callThrough();
    component.setLdapUidValue({ item: ldapUser });
    expect(spy).toHaveBeenCalledTimes(5);
  });
});
