/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { InternalPersonDetailsSandBox } from '@pages/persons/internal-person/internal-person-details/internal-person-details.sandbox';
import { Component, OnInit, Input } from '@angular/core';
import { CommunicationTypesSandbox } from '@pages/admin/communication-types/communication-types.sandbox';

@Component({
  selector: 'app-internal-person-communications-data-details',
  templateUrl: './communications-data-details.component.html',
  styleUrls: ['./communications-data-details.component.scss']
})
export class InternalPersonCommunicationsDataDetailsComponent implements OnInit {

  @Input() createOrEditCommunicationsData: string;

  constructor(
    public internalPersonDetailsSandbox: InternalPersonDetailsSandBox,
    public communicationTypesSandbox: CommunicationTypesSandbox
  ) { }

  ngOnInit() {
    this.internalPersonDetailsSandbox.registerCommunicationsDataEvents();
    this.internalPersonDetailsSandbox.clearCommunicationsData();
  }

}
