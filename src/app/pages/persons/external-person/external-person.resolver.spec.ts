/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ExternalPersonResolver } from '@pages/persons/external-person/external-person.resolver';

describe('ExternalPersonResolver', () => {
  let component: ExternalPersonResolver;
  let externalPersonSandbox: any;
  let userModuleAssignmentSandbox: any;
  let salutationsSandbox: any;
  let communicationTypesSandbox: any;
  let personTypesSandbox: any;
  let addressTypesSandbox: any;

  beforeEach(async(() => {
    externalPersonSandbox = {
      newExternalPerson() {},
      loadExternalPerson() {},
      loadExternalPersonAddresses() {},
      loadCommunicationsData() {},
    } as any;

    userModuleAssignmentSandbox = {
      loadFilteredUserModuleTypes() {},
      loadUserModuleAssignments() {},
    } as any;

    salutationsSandbox = {
      loadSalutations() {},
    } as any;

    personTypesSandbox = {
      loadPersonTypes() {},
    } as any;

    communicationTypesSandbox = {
      loadCommunicationTypes() {},
    } as any;

    addressTypesSandbox = {
      loadAddressTypes() {},
    } as any;
  }));

  beforeEach(() => {
    component = new ExternalPersonResolver(
      externalPersonSandbox,
      salutationsSandbox,
      userModuleAssignmentSandbox,
      communicationTypesSandbox,
      personTypesSandbox,
      addressTypesSandbox
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call loadExternalPerson, loadExternalPersonAddresses, loadCommunicationsData, loadFilteredUserModuleTypes if edit a external person', () => {
    const spy = spyOn(externalPersonSandbox, 'loadExternalPerson');
    const spy1 = spyOn(externalPersonSandbox, 'loadExternalPersonAddresses');
    const spy2 = spyOn(externalPersonSandbox, 'loadCommunicationsData');
    const spy3 = spyOn(userModuleAssignmentSandbox, 'loadFilteredUserModuleTypes');
    let ar: any = { params: { contactId: 'ID' } };
    component.resolve(ar);
    expect(spy).toHaveBeenCalled();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
    expect(spy3).toHaveBeenCalled();
  });

  it('should call newExternalPerson if create a new external person', () => {
    const spy = spyOn(externalPersonSandbox, 'newExternalPerson');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);
    expect(spy).toHaveBeenCalled();
  });

  it('should loadSalutations on resolve', () => {
    const spy = spyOn(salutationsSandbox, 'loadSalutations');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should loadCommunicationTypes on resolve', () => {
    const spy = spyOn(communicationTypesSandbox, 'loadCommunicationTypes');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should set isCommunicationsDataDetailViewVisible true on resolve', () => {
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(externalPersonSandbox.isCommunicationsDataDetailViewVisible).toBeFalsy();
  });

  it('should loadPersonTypes on resolve', () => {
    const spy = spyOn(personTypesSandbox, 'loadPersonTypes');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });

  it('should loadAddressTypes on resolve', () => {
    const spy = spyOn(addressTypesSandbox, 'loadAddressTypes');
    let ar: any = { params: { contactId: undefined } };
    component.resolve(ar);

    expect(spy).toHaveBeenCalled();
  });
});
