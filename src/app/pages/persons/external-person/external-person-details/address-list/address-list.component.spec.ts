/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ExternalPersonAddressListComponent } from '@pages/persons/external-person/external-person-details/address-list/address-list.component';
import { of } from 'rxjs/observable/of';

describe('AddressListComponent', () => {
  let component: ExternalPersonAddressListComponent;
  let externalPersonSandbox: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi =  { setDomLayout() {} }
    externalPersonSandbox = {
      registerAddressEvents() {},
      registerExternalPersonEvents() {},
      endSubscriptions() {},
      deleteAddress() {},
      clearAddressData() {},
    } as any;
    externalPersonSandbox.addressAgApi = agApi;
  }));

  beforeEach(() => {
    component = new ExternalPersonAddressListComponent(externalPersonSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define gridOptions onInit', () => {
    component.ngOnInit();

    expect(component.gridOptions).toBeDefined();
    expect(component.gridOptions.context).toBeDefined();
  });

  it('should emit if BusEvents is edit ', () => {
    const spy = spyOn(component.externalPersonDetailsIdLoaded, 'emit');
    const event: any = {type: 'edit', data: {id: 'id'}};
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should deleteAddress if BusEvents is delete', () => {
    const spy = spyOn(externalPersonSandbox, 'deleteAddress');
    const event: any = {type: 'delete', data: {id: 'id'}};
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should clearAddress and close edit area if callcreateNewAddressForm', () => {
    const spy = spyOn(externalPersonSandbox, 'clearAddressData');
    const spyEmit = spyOn(component.createNewExternalPerson, 'emit');
    component.createNewAddressForm();

    expect(spy).toHaveBeenCalled();
    expect(spyEmit).toHaveBeenCalled();
  });
});

