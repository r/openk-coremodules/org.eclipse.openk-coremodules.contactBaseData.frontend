/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ExternalPersonDetailsSandBox } from '@pages/persons/external-person/external-person-details/external-person-details.sandbox';
import { of } from 'rxjs';
import * as externalPersonActions from '@shared/store/actions/persons/external-person.action';
import { ExternalPerson, Address, CommunicationsData } from '@shared/models';

describe('ExternalPersonDetailsSandBox', () => {
  let component: ExternalPersonDetailsSandBox;
  let utilService: any;
  let appState: any;
  let actionSubject: any;
  let router: any;
  let modalService: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi =  { setDomLayout() {} }
    router = { navigateByUrl() {} } as any;
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionSubject = { pipe: () => of(true) } as any;
    utilService = { displayNotification: () => {} } as any;
    modalService = { open() {} } as any;
  }));

  beforeEach(() => {
    component = new ExternalPersonDetailsSandBox(appState, utilService, actionSubject, router, modalService);
    component.addressAgApi = agApi;
    component.communicationsAgApi = agApi;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dispatch if load an external person', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadExternalPerson('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: externalPersonActions.loadExternalPersonDetail.type }));
  });

  it('should call dispatch if persist an external person', () => {
    const spy = spyOn(appState, 'dispatch');
    const routerSpy = spyOn(router, 'navigateByUrl');
    const externalPerson = new ExternalPerson();
    externalPerson.lastName = 'test';
    component.externPersonDetailsCurrentFormState = { ...component.externPersonDetailsCurrentFormState, isValid: true, value: externalPerson };
    component.persistExternalPerson();
    expect(spy).toHaveBeenCalledWith(Object({ payload: externalPerson, type: externalPersonActions.persistExternalPersonDetail.type }));
    expect(routerSpy).toHaveBeenCalled();
  });

  it('should call error with displayNotification if form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.externPersonDetailsCurrentFormState = { ...component.externPersonDetailsCurrentFormState, isValid: false };
    component.persistExternalPerson();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should can register events and set the currentFormState', () => {
    component.registerExternalPersonEvents();
    expect(component.externPersonDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if load an external person addresses', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadExternalPersonAddresses('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: externalPersonActions.loadExternalPersonDetailAddresses.type }));
  });

  it('should call dispatch if load an external person address details', () => {
    const spy = spyOn(appState, 'dispatch');
    component.externalPersonContactId = 'contactId';
    component.loadExternalPersonDetailsAddressDetails('ID');
    expect(spy).toHaveBeenCalledWith(
      Object({ payload_contactId: 'contactId', payload_addressId: 'ID', type: externalPersonActions.loadExternalPersonDetailAddressDetails.type })
    );
  });

  it('should can register address events and set the addressDetailsCurrentFormState', () => {
    component.registerAddressEvents();
    expect(component.addressDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if newExternalPerson', () => {
    const spy = spyOn(appState, 'dispatch');
    component.newExternalPerson();
    expect(spy).toHaveBeenCalledTimes(3);
  });

  it('should call dispatch if clearAddressData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearAddressData();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if persist an external person address', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearAddressData');
    const externalPersonAddress = new Address();
    externalPersonAddress.addressTypeType = 'test';
    externalPersonAddress.contactId = 'id';

    component.addressDetailsCurrentFormState = { ...component.addressDetailsCurrentFormState, isValid: true, value: externalPersonAddress };
    component.persistAddress();
    expect(spy).toHaveBeenCalledWith(Object({ payload: externalPersonAddress, type: externalPersonActions.persistAddressDetail.type }));
    expect(spy1).toHaveBeenCalled();
  });

  it('should call error with displayNotification if address form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.addressDetailsCurrentFormState = { ...component.addressDetailsCurrentFormState, isValid: false };
    component.persistAddress();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call clearAddressData and negate isDetailViewVisible after call closeAddressDataDetail', () => {
    const spy = spyOn(component, 'clearAddressData');
    component.isAddressDataDetailViewVisible = true;
    component.closeAddressDataDetail();
    expect(spy).toHaveBeenCalled();
    expect(component.isAddressDataDetailViewVisible).toBe(false);
  });

  it('should open modal before deleting an address', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.deleteAddress(new Address());
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should call dispatch if load an external person communications Data', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadCommunicationsData('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: externalPersonActions.loadExternalPersonDetailCommunicationsData.type }));
  });

  it('should call dispatch if load an external person communicationsData details', () => {
    const spy = spyOn(appState, 'dispatch');
    component.externalPersonContactId = 'contactId';
    component.loadCommunicationsDataDetails('ID');
    expect(spy).toHaveBeenCalledWith(
      Object({
        payload_contactId: 'contactId',
        payload_communicationsId: 'ID',
        type: externalPersonActions.loadExternalPersonDetailCommunicationsDataDetails.type,
      })
    );
  });

  it('should can register communicationsData events and set the communicationsDataDetailsCurrentFormState', () => {
    component.registerCommunicationsDataEvents();
    expect(component.communicationsDataDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if clearCommunicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearCommunicationsData();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if persist an external person communicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearCommunicationsData');
    const externalPersonCommunicationsData = new CommunicationsData();
    externalPersonCommunicationsData.communicationTypeType = 'test';
    externalPersonCommunicationsData.contactId = 'id';

    component.communicationsDataDetailsCurrentFormState = {
      ...component.communicationsDataDetailsCurrentFormState,
      isValid: true,
      value: externalPersonCommunicationsData,
    };
    component.persistCommunicationsData();
    expect(spy).toHaveBeenCalledWith(Object({ payload: externalPersonCommunicationsData, type: externalPersonActions.persistCommunicationsDataDetail.type }));
    expect(spy1).toHaveBeenCalled();
  });

  it('should call error with displayNotification if communicationsData form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.communicationsDataDetailsCurrentFormState = { ...component.communicationsDataDetailsCurrentFormState, isValid: false };
    component.persistCommunicationsData();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call clearCommunicationsData and negate isCommunicationsDataDetailViewVisible after call closeCommunicationsDataDetail', () => {
    const spy = spyOn(component, 'clearCommunicationsData');
    const spy1 = spyOn(component, 'loadCommunicationsData');
    component.isCommunicationsDataDetailViewVisible = true;
    component.closeCommunicationsDataDetail();
    expect(spy).toHaveBeenCalled();
    expect(spy1).toHaveBeenCalled();
    expect(component.isCommunicationsDataDetailViewVisible).toBe(false);
  });

  it('should open modal before deleting an communicationsData', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.deleteCommunicationsData(new CommunicationsData());
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should call clear and markastouched if newAddressData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.newAddressData();
    expect(spy).toHaveBeenCalledTimes(3);
    expect(component.isCurrentAddressMainAddress).toBeFalsy();
  });

  it('should call clear and markastouched if newCommunicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.newCommunicationsData();
    expect(spy).toHaveBeenCalledTimes(3);
  });

  it('should call dispatch if clearExternalPerson', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearExternalPerson();
    expect(spy).toHaveBeenCalledTimes(2);
  });
});
