/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ExternalPersonDetailsSandBox } from '@pages/persons/external-person/external-person-details/external-person-details.sandbox';
import { Component, OnInit, Input } from '@angular/core';
import { AddressTypesSandbox } from '@pages/admin/address-types/address-types.sandbox';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionsSubject } from '@ngrx/store';
import * as externalPersonActions from '@shared/store/actions/persons/external-person.action';
import { ofType } from '@ngrx/effects';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-external-person-address-details',
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.scss'],
})
export class ExternalPersonAddressDetailsComponent implements OnInit {
  @Input() createOrEditAddressData: string;

  constructor(
    public externalPersonDetailsSandbox: ExternalPersonDetailsSandBox,
    public addressTypesSandbox: AddressTypesSandbox,
    private modalService: NgbModal,
    private actionsSubject: ActionsSubject
  ) {}

  ngOnInit() {
    this.externalPersonDetailsSandbox.registerAddressEvents();
    this.externalPersonDetailsSandbox.newAddressData();
  }

  showDialog(): void {
    if (
      this.externalPersonDetailsSandbox.addressDetailsCurrentFormState.value.isMainAddress &&
      this.externalPersonDetailsSandbox.existMainAddress &&
      !this.externalPersonDetailsSandbox.isCurrentAddressMainAddress
    ) {
      const modalRef = this.modalService.open(SafetyQueryDialogComponent);
      modalRef.componentInstance.title = 'ConfirmDialog.Action.changeMainAddress';
      modalRef.componentInstance.body = 'ConfirmDialog.MainAddressModification';
      modalRef.result.then(
        () => {
          this.actionsSubject.pipe(ofType(externalPersonActions.persistAddressDetailSuccess), take(1)).subscribe(() => {
            this.externalPersonDetailsSandbox.persistAddress();
          });
          this.externalPersonDetailsSandbox.removeCurrentMainAddress();
        },
        () => {}
      );
    } else {
      this.externalPersonDetailsSandbox.persistAddress();
    }
  }
}
