/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ExternalPersonDetailsSandBox } from '@pages/persons/external-person/external-person-details/external-person-details.sandbox';
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PersonTypesSandbox } from '@pages/admin/person-types/person-types.sandbox';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
import { UserModuleAssignment } from '@shared/models';

@Component({
  selector: 'app-external-person-details',
  templateUrl: './external-person-details.component.html',
  styleUrls: ['./external-person-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ExternalPersonDetailsComponent implements OnInit, OnDestroy {
  public createOrEditAddressData = '';
  public createOrEditUserModuleAssignmentData = '';
  public createOrEditCommunicationsData = '';
  public isExpandableVisible = false;

  constructor(
    public externalPersonSandBox: ExternalPersonDetailsSandBox,
    public userModuleAssignmentSandBox: UserModuleAssignmentSandBox,
    public salutationSandBox: SalutationsSandbox,
    public personTypesSandbox: PersonTypesSandbox,
    private _translate: TranslateService
  ) {}

  ngOnInit() {
    this.externalPersonSandBox.registerExternalPersonEvents();
    this._initExpandableState();
  }

  ngOnDestroy() {
    this.externalPersonSandBox.endSubscriptions();
    this.externalPersonSandBox.externalPersonContactId = null;
  }

  public loadAddressDetail(detailId: string) {
    if (detailId == null) {
      this.createOrEditAddressData = this._translate.instant('Address.NewAddress');
    } else {
      this.createOrEditAddressData = this._translate.instant('Address.EditAddress');
      this.externalPersonSandBox.loadExternalPersonDetailsAddressDetails(detailId);
    }

    this.externalPersonSandBox.isAddressDataDetailViewVisible = true;
  }

  public loadCommunicationsDataDetail(detailId: string) {
    if (detailId == null) {
      this.createOrEditCommunicationsData = this._translate.instant('CommunicationsData.NewCommunicationsData');
    } else {
      this.createOrEditCommunicationsData = this._translate.instant('CommunicationsData.EditCommunicationsData');
      this.externalPersonSandBox.loadCommunicationsDataDetails(detailId);
    }
    this.externalPersonSandBox.isCommunicationsDataDetailViewVisible = true;
  }

  public loadUserModuleAssignmentDataDetail(userModuleAssignment: UserModuleAssignment) {
    if (userModuleAssignment == null) {
      this.createOrEditUserModuleAssignmentData = this._translate.instant('UserModuleAssignment.NewUserModuleAssignment');
    } else {
      this.createOrEditUserModuleAssignmentData = this._translate.instant('UserModuleAssignment.EditUserModuleAssignment');
      this.userModuleAssignmentSandBox.loadUserModuleAssignmentDetails(userModuleAssignment);
    }
    this.userModuleAssignmentSandBox.isUserModuleAssignmentDataDetailViewVisible = true;
  }

  private _initExpandableState() {
    this.isExpandableVisible = !!this.externalPersonSandBox.externalPersonContactId;

    this.externalPersonSandBox.isAddressDataDetailViewVisible = false;
    this.externalPersonSandBox.isCommunicationsDataDetailViewVisible = false;
  }
}
