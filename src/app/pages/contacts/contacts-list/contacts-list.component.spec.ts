import { ColumnApi } from 'ag-grid-community';
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ContactsListComponent } from '@pages/contacts/contacts-list/contacts-list.component';
import { Globals } from '@shared/constants/globals';
import { ModifiedContacts } from '@shared/models/modifiedContacts.model';

describe('ContactsListComponent', () => {
  let component: ContactsListComponent;
  let contactsSandbox: any = {};
  let userModuleAssignmentSandbox: any = {};
  let router: any = {};
  let ngZone: any = {};

  beforeEach(() => {
    router = { navigateByUrl() {}, navigate() {} } as any;
    ngZone = { run() {} } as any;
    component = new ContactsListComponent(contactsSandbox, userModuleAssignmentSandbox, router, ngZone);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define gridOptions onInit', () => {
    component.ngOnInit();

    expect(component.gridOptions).toBeDefined();
    expect(component.gridOptions.context).toBeDefined();
  });

  it('should set sessionStorage via _saveColumnPositions for ColumnMovedEvent', () => {
    const spy = spyOn(sessionStorage as any, 'setItem');
    const event: any = { columnApi: { getColumnState() {} } };
    (component as any)._saveColumnPositions(event);

    expect(spy).toHaveBeenCalled();
  });

  it('should set columnState via _restorColumnPositions ', () => {
    const event: any = {
      columnApi: {
        getColumnState() {
          return 'x';
        },
        setColumnState() {},
      },
    };
    const spy = spyOn(event.columnApi as any, 'setColumnState');
    (component as any)._saveColumnPositions(event);
    (component as any)._restoreColumnPositions(event);

    expect(spy).toHaveBeenCalled();
  });

  it('should navigateToDetails if BusEvents is edit ', () => {
    const spy = spyOn(component as any, 'navigateToDetails');
    const event: any = { type: 'edit' };
    (component as any)._handleBusEvents(event);

    expect(spy).toHaveBeenCalled();
  });

  it('should do nothing(TODO:) if BusEvents is delete ', () => {
    const event: any = { type: 'delete' };
    (component as any)._handleBusEvents(event);

    expect().nothing();
  });

  it('should do nothing if BusEvents is undefined ', () => {
    const event: any = { type: undefined };
    (component as any)._handleBusEvents(event);

    expect().nothing();
  });

  it('should navigate to the right url', () => {
    const url = '/test';
    const spy = spyOn(ngZone, 'run');
    component.navigateTo(url);
    expect(spy).toHaveBeenCalled();

    /* const spy = spyOn(router, 'navigate');
    const url = '/test';
    component.navigateTo(url);
    expect(spy).toHaveBeenCalledWith([url]); */
  });

  it('should navigate to the external person details page', () => {
    const event: any = {
      data: {
        contactType: Globals.CONTACT_TYPE_ID.EXTERNAL_PERSON,
        uuid: 'uuid',
      },
    };

    const spy = spyOn(ngZone, 'run');
    component.navigateToDetails(event);
    expect(spy).toHaveBeenCalled();

    /* const spy = spyOn(router, 'navigate');
    (component as any).navigateToDetails(event);
    const url = `/${Globals.PATH.PERSONS}/${Globals.PATH.EXTERNAL}/`;
    expect(spy).toHaveBeenCalledWith([url, event.data.uuid]); */
  });

  it('should navigate to the internal person details page', () => {
    const event: any = {
      data: {
        contactType: Globals.CONTACT_TYPE_ID.INTERNAL_PERSON,
        uuid: 'uuid',
      },
    };

    const spy = spyOn(ngZone, 'run');
    component.navigateToDetails(event);
    expect(spy).toHaveBeenCalled();
    //const spy = spyOn(router, 'navigate');
    //(component as any).navigateToDetails(event);
    //const url = `/${Globals.PATH.PERSONS}/${Globals.PATH.INTERNAL}/`;
    //expect(spy).toHaveBeenCalledWith([url, event.data.uuid]);
  });

  it('should navigate to the contact person details page', () => {
    const event: any = {
      data: {
        contactType: Globals.CONTACT_TYPE_ID.CONTACT_PERSON,
        uuid: 'uuid',
        companyId: 'x',
      },
    };

    const spy = spyOn(ngZone, 'run');
    component.navigateToDetails(event);
    expect(spy).toHaveBeenCalled();

    /* const spy = spyOn(router, 'navigate');
    (component as any).navigateToDetails(event);
    const url = `/${Globals.PATH.COMPANY}/${event.data.companyId}/${Globals.PATH.CONTACT_PERSON}/`;
    expect(spy).toHaveBeenCalledWith([url, event.data.uuid]); */
  });

  it('should navigate to the company details page', () => {
    const event: any = {
      data: {
        contactType: Globals.CONTACT_TYPE_ID.COMPANY,
        uuid: 'uuid',
      },
    };

    const spy = spyOn(ngZone, 'run');
    component.navigateToDetails(event);
    expect(spy).toHaveBeenCalled();

    /* const spy = spyOn(router, 'navigate');
    (component as any).navigateToDetails(event);
    const url = `/${Globals.PATH.COMPANY}/`;
    expect(spy).toHaveBeenCalledWith([url, event.data.uuid]); */
  });

  it('should navigate to the overview if contactType undefined', () => {
    const event: any = {
      data: {
        contactType: undefined,
        uuid: 'uuid',
      },
    };

    const spy = spyOn(ngZone, 'run');
    component.navigateToDetails(event);
    expect(spy).toHaveBeenCalled();

    /* const spy = spyOn(router, 'navigate');
    (component as any).navigateToDetails(event);
    const url = '/overview';
    expect(spy).toHaveBeenCalledWith([url]); */
  });

  it('checks if function searchContacts() would be called and works', () => {
    const modificationContacts = new ModifiedContacts();
    component.modifiedContacts = modificationContacts;
    const event: any = { type: 'change' };
    component.searchContacts(event);
    expect(component.modifiedContacts).not.toEqual(modificationContacts);
  });

  it('checks if function sortContacts() works', () => {
    const setModifiedContactsSortSpy = spyOn(component as any, '_setModifiedContactsSort');
    component.sortContacts();
    expect(setModifiedContactsSortSpy).toHaveBeenCalledTimes(1);
  });

  it('checks if function setModifiedContactsSearchText() works', () => {
    component.setModifiedContactsSearchText('big');
    expect(component.modifiedContacts.searchText).toBe('big');
  });

  it('checks if function setModifiedContactsContactTypeId() works', () => {
    component.setModifiedContactsContactTypeId('3IP');
    expect(component.modifiedContacts.contactTypeId).toBe('3IP');
  });

  it('checks if function setSortingContactType() works', () => {
    component.setSortingContactType('name');
    expect((component as any)._sortingContactType).toBe('name');
  });

  it('checks if function setSortingOrder() works', () => {
    component.setSortingOrder('asc');
    expect((component as any)._sortingOrder).toBe('asc');
  });

  it('checks if function _setModifiedContactsSort() works', () => {
    component.setSortingContactType('department');
    (component as any)._setModifiedContactsSort();
    expect(component.modifiedContacts.sort).toBe('department,name,id,asc');

    (component as any)._sortingOrder = null;
    (component as any)._setModifiedContactsSort();
    expect(component.modifiedContacts.sort).toBe(null);

    (component as any)._sortingContactType = null;
    (component as any)._setModifiedContactsSort();
    expect(component.modifiedContacts.sort).toBe(null);
  });

  it('checks if function setModifiedContactsExpiryDateFilter() works', () => {
    expect(component.modifiedContacts.expiringDataInPast).toBeFalsy();
    component.setModifiedContactsExpiryDateFilter();
    expect(component.modifiedContacts.expiringDataInPast).toBeTruthy();
  });

  it('checks if function setModifiedContactsDeletionLockExceedFilter() works', () => {
    expect(component.modifiedContacts.deletionLockExceeded).toBeFalsy();
    component.setModifiedContactsDeletionLockExceedFilter();
    expect(component.modifiedContacts.deletionLockExceeded).toBeTruthy();
  });

  it('checks if function setModifiedContactsModuleAssignmentFilter() works', () => {
    let moduleName = '-1';
    component.setModifiedContactsModuleAssignmentFilter(moduleName);
    expect(component.modifiedContacts.moduleName).toBe(null);
    expect(component.modifiedContacts.withoutAssignedModule).toBeTruthy();
    moduleName = '';
    component.setModifiedContactsModuleAssignmentFilter(moduleName);
    expect(component.modifiedContacts.moduleName).toBe(null);
    expect(component.modifiedContacts.withoutAssignedModule).toBeFalsy();
    moduleName = 'Betriebstagebuch';
    component.setModifiedContactsModuleAssignmentFilter(moduleName);
    expect(component.modifiedContacts.moduleName).toBe(moduleName);
    expect(component.modifiedContacts.withoutAssignedModule).toBeFalsy();
  });

  it('checks if function _resetDSGVOFilter works', () => {
    (component as any)._deletionLockExceeded = true;
    (component as any)._resetDSGVOFilter();
    expect((component as any)._deletionLockExceeded).toBeFalsy();
  });

  it('checks if function setDSGVOFilterAdvancedVisible() works', () => {
    const spy = spyOn(component as any, '_resetDSGVOFilter');
    component.isDSGVOFilterAdvancedVisible = true;
    component.modifiedContacts.expiringDataInPast = true;
    component.setDSGVOFilterAdvancedVisible();

    expect(component.isDSGVOFilterAdvancedVisible).toBeFalsy();
    expect(spy).toHaveBeenCalled();
  });
});
