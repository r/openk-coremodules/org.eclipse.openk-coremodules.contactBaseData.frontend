/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ContactTypeCellRendererComponent } from '@shared/components/cell-renderer/contact-type-cell-renderer/contact-type-cell-renderer.component';
import { IconCellRendererComponent } from '@shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';

export const CONTACTS_COLDEF = [
  {
    field: 'name',
    colId: 'name',
    headerName: 'Contacts.Name',
    filterParams: {
      debounceMs: 1000,
    },
  },
  {
    field: 'email',
    colId: 'email',
    headerName: 'Contacts.Mail',
    filterParams: {
      debounceMs: 1000,
    },
  },
  {
    field: 'contactType',
    colId: 'contactType',
    headerName: 'Contacts.ContactType',
    filterParams: {
      debounceMs: 1000,
    },
    cellRendererFramework: ContactTypeCellRendererComponent,
  },
  {
    field: 'note',
    colId: 'note',
    headerName: 'Contacts.Note',
    maxWidth: 130,
    tooltipValueGetter: params => {
      if (!!params.value && params.value.length >= 20) {
        return params.value;
      } else {
        return '';
      }
    },
    filterParams: {
      debounceMs: 1000,
    },
  },
  {
    field: 'mainAddress',
    colId: 'mainAddress',
    headerName: 'Contacts.MainAddress',
    filterParams: {
      debounceMs: 1000,
    },
  },
  {
    field: 'department',
    colId: 'department',
    headerName: 'Contacts.Department',
    filterParams: {
      debounceMs: 1000,
    },
  },
  {
    field: 'tools',
    colId: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 110,
    minWidth: 110,
    lockPosition: true,
    sortable: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRendererFramework: IconCellRendererComponent,
  },
];
