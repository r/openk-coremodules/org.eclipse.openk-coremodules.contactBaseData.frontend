/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { ContactsSandbox } from '@pages/contacts/contacts.sandbox';
import { CONTACTS_COLDEF } from '@pages/contacts/contacts-list/contacts-list-column-definition';
import { BaseList } from '@shared/components/base-components/base.list';
import { Router } from '@angular/router';
import { Globals } from '@shared/constants/globals';
import { ModifiedContacts } from '@shared/models/modifiedContacts.model';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
import { ColumnMovedEvent, GridReadyEvent } from 'ag-grid-community';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss'],
})
export class ContactsListComponent extends BaseList implements OnInit, OnDestroy {
  public readonly NEW_EXTERNAL_PERSON_PATH = `/${Globals.PATH.PERSONS}${Globals.PATH.EXTERNAL}/${Globals.PATH.NEW}`;
  public readonly NEW_INTERNAL_PERSON_PATH = `/${Globals.PATH.PERSONS}${Globals.PATH.INTERNAL}/${Globals.PATH.NEW}`;
  public readonly NEW_COMPANY_PATH = `/${Globals.PATH.COMPANY}/${Globals.PATH.NEW}`;

  public readonly INTERNAL_PERSON = Globals.CONTACT_TYPE_ID.INTERNAL_PERSON;
  public readonly EXTERNAL_PERSON = Globals.CONTACT_TYPE_ID.EXTERNAL_PERSON;
  public readonly COMPANY = Globals.CONTACT_TYPE_ID.COMPANY;
  public readonly CONTACT_PERSON = Globals.CONTACT_TYPE_ID.CONTACT_PERSON;

  public columnDefinition: any = CONTACTS_COLDEF;
  public modifiedContacts: ModifiedContacts = new ModifiedContacts();
  public isDSGVOFilterAdvancedVisible = false;
  public moduleDisplayName: string = '';
  public numberOfSubstrings: number = 0;

  private _sortingOrder: string = 'asc';
  private _standardSorting = 'name,id';
  private _sortingContactType: string = '';
  private _expiringDataInPast = false;
  private _withSyncError = false;
  private _deletionLockExceeded = false;
  private _subscription: Subscription;

  constructor(public contactsSandbox: ContactsSandbox, public userModuleAssignmentSandbox: UserModuleAssignmentSandBox, private router: Router, private ngZone: NgZone) {
    super();
  }
  // modifiedContacts.sort.split(',')[(modifiedContacts.sort.match(/,/g) || []).length]
  ngOnInit() {
    this.gridOptions = {
      ...this.gridOptions,
      localeText: Globals.LOCALE_TEXT,
    };
    // save / retrieve column positions
    this.gridOptions.onColumnMoved = this._saveColumnPositions.bind(this);
    this.gridOptions.onGridReady = this._restoreColumnPositions.bind(this);
    // context
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { edit: true, delete: false },
    };
    // handling icon events
    this._subscription = this.gridOptions.context.eventSubject.subscribe(this._handleBusEvents.bind(this));
    // save filter / search config
    if (JSON.parse(sessionStorage.getItem(Globals.SESSSION_STORAGE_KEYS.contactListSearchFilterKey))) {
      this.modifiedContacts = JSON.parse(sessionStorage.getItem(Globals.SESSSION_STORAGE_KEYS.contactListSearchFilterKey));
      if (this.modifiedContacts.sort) {
        //sonst wird bei rückkehr aus navigation die sort property null gesetzt (via _setModifiedContactsSort)
        this.numberOfSubstrings = (this.modifiedContacts.sort.match(/,/g) || []).length;
        this._sortingContactType = this.modifiedContacts.sort.split(',')[0];
        this._sortingOrder = this.modifiedContacts.sort.split(',')[this.numberOfSubstrings];
      }
      this.isDSGVOFilterAdvancedVisible = this.modifiedContacts.isDSGVOFilterAdvancedVisible;
      this._expiringDataInPast = this.modifiedContacts.expiringDataInPast;
      this._withSyncError = this.modifiedContacts.withSyncError;
      this._deletionLockExceeded = this.modifiedContacts.deletionLockExceeded;
      // modifiedContacts modulWerte zurueckuebersetzen in Display modulWerte
      this.setModifiedContactsModuleAssignmentFilterDisplayValues(this.modifiedContacts.moduleName, this.modifiedContacts.withoutAssignedModule);
    }
  }

  private _saveColumnPositions(event: ColumnMovedEvent) {
    let columnState = JSON.stringify(event.columnApi.getColumnState());
    sessionStorage.setItem(Globals.SESSSION_STORAGE_KEYS.contactListColumnStateKey, columnState);
  }

  private _restoreColumnPositions(event: GridReadyEvent) {
    let columnState = JSON.parse(sessionStorage.getItem(Globals.SESSSION_STORAGE_KEYS.contactListColumnStateKey));
    if (columnState) {
      event.columnApi.setColumnState(columnState);
    }
  }

  private _handleBusEvents(event: any): any {
    switch (event.type) {
      case 'readonly':
        this.navigateToDetails(event);
      case 'edit':
        this.navigateToDetails(event);
        break;
      default:
        break;
    }
  }

  public navigateTo(url: any) {
    this.ngZone.run(() => this.router.navigate([url]));
  }

  public navigateToDetails(event: any) {
    let route;
    switch (event.data.contactType) {
      case Globals.CONTACT_TYPE_ID.EXTERNAL_PERSON:
        route = `/${Globals.PATH.PERSONS}${Globals.PATH.EXTERNAL}/`;
        break;
      case Globals.CONTACT_TYPE_ID.INTERNAL_PERSON:
        route = `/${Globals.PATH.PERSONS}${Globals.PATH.INTERNAL}/`;
        break;
      case Globals.CONTACT_TYPE_ID.CONTACT_PERSON:
        route = `/${Globals.PATH.COMPANY}/${event.data.companyId}/${Globals.PATH.CONTACT_PERSON}/`;
        break;
      case Globals.CONTACT_TYPE_ID.COMPANY:
        route = `/${Globals.PATH.COMPANY}/`;
        break;
      default:
        route = '/overview';
        break;
    }

    this.ngZone.run(() => this.router.navigate(route !== '/overview' ? [route, event.data.uuid] : [route]));
  }

  public setModifiedContactsSearchText(searchText: string) {
    this.modifiedContacts.searchText = searchText;
  }

  public setModifiedContactsContactTypeId(contactTypeId: string) {
    this.modifiedContacts.contactTypeId = contactTypeId;
  }

  public setModifiedContactsModuleAssignmentFilter(moduleName: string) {
    if (moduleName === '-1') {
      //show contacts without module assignments
      this.modifiedContacts.moduleName = null;
      this.modifiedContacts.withoutAssignedModule = true;
    } else if (moduleName == '') {
      //show contacts with all module assignments
      this.modifiedContacts.moduleName = null;
      this.modifiedContacts.withoutAssignedModule = false;
    } else {
      //show contacts with specific module assignments
      this.modifiedContacts.moduleName = moduleName;
      this.modifiedContacts.withoutAssignedModule = false;
    }
  }

  public setModifiedContactsModuleAssignmentFilterDisplayValues(moduleName: string, withoutAssignedModule: boolean) {
    if (!moduleName && withoutAssignedModule) {
      //show contacts without module assignments
      this.moduleDisplayName = '-1';
    } else if (!moduleName && !withoutAssignedModule) {
      //show contacts with all module assignments
      this.moduleDisplayName = '';
    } else {
      //show contacts with specific module assignments
      this.moduleDisplayName = moduleName;
    }
  }

  public setModifiedContactsExpiryDateFilter() {
    this._expiringDataInPast = !this._expiringDataInPast;
    this.modifiedContacts.expiringDataInPast = this._expiringDataInPast;
  }

  public setModifiedContactsWithSyncErrorFilter() {
    this._withSyncError = !this._withSyncError;
    this.modifiedContacts.withSyncError = this._withSyncError;
  }

  public setModifiedContactsDeletionLockExceedFilter() {
    this._deletionLockExceeded = !this._deletionLockExceeded;
    this.modifiedContacts.deletionLockExceeded = this._deletionLockExceeded;
  }

  public setSortingContactType(sortingContactType: string) {
    this._sortingContactType = sortingContactType;
  }

  public setSortingOrder(sortingOrder: string) {
    this._sortingOrder = sortingOrder;
  }

  public searchContacts(event) {
    if (event.type === 'change' || event.key === 'Enter') {
      this.modifiedContacts = { ...this.modifiedContacts };
    } else if (event.type == 'keyup') {
      setTimeout(() => {
        this.modifiedContacts = { ...this.modifiedContacts };
      }, 1000);
    }
    sessionStorage.setItem(Globals.SESSSION_STORAGE_KEYS.contactListSearchFilterKey, JSON.stringify(this.modifiedContacts));
  }

  public sortContacts() {
    this._setModifiedContactsSort();
    this.modifiedContacts = { ...this.modifiedContacts };
    sessionStorage.setItem(Globals.SESSSION_STORAGE_KEYS.contactListSearchFilterKey, JSON.stringify(this.modifiedContacts));
  }

  public setDSGVOFilterAdvancedVisible() {
    this.isDSGVOFilterAdvancedVisible = !this.isDSGVOFilterAdvancedVisible;
    if (
      !this.isDSGVOFilterAdvancedVisible &&
      (this.modifiedContacts.moduleName != null ||
        this.modifiedContacts.withoutAssignedModule ||
        this.modifiedContacts.deletionLockExceeded ||
        this.modifiedContacts.expiringDataInPast)
    ) {
      this._resetDSGVOFilter();
    }
    this.modifiedContacts.isDSGVOFilterAdvancedVisible = this.isDSGVOFilterAdvancedVisible;
    sessionStorage.setItem(Globals.SESSSION_STORAGE_KEYS.contactListSearchFilterKey, JSON.stringify(this.modifiedContacts));
  }

  private _resetDSGVOFilter() {
    this._deletionLockExceeded = false;
    this._expiringDataInPast = false;
    this.modifiedContacts.moduleName = null;
    this.modifiedContacts.withoutAssignedModule = false;
    this.modifiedContacts.deletionLockExceeded = false;
    this.modifiedContacts.expiringDataInPast = false;
    this.modifiedContacts = { ...this.modifiedContacts };
  }

  private _setModifiedContactsSort() {
    if (this._sortingOrder && this._sortingContactType) {
      const sort = this._sortingContactType + ',' + this._standardSorting + ',' + this._sortingOrder;
      this.modifiedContacts.sort = sort;
      this.numberOfSubstrings = (this.modifiedContacts.sort.match(/,/g) || []).length;
      sessionStorage.setItem(Globals.SESSSION_STORAGE_KEYS.contactListSearchFilterKey, JSON.stringify(this.modifiedContacts));
    } else {
      this.modifiedContacts.sort = null;
      sessionStorage.setItem(Globals.SESSSION_STORAGE_KEYS.contactListSearchFilterKey, JSON.stringify(this.modifiedContacts));
    }
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
