/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ModifiedContacts } from '@shared/models/modifiedContacts.model';
import { Injectable } from '@angular/core';
import { HttpService, Query, GET, Path, Adapter, PUT, Body, DefaultHeaders } from '@shared/asyncServices/http';
import { Observable } from 'rxjs';
import { ContactsService } from '@pages/contacts/contacts.service';
import { Contact } from '@shared/models';
import { PageRequestInterface } from '@shared/models/page/page-request.interface';
import { PageModel } from '@shared/models/page/page.model';

@Injectable()
@DefaultHeaders({
  Accept: 'application/json',
  'Content-Type': 'application/json',
})
export class ContactsApiClient extends HttpService {
  public getContacts(request: PageRequestInterface = null): Observable<PageModel<Contact>> {
    let modifiedContacts: ModifiedContacts = null;
    if (request != null) {
      modifiedContacts = request.queryParameter || {};
      return this._getContactsPage(
        modifiedContacts.searchText,
        modifiedContacts.contactTypeId,
        modifiedContacts.sort,
        modifiedContacts.moduleName,
        modifiedContacts.withoutAssignedModule,
        modifiedContacts.expiringDataInPast,
        modifiedContacts.withSyncError,
        modifiedContacts.deletionLockExceeded,
        request.pageNumber ? request.pageNumber - 1 : 0,
        request.pageSize
      );
    } else {
      return this._getContactsPage(
        modifiedContacts.searchText,
        modifiedContacts.contactTypeId,
        modifiedContacts.sort,
        modifiedContacts.moduleName,
        modifiedContacts.withoutAssignedModule,
        modifiedContacts.expiringDataInPast,
        modifiedContacts.withSyncError,
        modifiedContacts.deletionLockExceeded,
        1,
        3
      );
    }
  }

  /**
   * Retrieves all paged contacts
   */
  @GET('/contacts')
  @Adapter(ContactsService.gridPageAdapter)
  private _getContactsPage(
    @Query('searchText') searchtext: string,
    @Query('contactType') contactTypeId: string,
    @Query('sort') sort: string,
    @Query('moduleName') moduleName: string,
    @Query('withoutAssignedModule') withoutAssignedModule: boolean,
    @Query('expiringDataInPast') expiringDataInPast: boolean,
    @Query('withSyncError') withSyncError: boolean,
    @Query('deletionLockExceeded') deletionLockExceeded: boolean,
    @Query('page') pageNumber: number,
    @Query('size') pageSize: number
  ): Observable<any> {
    return null;
  }

  /**
   * Retrieves contacts details by a given id
   *
   * @param id
   */
  @GET('/contacts/{id}')
  @Adapter(ContactsService.contactDetailsAdapter)
  public getContactDetails(@Path('id') id: number): Observable<any> {
    return null;
  }

  @PUT('/contacts/{id}')
  @Adapter(ContactsService.contactDetailsAdapter)
  public putContactDetails(@Path('id') id: number, @Body() editedContact: Contact): Observable<Contact> {
    return null;
  }

  @PUT('/contacts/{id}/anonymize')
  public anonymizeContact(@Path('id') id: string): Observable<void> {
    return null;
  }
}
