/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AddressTypesSandbox } from '@pages/admin/address-types//address-types.sandbox';

@Component({
  selector: 'app-address-types-details',
  templateUrl: './address-types-details.component.html',
  styleUrls: ['./address-types-details.component.scss']
})
export class AddressTypesDetailsComponent implements OnInit, OnDestroy {

  constructor(
    public addressTypesSandbox: AddressTypesSandbox,
  ) { }

  ngOnInit() {
    this.addressTypesSandbox.registerEvents();
  }

  ngOnDestroy() {
    this.addressTypesSandbox.clear();
    this.addressTypesSandbox.endSubscriptions();
  }

}
