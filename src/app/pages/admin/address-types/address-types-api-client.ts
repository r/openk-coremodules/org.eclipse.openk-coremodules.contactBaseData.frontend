/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AddressTypesService } from '@pages/admin/address-types/address-types.service';
import { Injectable } from '@angular/core';
import { HttpService, GET, Adapter, DefaultHeaders, Path, PUT, Body, POST, DELETE } from '@shared/asyncServices/http';
import { Observable } from 'rxjs';
import { AddressType } from '@shared/models';

@Injectable()
@DefaultHeaders({
  'Accept': 'application/json',
  'Content-Type': 'application/json',
})
export class AddressTypesApiClient extends HttpService {

  @GET('/addressTypes')
  @Adapter(AddressTypesService.gridAdapter)
  public getAddressTypes(): Observable<AddressType[]> {
    return null;
  }

  @GET('/addressTypes/{id}')
  @Adapter(AddressTypesService.itemAdapter)
  public getAddressTypeDetails(@Path('id') id: string): Observable<AddressType> {
    return null;
  }

  @PUT('/addressTypes/{id}')
  @Adapter(AddressTypesService.itemAdapter)
  public putAddressType(@Path('id') id: string, @Body() item: AddressType): Observable<AddressType> {
    return null;
  }

  @POST('/addressTypes')
  @Adapter(AddressTypesService.itemAdapter)
  public postAddressType(@Body() newAddressType: AddressType): Observable<AddressType> {
    return null;
  }

  @DELETE('/addressTypes/{id}')
  @Adapter(AddressTypesService.itemAdapter)
  public deleteAddressType(@Path('id') id: string): Observable<void> {
    return null;
  }

}
