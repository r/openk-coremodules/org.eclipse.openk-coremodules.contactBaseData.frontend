/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';
import {
  SALUTATIONS_COLDEF,
  COMMUNICATION_TYPES_COLDEF,
  PERSON_TYPES_COLDEF,
  ADDRESS_TYPES_COLDEF,
} from '@pages/admin/salutations/salutations-list/salutations-list-column-definition';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BaseList } from '@shared/components/base-components/base.list';
import { SetFilterComponent } from '@shared/filters/ag-grid/set-filter/set-filter.component';
import { Subscription } from 'rxjs';
import { CommunicationTypesSandbox } from './communication-types/communication-types.sandbox';
import { GridOptions } from 'ag-grid-community';
import { Subject } from 'rxjs';
import { PersonTypesSandbox } from './person-types/person-types.sandbox';
import { AddressTypesSandbox } from './address-types/address-types.sandbox';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent extends BaseList implements OnInit, OnDestroy {
  public salutationsColDef: any = SALUTATIONS_COLDEF;
  public communicationsTypeColDef: any = COMMUNICATION_TYPES_COLDEF;
  public personTypeColDef: any = PERSON_TYPES_COLDEF;
  public addressTypeColDef: any = ADDRESS_TYPES_COLDEF;
  public frameworkComponents: { setFilterComponent: any };
  private _subscription: Subscription;
  public isExpandableVisible = false;

  public salutationEvents$: Subject<any> = new Subject();
  public salutationGridOptions: GridOptions = {
    context: {
      eventSubject: this.salutationEvents$,
    },
    defaultColDef: {
      filter: false,
    },
    suppressLoadingOverlay: true,
  };

  public communicationTypesEvents$: Subject<any> = new Subject();
  public communicationTypesGridOptions: GridOptions = {
    context: {
      eventSubject: this.communicationTypesEvents$,
    },
    defaultColDef: {
      filter: false,
    },
    suppressLoadingOverlay: true,
  };

  public personTypesEvents$: Subject<any> = new Subject();
  public personTypesGridOptions: GridOptions = {
    context: {
      eventSubject: this.personTypesEvents$,
    },
    defaultColDef: {
      filter: false,
    },
    suppressLoadingOverlay: true,
  };

  public addressTypesEvents$: Subject<any> = new Subject();
  public addressTypesGridOptions: GridOptions = {
    context: {
      eventSubject: this.addressTypesEvents$,
    },
    defaultColDef: {
      filter: false,
    },
    suppressLoadingOverlay: true,
  };

  constructor(
    public salutationsSandbox: SalutationsSandbox,
    public communicationTypesSandbox: CommunicationTypesSandbox,
    public personTypesSandbox: PersonTypesSandbox,
    public addressTypesSandbox: AddressTypesSandbox
  ) {
    super();
    this.frameworkComponents = { setFilterComponent: SetFilterComponent };
  }

  ngOnInit() {
    /********************************* Salutations  *********************************/
    this.salutationGridOptions.context = {
      ...this.salutationGridOptions.context,
      icons: { edit: true, delete: true },
    };
    this._subscription = this.salutationGridOptions.context.eventSubject.subscribe(event => {
      if (event.type === 'edit' || event.type === 'readonly') {
        this.salutationsSandbox.setDisplayForm();
        this.salutationsSandbox.loadSalutation(event.data.id);
      }
      if (event.type === 'delete') {
        this.salutationsSandbox.deleteSalutation(event.data.id);
      }
    });

    /********************************* Communication-Types  *********************************/
    this.communicationTypesGridOptions.context = {
      ...this.communicationTypesGridOptions.context,
      icons: { edit: true, readonly: true, delete: true },
    };
    this._subscription = this.communicationTypesGridOptions.context.eventSubject.subscribe(event => {
      if (event.type === 'edit' || event.type === 'readonly') {
        this.communicationTypesSandbox.setDisplayForm(event.type);
        this.communicationTypesSandbox.loadCommunicationType(event.data.id);
      }
      if (event.type === 'delete') {
        this.communicationTypesSandbox.deleteCommunicationType(event.data.id);
      }
    });

    /********************************* Person-Types  *********************************/
    this.personTypesGridOptions.context = {
      ...this.personTypesGridOptions.context,
      icons: { edit: true, delete: true },
    };
    this._subscription = this.personTypesGridOptions.context.eventSubject.subscribe(event => {
      if (event.type === 'edit' || event.type === 'readonly') {
        this.personTypesSandbox.setDisplayForm();
        this.personTypesSandbox.loadPersonType(event.data.id);
      }
      if (event.type === 'delete') {
        this.personTypesSandbox.deletePersonType(event.data.id);
      }
    });

    /********************************* Address-Types  *********************************/
    this.addressTypesGridOptions.context = {
      ...this.addressTypesGridOptions.context,
      icons: { edit: true, delete: true },
    };
    this._subscription = this.addressTypesGridOptions.context.eventSubject.subscribe(event => {
      if (event.type === 'edit' || event.type === 'readonly') {
        this.addressTypesSandbox.setDisplayForm();
        this.addressTypesSandbox.loadAddressType(event.data.id);
      }
      if (event.type === 'delete') {
        this.addressTypesSandbox.deleteAddressType(event.data.id);
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _initExpandableState() {
    //this.isExpandableVisible = !!this.salutationsSandbox.companyContactId;
  }
}
