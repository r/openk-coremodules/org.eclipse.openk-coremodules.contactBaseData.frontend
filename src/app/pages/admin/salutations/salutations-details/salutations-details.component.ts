 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-salutations-details',
  templateUrl: './salutations-details.component.html',
  styleUrls: ['./salutations-details.component.scss']
})
export class SalutationsDetailsComponent implements OnInit, OnDestroy {

  constructor(
    public salutationsSandbox: SalutationsSandbox,
  ) { }

  ngOnInit() {
    this.salutationsSandbox.registerEvents();
  }

  ngOnDestroy() {
    this.salutationsSandbox.clear();
    this.salutationsSandbox.endSubscriptions();
  }

}
