/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { IconCellRendererComponent } from '@shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { BoolCellRendererComponent } from '@shared/components/cell-renderer/bool-cell-renderer/bool-cell-renderer.component';

export const SALUTATIONS_COLDEF = [
  {
    field: 'type',
    headerName: 'Salutations.Type',
    sortable: true,
    flex: 1
  },
  {
    field: 'description',
    headerName: 'Salutations.Description',
    sortable: true,
    flex: 1
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 110,
    minWidth: 110,
    lockPosition: true,
    sortable: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRendererFramework: IconCellRendererComponent,
  },
];

export const COMMUNICATION_TYPES_COLDEF = [
  {
    field: 'type',
    headerName: 'CommunicationTypes.Type',
    sortable: true,
    flex: 1
  },
  {
    field: 'description',
    headerName: 'CommunicationTypes.Description',
    sortable: true,
    flex: 1
  },
  {
    field: 'editable',
    headerName: 'CommunicationTypes.Editable',
    sortable: true,
    flex: 1,
    cellRendererFramework: BoolCellRendererComponent,
  },
  {
    field: 'mappingLdap',
    headerName: 'CommunicationTypes.MappingLdap',
    sortable: true,
    flex: 1,
    cellRendererFramework: BoolCellRendererComponent,
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 110,
    minWidth: 110,
    lockPosition: true,
    sortable: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRendererFramework: IconCellRendererComponent,
  },
];

export const PERSON_TYPES_COLDEF = [
  {
    field: 'type',
    headerName: 'PersonTypes.Type',
    sortable: true,
    flex: 1
  },
  {
    field: 'description',
    headerName: 'PersonTypes.Description',
    sortable: true,
    flex: 1
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 110,
    minWidth: 110,
    lockPosition: true,
    sortable: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRendererFramework: IconCellRendererComponent,
  },
];

export const ADDRESS_TYPES_COLDEF = [
  {
    field: 'type',
    headerName: 'AddressTypes.Type',
    sortable: true,
    flex: 1
  },
  {
    field: 'description',
    headerName: 'AddressTypes.Description',
    sortable: true,
    flex: 1
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 110,
    minWidth: 110,
    lockPosition: true,
    sortable: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRendererFramework: IconCellRendererComponent,
  },
];
