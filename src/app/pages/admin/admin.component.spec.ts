/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Salutation } from '@shared/models';
import { AdminComponent } from '@pages/admin/admin.component';

describe('AdminComponent', () => {
  let component: AdminComponent;
  let communicationTypesSandbox: any = {
    setDisplayForm: () => {},
    loadCommunicationType: () => {},
    deleteCommunicationType: () => {},
  };
  let personTypesSandbox: any = {
    setDisplayForm: () => {},
    loadPersonType: () => {},
    deletePersonType: () => {},
  };
  let addressTypesSandbox: any = {
    setDisplayForm: () => {},
    loadAddressType: () => {},
    deleteAddressType: () => {},
  };
  const salutationsSandbox: any = {
    setDisplayForm: () => {},
    loadSalutation: () => {},
    deleteSalutation: () => {},
  };

  beforeEach(() => {
    component = new AdminComponent(salutationsSandbox, communicationTypesSandbox, personTypesSandbox, addressTypesSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize gridOptions context', () => {
    component.ngOnInit();
    expect(component.salutationGridOptions.context.icons.edit).toBeTruthy();
    expect(component.salutationGridOptions.context.icons.delete).toBeTruthy();
  });

  it('should call appropriate functions for edit event', () => {
    const spy1: any = spyOn(salutationsSandbox, 'setDisplayForm');
    const spy2: any = spyOn(salutationsSandbox, 'loadSalutation');
    component.ngOnInit();
    component.salutationGridOptions.context.eventSubject.next({ type: 'edit', data: new Salutation() });
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });

  it('should call appropriate functions for delete event', () => {
    const spy3: any = spyOn(salutationsSandbox, 'deleteSalutation');
    component.ngOnInit();
    component.salutationGridOptions.context.eventSubject.next({ type: 'delete', data: new Salutation() });
    expect(spy3).toHaveBeenCalled();
  });

  it('should call appropriate functions for edit event', () => {
    const spy1: any = spyOn(communicationTypesSandbox, 'setDisplayForm');
    const spy2: any = spyOn(communicationTypesSandbox, 'loadCommunicationType');
    component.ngOnInit();
    component.communicationTypesGridOptions.context.eventSubject.next({ type: 'edit', data: new Salutation() });
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });

  it('should call appropriate functions for delete event', () => {
    const spy3: any = spyOn(communicationTypesSandbox, 'deleteCommunicationType');
    component.ngOnInit();
    component.communicationTypesGridOptions.context.eventSubject.next({ type: 'delete', data: new Salutation() });
    expect(spy3).toHaveBeenCalled();
  });

  it('should call appropriate functions for edit event', () => {
    const spy1: any = spyOn(personTypesSandbox, 'setDisplayForm');
    const spy2: any = spyOn(personTypesSandbox, 'loadPersonType');
    component.ngOnInit();
    component.personTypesGridOptions.context.eventSubject.next({ type: 'edit', data: new Salutation() });
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });

  it('should call appropriate functions for delete event', () => {
    const spy3: any = spyOn(personTypesSandbox, 'deletePersonType');
    component.ngOnInit();
    component.personTypesGridOptions.context.eventSubject.next({ type: 'delete', data: new Salutation() });
    expect(spy3).toHaveBeenCalled();
  });

  it('should call appropriate functions for edit event', () => {
    const spy1: any = spyOn(addressTypesSandbox, 'setDisplayForm');
    const spy2: any = spyOn(addressTypesSandbox, 'loadAddressType');
    component.ngOnInit();
    component.addressTypesGridOptions.context.eventSubject.next({ type: 'edit', data: new Salutation() });
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });

  it('should call appropriate functions for delete event', () => {
    const spy3: any = spyOn(addressTypesSandbox, 'deleteAddressType');
    component.ngOnInit();
    component.addressTypesGridOptions.context.eventSubject.next({ type: 'delete', data: new Salutation() });
    expect(spy3).toHaveBeenCalled();
  });
});
