 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommunicationTypesService } from '@pages/admin/communication-types/communication-types.service';
import { Injectable } from '@angular/core';
import { HttpService, GET, Adapter, DefaultHeaders, Path, PUT, Body, POST, DELETE } from '@shared/asyncServices/http';
import { Observable } from 'rxjs';
import { CommunicationType } from '@shared/models';

@Injectable()
@DefaultHeaders({
  'Accept': 'application/json',
  'Content-Type': 'application/json',
})
export class CommunicationTypesApiClient extends HttpService {

  @GET('/communication-types')
  @Adapter(CommunicationTypesService.gridAdapter)
  public getCommunicationTypes(): Observable<Array<CommunicationType>> {
    return null;
  }

  @GET('/communication-types/{id}')
  @Adapter(CommunicationTypesService.responseAdapter)
  public getCommunicationType(@Path('id') id: string): Observable<CommunicationType> {
    return null;
  }

  @PUT('/communication-types/{id}')
  @Adapter(CommunicationTypesService.responseAdapter)
  public putCommunicationType(@Path('id') id: string, @Body() item: CommunicationType): Observable<CommunicationType> {
    return null;
  }

  @POST('/communication-types')
  @Adapter(CommunicationTypesService.responseAdapter)
  public postCommunicationType(@Body() newCommunicationType: CommunicationType): Observable<CommunicationType> {
    return null;
  }

  @DELETE('/communication-types/{id}')
  @Adapter(CommunicationTypesService.responseAdapter)
  public deleteCommunicationType(@Path('id') id: string): Observable<void> {
    return null;
  }

}
