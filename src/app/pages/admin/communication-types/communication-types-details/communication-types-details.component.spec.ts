import { CommunicationTypesDetailsComponent } from '@pages/admin/communication-types/communication-types-details/communication-types-details.component';
import { CommunicationTypesSandbox } from '@pages/admin/communication-types/communication-types.sandbox';

describe('CommunicationTypesDetailsComponent', () => {
  let component: CommunicationTypesDetailsComponent;
  let sandbox: CommunicationTypesSandbox;

beforeEach(() => {
  sandbox = {
    registerEvents() {},
    endSubscriptions() {},
    clear() {},
  } as any;
  component = new CommunicationTypesDetailsComponent(sandbox);
});

it('should create', () => {
  expect(component).toBeTruthy();
});

it('should registerEvents OnInit', () => {
  let spy = spyOn(sandbox,'registerEvents');
  component.ngOnInit();
  expect(spy).toHaveBeenCalled();
});

it('should endSubscriptions OnDestroy', () => {
  let spy = spyOn(sandbox,'endSubscriptions');
  component.ngOnDestroy();
  expect(spy).toHaveBeenCalled();
});
});

