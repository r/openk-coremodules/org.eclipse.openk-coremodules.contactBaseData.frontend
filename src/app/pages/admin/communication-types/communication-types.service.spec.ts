import { CommunicationType } from '@shared/models';
import { CommunicationTypesService } from '@pages/admin/communication-types/communication-types.service';

describe('CommunicationTypesService', () => {
  let service: CommunicationTypesService;

  beforeEach(() => {
  });

  it('should transform list', () => {
    const response = [new CommunicationType()];
    response[0].type = 'X';

    expect(CommunicationTypesService.gridAdapter(response)[0].type).toBe('X');
  });


  it('should transform details', () => {
    const response: any = { type: 'X'};
    const item = CommunicationTypesService.responseAdapter(response);

    expect(item.type).toBe(response.type);
  });
});
