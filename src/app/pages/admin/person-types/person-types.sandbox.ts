/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UtilService } from '@shared/utility/utility.service';
import { Injectable } from '@angular/core';
import { Store, ActionsSubject } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import * as store from '@shared/store';
import * as personTypesActions from '@shared/store/actions/admin/person-types.action';
import * as personTypesFormReducer from '@shared/store/reducers/admin/person-types-details-form.reducer';
import { PersonType } from '@shared/models';
import { FormGroupState, SetValueAction, ResetAction, MarkAsTouchedAction } from 'ngrx-forms';
import { ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { takeUntil, take } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { ILoadPersonTypesSuccess } from '@shared/store/actions/admin/person-types.action';

@Injectable()
export class PersonTypesSandbox extends BaseSandbox {
  public personTypes$ = this.appState$.select(store.getPersonTypesData);
  public personTypesLoading$ = this.appState$.select(store.getPersonTypesLoading);
  public formState$ = this.appState$.select(store.getPersonTypesDetails);
  public currentFormState: FormGroupState<PersonType>;
  public displayForm: boolean = false;

  constructor(
    protected appState$: Store<store.State>,
    protected actionsSubject: ActionsSubject,
    protected router: Router,
    protected utilService: UtilService,
    protected modalService: NgbModal
  ) {
    super(appState$);
  }

  public setDisplayForm(): void {
    this.clear();
    this.appState$.dispatch(new MarkAsTouchedAction(personTypesFormReducer.FORM_ID));
    this.displayForm = true;
  }

  public loadPersonTypes(): Observable<ILoadPersonTypesSuccess> {
    this.appState$.dispatch(personTypesActions.loadPersonTypes());
    return this.actionsSubject.pipe(ofType(personTypesActions.loadPersonTypesSuccess), take(1));
  }

  public loadPersonType(id: string): void {
    this.appState$.dispatch(personTypesActions.loadPersonType({ payload: id }));
  }

  public deletePersonType(id: string): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(personTypesActions.deletePersonType({ payload: id }));
        this.clear();
      },
      () => {}
    );
  }

  public cancel(): void {
    if (!this.currentFormState.isPristine) {
      const modalRef = this.modalService.open(SafetyQueryDialogComponent);
      modalRef.componentInstance.title = 'ConfirmDialog.Action.edit';
      modalRef.componentInstance.body = 'ConfirmDialog.Content';
      modalRef.result.then(
        () => {
          this.clear();
        },
        () => {}
      );
    } else {
      this.clear();
    }
  }

  clear(): void {
    this.appState$.dispatch(new SetValueAction(personTypesFormReducer.FORM_ID, personTypesFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(personTypesFormReducer.FORM_ID));
    this.displayForm = false;
  }

  public savePersonType(): void {
    if (this.currentFormState.isValid) {
      this.appState$.dispatch(
        personTypesActions.savePersonType({
          payload: new PersonType(this.currentFormState.value),
        })
      );
      this.actionsSubject.pipe(ofType(personTypesActions.savePersonTypeSuccess), takeUntil(this._endSubscriptions$)).subscribe(() => {
        this.clear();
      });
    } else {
      this.utilService.displayNotification('MandatoryFieldError', 'alert');
    }
  }

  public registerEvents(): void {
    this.formState$.pipe(takeUntil(this._endSubscriptions$)).subscribe((formState: FormGroupState<PersonType>) => (this.currentFormState = formState));
  }
}
