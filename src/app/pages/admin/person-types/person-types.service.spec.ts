import { PersonType } from '@shared/models';
import { PersonTypesService } from '@pages/admin/person-types/person-types.service';

describe('PersonTypesService', () => {
  let service: PersonTypesService;

  beforeEach(() => {
  });

  it('should transform list', () => {
    const response = [new PersonType()];
    response[0].type = 'Herr';

    expect(PersonTypesService.gridAdapter(response)[0].type).toBe('Herr');
  });


  it('should transform details', () => {
    const response: any = { type: 'Herr'};
    const item = PersonTypesService.itemAdapter(response);

    expect(item.type).toBe(response.type);
  });
});
