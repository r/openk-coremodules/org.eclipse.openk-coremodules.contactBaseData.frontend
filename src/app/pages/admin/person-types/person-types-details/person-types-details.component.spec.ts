import { PersonTypesDetailsComponent } from '@pages/admin/person-types/person-types-details/person-types-details.component';
import { PersonTypesSandbox } from '@pages/admin/person-types/person-types.sandbox';

describe('PersonTypesDetailsComponent', () => {
  let component: PersonTypesDetailsComponent;
  let sandbox: PersonTypesSandbox;

beforeEach(() => {
  sandbox = {
    registerEvents() {},
    endSubscriptions() {},
    clear() {},
  } as any;
  component = new PersonTypesDetailsComponent(sandbox);
});

it('should create', () => {
  expect(component).toBeTruthy();
});

it('should registerEvents OnInit', () => {
  let spy = spyOn(sandbox,'registerEvents');
  component.ngOnInit();
  expect(spy).toHaveBeenCalled();
});

it('should endSubscriptions OnDestroy', () => {
  let spy = spyOn(sandbox,'endSubscriptions');
  component.ngOnDestroy();
  expect(spy).toHaveBeenCalled();
});
});
