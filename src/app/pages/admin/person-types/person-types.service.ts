import { Injectable } from '@angular/core';
import { PersonType } from '@shared/models';

@Injectable()
export class PersonTypesService {

  static gridAdapter(response: any): Array<PersonType> {
    return response.map(personType => new PersonType(personType));
  }

  static itemAdapter(response: any): PersonType {
    return new PersonType(response);
  }
}
