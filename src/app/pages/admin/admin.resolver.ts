/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ILoadSalutationsSuccess } from '@shared/store/actions/admin/salutations.action';
import { CommunicationTypesSandbox } from './communication-types/communication-types.sandbox';
import { PersonTypesSandbox } from './person-types/person-types.sandbox';
import { AddressTypesSandbox } from './address-types/address-types.sandbox';

@Injectable()
export class AdminResolver implements Resolve<ILoadSalutationsSuccess> {
  constructor(
    private salutationsSandbox: SalutationsSandbox,
    private communicationTypesSandbox: CommunicationTypesSandbox,
    private personTypesSandbox: PersonTypesSandbox,
    private addressTypesSandbox: AddressTypesSandbox
  ) {}

  public resolve(): Observable<any> {
    this.addressTypesSandbox.loadAddressTypes();
    this.personTypesSandbox.loadPersonTypes();
    this.communicationTypesSandbox.loadCommunicationTypes();
    return this.salutationsSandbox.loadSalutations();
  }
}
