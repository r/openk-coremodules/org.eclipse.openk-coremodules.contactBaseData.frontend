/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '@shared/components';
import { TranslateModule } from '@ngx-translate/core';
import { NgrxFormsModule } from 'ngrx-forms';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { DirectivesModule } from '@shared/directives';
import { FiltersModule } from '@shared/filters/index.module';
import { ContainersModule } from '@shared/containers';
import { SetFilterComponent } from '@shared/filters/ag-grid/set-filter/set-filter.component';
import { CompanyApiClient } from '@pages/company/company-api-client';
import { CompanyDetailsComponent } from '@pages/company/company-details/company-details.component';
import { CompanyService } from '@pages/company/company.service';
import { CompanyDetailsSandBox } from '@pages/company/company-details/company-details.sandbox';
import { StoreModule } from '@ngrx/store';
import { companyReducers } from '@shared/store';
import { CompanyDetailsResolver } from '@pages/company/company-details/company-details.resolver';
import { CompanyRoutingModule } from '@pages/company/company.routing.module';
import { EffectsModule } from '@ngrx/effects';
import { CompanyEffect } from '@shared/store/effects/company/company.effect';
import { CompanyCommunicationsDataListComponent } from '@pages/company/company-details/communications-data-list/communications-data-list.component';
import { CompanyCommunicationsDataDetailsComponent } from '@pages/company/company-details/communications-data-details/communications-data-details.component';
import { CompanyAddressDetailsComponent } from '@pages/company/company-details/address-details/address-details.component';
import { CompanyAddressListComponent } from '@pages/company/company-details/address-list/address-list.component';
import { CompanyContactPersonListComponent } from '@pages/company/company-details/contact-person-list/contact-person-list.component';
import { CompanyContactPersonDetailsComponent } from '@pages/company/company-details/contact-person-details/contact-person-details.component';
import { ContactPersonDetailsSandbox } from '@pages/company/company-details/contact-person-details/contact-person-details.sandbox';
import { ContactPersonDetailsResolver } from '@pages/company/company-details/contact-person-details/contact-person-details.resolver';
import { ContactPersonEffect } from '@shared/store/effects/company/contact-person.effect';
import { ContactPersonAddressDetailsComponent } from '@pages/company/company-details/contact-person-details/address-details/address-details.component';
import { ContactPersonAddressListComponent } from '@pages/company/company-details/contact-person-details/address-list/address-list.component';
import { ContactPersonCommunicationsDataListComponent } from '@pages/company/company-details/contact-person-details/communications-data-list/communications-data-list.component';
import { ContactPersonCommunicationsDataDetailsComponent } from '@pages/company/company-details/contact-person-details/communications-data-details/communications-data-details.component';
import { CommunicationTypesSandbox } from '../admin/communication-types/communication-types.sandbox';
import { AddressTypesSandbox } from '../admin/address-types/address-types.sandbox';
import { SalutationsSandbox } from '../admin/salutations/salutations.sandbox';
import { PersonTypesSandbox } from '../admin/person-types/person-types.sandbox';
import { CommunicationTypesEffects } from '@app/shared/store/effects/admin/communication-types.effect';
import { PersonTypesEffects } from '@app/shared/store/effects/admin/person-types.effect';
import { AddressTypesEffects } from '@app/shared/store/effects/admin/address-types.effect';
import { SalutationsEffects } from '@app/shared/store/effects/admin/salutations.effect';
import { CommunicationTypesApiClient } from '../admin/communication-types/communication-types-api-client';
import { PersonTypesApiClient } from '../admin/person-types/person-types-api-client';
import { AddressTypesApiClient } from '../admin/address-types/address-types-api-client';
import { SalutationsApiClient } from '../admin/salutations/salutations-api-client';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    TranslateModule,
    DirectivesModule,
    FiltersModule,
    ReactiveFormsModule,
    RouterModule,
    NgrxFormsModule,
    FormsModule,
    StoreModule.forFeature('companyData', companyReducers),
    AgGridModule.withComponents([SetFilterComponent]),
    ContainersModule,
    CompanyRoutingModule,
    EffectsModule.forFeature([CompanyEffect, ContactPersonEffect, CommunicationTypesEffects, PersonTypesEffects, AddressTypesEffects, SalutationsEffects]),
  ],
  declarations: [
    CompanyDetailsComponent,
    CompanyCommunicationsDataListComponent,
    CompanyCommunicationsDataDetailsComponent,
    CompanyAddressDetailsComponent,
    CompanyAddressListComponent,
    CompanyContactPersonListComponent,
    CompanyContactPersonDetailsComponent,
    ContactPersonAddressListComponent,
    ContactPersonAddressDetailsComponent,
    ContactPersonAddressListComponent,
    ContactPersonAddressDetailsComponent,
    ContactPersonCommunicationsDataListComponent,
    ContactPersonCommunicationsDataDetailsComponent,
  ],
  providers: [
    CompanyService,
    CompanyApiClient,
    CompanyDetailsSandBox,
    CompanyDetailsResolver,
    ContactPersonDetailsSandbox,
    CommunicationTypesSandbox,
    AddressTypesSandbox,
    ContactPersonDetailsResolver,
    SalutationsSandbox,
    PersonTypesSandbox,
    CommunicationTypesApiClient,
    PersonTypesApiClient,
    AddressTypesApiClient,
    SalutationsApiClient,
  ],
})
export class CompanyModule {}
