import { UserModuleAssignment } from '@shared/models';
import { UserModuleAssignmentSandBox } from '@app/shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyDetailsSandBox } from '@pages/company/company-details/company-details.sandbox';
import { Component, ViewEncapsulation } from '@angular/core';
import { OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CompanyDetailsComponent implements OnInit, OnDestroy {
  public createOrEditCommunicationsData = '';
  public createOrEditAddressData = '';
  public createOrEditUserModuleAssignmentData = '';
  public isExpandableVisible = false;

  constructor(
    public companyDetailsSandBox: CompanyDetailsSandBox,
    public userModuleAssignmentSandBox: UserModuleAssignmentSandBox,
    private _translate: TranslateService
  ) {}

  ngOnInit() {
    this.companyDetailsSandBox.registerCompanyEvents();
    this._initExpandableState();
  }

  ngOnDestroy() {
    this.companyDetailsSandBox.endSubscriptions();
    this.companyDetailsSandBox.companyContactId = null;
  }

  public loadCommunicationsDataDetail(detailId: string) {
    if (detailId == null) {
      this.createOrEditCommunicationsData = this._translate.instant('CommunicationsData.NewCommunicationsData');
    } else {
      this.createOrEditCommunicationsData = this._translate.instant('CommunicationsData.EditCommunicationsData');
      this.companyDetailsSandBox.loadCommunicationsDataDetails(detailId);
    }
    this.companyDetailsSandBox.isCommunicationsDataDetailViewVisible = true;
  }

  public loadAddressDetail(detailId: string) {
    if (detailId == null) {
      this.createOrEditAddressData = this._translate.instant('Address.NewAddress');
    } else {
      this.createOrEditAddressData = this._translate.instant('Address.EditAddress');
      this.companyDetailsSandBox.loadCompanyDetailsAddressDetails(detailId);
    }
    this.companyDetailsSandBox.isAddressDataDetailViewVisible = true;
  }

  public loadUserModuleAssignmentDataDetail(userModuleAssignment: UserModuleAssignment) {
    if (userModuleAssignment == null) {
      this.createOrEditUserModuleAssignmentData = this._translate.instant('UserModuleAssignment.NewUserModuleAssignment');
    } else {
      this.createOrEditUserModuleAssignmentData = this._translate.instant('UserModuleAssignment.EditUserModuleAssignment');
      this.userModuleAssignmentSandBox.loadUserModuleAssignmentDetails(userModuleAssignment);
    }
    this.userModuleAssignmentSandBox.isUserModuleAssignmentDataDetailViewVisible = true;
  }

  private _initExpandableState() {
    this.isExpandableVisible = !!this.companyDetailsSandBox.companyContactId;

    this.companyDetailsSandBox.isAddressDataDetailViewVisible = false;
    this.companyDetailsSandBox.isCommunicationsDataDetailViewVisible = false;
  }
}
