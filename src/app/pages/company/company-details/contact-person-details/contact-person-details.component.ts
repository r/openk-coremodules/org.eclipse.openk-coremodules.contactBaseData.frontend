/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ContactPersonDetailsSandbox } from '@pages/company/company-details/contact-person-details/contact-person-details.sandbox';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { SalutationsSandbox } from '@pages/admin/salutations/salutations.sandbox';
import { PersonTypesSandbox } from '@pages/admin/person-types/person-types.sandbox';
import { TranslateService } from '@ngx-translate/core';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
import { Address, CommunicationsData, UserModuleAssignment } from '@shared/models';

@Component({
  selector: 'app-company-contact-person-details',
  templateUrl: './contact-person-details.component.html',
  styleUrls: ['./contact-person-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CompanyContactPersonDetailsComponent implements OnInit, OnDestroy {
  public createOrEditCommunicationsData = '';
  public createOrEditAddressData = '';
  public createOrEditUserModuleAssignmentData = '';
  public isExpandableVisible = true;

  constructor(
    public contactPersonDetailSandbox: ContactPersonDetailsSandbox,
    public salutationSandbox: SalutationsSandbox,
    public personTypesSandbox: PersonTypesSandbox,
    public userModuleAssignmentSandBox: UserModuleAssignmentSandBox,
    private _translate: TranslateService
  ) {}

  ngOnInit() {
    this.contactPersonDetailSandbox.registerEvents();
    this._initExpandableState();
  }

  ngOnDestroy() {
    this.contactPersonDetailSandbox.companyContactId = null;
    this.contactPersonDetailSandbox.contactPersonContactId = null;
    this.contactPersonDetailSandbox.endSubscriptions();
  }

  public loadCommunicationsDataDetail(comm: CommunicationsData) {
    if (comm == null) {
      this.createOrEditCommunicationsData = this._translate.instant('CommunicationsData.NewCommunicationsData');
    } else {
      this.createOrEditCommunicationsData = this._translate.instant('CommunicationsData.EditCommunicationsData');
      this.contactPersonDetailSandbox.loadCommunicationsDataDetails(comm.contactId, comm.id);
    }
    this.contactPersonDetailSandbox.isCommunicationsDataDetailViewVisible = true;
  }

  public loadAddressDetail(address: Address) {
    if (address == null) {
      this.createOrEditAddressData = this._translate.instant('Address.NewAddress');
    } else {
      this.createOrEditAddressData = this._translate.instant('Address.EditAddress');
      this.contactPersonDetailSandbox.loadContactPersonDetailsAddressDetails(address.contactId, address.id);
    }
    this.contactPersonDetailSandbox.isAddressDataDetailViewVisible = true;
  }

  public loadUserModuleAssignmentDataDetail(userModuleAssignment: UserModuleAssignment) {
    if (userModuleAssignment == null) {
      this.createOrEditUserModuleAssignmentData = this._translate.instant('UserModuleAssignment.NewUserModuleAssignment');
    } else {
      this.createOrEditUserModuleAssignmentData = this._translate.instant('UserModuleAssignment.EditUserModuleAssignment');
      this.userModuleAssignmentSandBox.loadUserModuleAssignmentDetails(userModuleAssignment);
    }
    this.userModuleAssignmentSandBox.isUserModuleAssignmentDataDetailViewVisible = true;
  }

  private _initExpandableState() {
    this.isExpandableVisible = !!this.contactPersonDetailSandbox.contactPersonContactId;

    this.contactPersonDetailSandbox.isAddressDataDetailViewVisible = false;
    this.contactPersonDetailSandbox.isCommunicationsDataDetailViewVisible = false;
  }
}
