/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ContactPersonAddressDetailsComponent } from '@pages/company/company-details/contact-person-details/address-details/address-details.component';

describe('AddressDetailsComponent', () => {
  let component: ContactPersonAddressDetailsComponent;
  let contactPersonDetailsSandbox: any;
  let addressTypesSandbox: any;

  beforeEach(async(() => {
    contactPersonDetailsSandbox = {
      registerAddressEvents() {},
      registerCompanyEvents() {},
      endSubscriptions() {},
      clearAddressData() {},
    } as any;
  }));

  addressTypesSandbox = {};

  beforeEach(() => {
    component = new ContactPersonAddressDetailsComponent(contactPersonDetailsSandbox, addressTypesSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call registerAddressEvents and newAddressData onInit', () => {
    const spy1 = spyOn(contactPersonDetailsSandbox, 'registerAddressEvents');
    const spy2 = spyOn(contactPersonDetailsSandbox, 'clearAddressData');
    component.ngOnInit();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });
});
