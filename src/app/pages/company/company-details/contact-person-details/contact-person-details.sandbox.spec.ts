import { MarkAsTouchedAction } from 'ngrx-forms';
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ContactPersonDetailsSandbox } from '@pages/company/company-details/contact-person-details/contact-person-details.sandbox';
import { of } from 'rxjs';
import * as contactPersonActions from '@shared/store/actions/company/contact-person.actions';
import { CommunicationsData, Address, ContactPerson } from '@shared/models';
import { Globals } from '@shared/constants/globals';

describe('ContactPersonDetailsSandbox', () => {
  let component: ContactPersonDetailsSandbox;
  let utilService: any;
  let appState: any;
  let actionSubject: any;
  let router: any;
  let modalService: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi =  { setDomLayout() {} }
    router = { navigate() {} } as any;
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionSubject = { pipe: () => of(true) } as any;
    utilService = { displayNotification: () => {} } as any;
    modalService = { open() {} } as any;
  }));

  beforeEach(() => {
    component = new ContactPersonDetailsSandbox(appState, actionSubject, router, utilService, modalService);
    component.communicationsAgApi = agApi;
    component.addressAgApi = agApi;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigateBackToCompany', () => {
    const routerSpy = spyOn(router, 'navigate');
    component.navigateBackToCompany();
    expect(routerSpy).toHaveBeenCalledWith([`/${Globals.PATH.COMPANY}/${component.companyContactId}`]);
  });

  it('should call dispatch if load contact person details', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadContactPersonDetails('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: contactPersonActions.loadContactPersonDetail.type }));
  });

  it('should call dispatch if save a contact person', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearContactPerson');
    const spy2 = spyOn(component, 'navigateBackToCompany');
    const cp = new ContactPerson();
    cp.lastName = 'test';
    component.currentFormState = { ...component.currentFormState, isValid: true, value: cp };
    component.saveContactPersonDetails();
    expect(spy).toHaveBeenCalledWith(Object({ payload: cp, type: contactPersonActions.saveContactPersonDetail.type }));
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });

  it('should call error with displayNotification if for not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.currentFormState = { ...component.currentFormState, isValid: false };
    component.saveContactPersonDetails();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call register events and set the currentFormState', () => {
    component.registerEvents();
    expect(component.currentFormState).toBeDefined();
  });

  it('should call dispatch if load a company communications Data', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadCommunicationsDataDetails('ID', 'cID');
    expect(spy).toHaveBeenCalledWith(
      Object({ payload_contactId: 'ID', payload_communicationsId: 'cID', type: contactPersonActions.loadContactPersonDetailCommunicationsDataDetails.type })
    );
  });

  it('should can register communicationsData events and set the communicationsDataDetailsCurrentFormState', () => {
    component.registerCommunicationsDataEvents();
    expect(component.communicationsDataDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if clearCommunicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearCommunicationsData();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if new CommunicationsData are created', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearCommunicationsData');
    component.newCommunicationsData();
    expect(spy).toHaveBeenCalled();
    expect(spy1).toHaveBeenCalled();
  });

  it('should call dispatch if persist communicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearCommunicationsData');
    const companyCommunicationsData = new CommunicationsData();
    companyCommunicationsData.communicationTypeType = 'test';
    companyCommunicationsData.contactId = 'id';

    component.communicationsDataDetailsCurrentFormState = {
      ...component.communicationsDataDetailsCurrentFormState,
      isValid: true,
      value: companyCommunicationsData,
    };
    component.persistCommunicationsData();
    expect(spy).toHaveBeenCalledWith(Object({ payload: companyCommunicationsData, type: contactPersonActions.persistCommunicationsDataDetail.type }));
    expect(spy1).toHaveBeenCalled();
  });

  it('should call error with displayNotification if communicationsData form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.communicationsDataDetailsCurrentFormState = { ...component.communicationsDataDetailsCurrentFormState, isValid: false };
    component.persistCommunicationsData();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call clearCommunicationsData and negate isCommunicationsDataDetailViewVisible after call closeCommunicationsDataDetail', () => {
    const spy = spyOn(component, 'clearCommunicationsData');
    component.isCommunicationsDataDetailViewVisible = true;
    component.closeCommunicationsDataDetail();
    expect(spy).toHaveBeenCalled();
    expect(component.isCommunicationsDataDetailViewVisible).toBe(false);
  });

  it('should open modal before deleting an communicationsData', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.deleteCommunicationsData(new CommunicationsData());
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should call dispatch if load addresses', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadContactPersonAddresses('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: contactPersonActions.loadContactPersonDetailAddresses.type }));
  });

  it('should call dispatch if load a address details', () => {
    const spy = spyOn(appState, 'dispatch');
    component.companyContactId = 'contactId';
    component.loadContactPersonDetailsAddressDetails('ID', 'aID');
    expect(spy).toHaveBeenCalledWith(
      Object({ payload_contactId: 'ID', payload_addressId: 'aID', type: contactPersonActions.loadContactPersonDetailAddressDetails.type })
    );
  });

  it('should can register address events and set the addressDetailsCurrentFormState', () => {
    component.registerAddressEvents();
    expect(component.addressDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if clear ContactPerson', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearContactPerson();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if clearAddressData', () => {
    const spy = spyOn(appState, 'dispatch');
    (component as any).clearAddressData();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if persist a address', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearAddressData');
    const companyAddress = new Address();
    companyAddress.addressTypeType = 'test';
    companyAddress.contactId = 'id';

    component.addressDetailsCurrentFormState = { ...component.addressDetailsCurrentFormState, isValid: true, value: companyAddress };
    component.persistAddress();
    expect(spy).toHaveBeenCalledWith(Object({ payload: companyAddress, type: contactPersonActions.persistAddressDetail.type }));
    expect(spy1).toHaveBeenCalled();
  });

  it('should call error with displayNotification if address form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.addressDetailsCurrentFormState = { ...component.addressDetailsCurrentFormState, isValid: false };
    component.persistAddress();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call clearAddressData and negate isDetailViewVisible after call closeAddressDataDetail', () => {
    const spy = spyOn(component, 'clearAddressData');
    component.isAddressDataDetailViewVisible = true;
    component.closeAddressDataDetail();
    expect(spy).toHaveBeenCalled();
    expect(component.isAddressDataDetailViewVisible).toBe(false);
  });

  it('should open modal before deleting an address', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.deleteAddress(new Address());
    expect(modalService.open).toHaveBeenCalled();
  });
});
