/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import { Injectable, NgZone } from '@angular/core';
import * as companyActions from '@shared/store/actions/company/company.action';
import { FormGroupState, SetValueAction, ResetAction } from 'ngrx-forms';
import { Company, CommunicationsData, CommunicationType, ContactPerson, Address } from '@shared/models';
import { Observable } from 'rxjs';
import * as store from '@shared/store';
import { Store, ActionsSubject } from '@ngrx/store';
import * as companyDetailsFormReducer from '@shared/store/reducers/company/company-details-form.reducer';
import * as companyAddressDetailsFormReducer from '@shared/store/reducers/company/addresses-details-form.reducer';
import { ofType } from '@ngrx/effects';
import { UtilService } from '@shared/utility';
import { takeUntil, take, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as companyCommunicationsDataDetailsFormReducer from '@shared/store/reducers/company/communications-data-details-form.reducer';
import * as communicationTypesActions from '@shared/store/actions/admin/communication-types.action';
import { MarkAsTouchedAction } from 'ngrx-forms';
import { ILoadCompanyContactPersonsSuccess } from '@shared/store/actions/company/company.action';
import { Globals } from '@shared/constants/globals';
import * as contactPersonActions from '@shared/store/actions/company/contact-person.actions';

@Injectable()
export class CompanyDetailsSandBox extends BaseSandbox {
  public companyCurrentFormState: FormGroupState<Company>;
  public companyFormState$: Observable<FormGroupState<Company>> = this.appState$.select(store.getCompanyDetails);
  public communicationsDataDetailsFormState$: Observable<FormGroupState<CommunicationsData>> = this.appState$.select(store.getCompanyCommunicationsDataDetails);
  public communicationsDataDetailsCurrentFormState: FormGroupState<CommunicationsData>;
  public communicationsDataList$: Observable<Array<CommunicationsData>> = this.appState$.select(store.getCompanyCommunicationsDataData);
  public communicationsDataListLoading$: Observable<boolean> = this.appState$.select(store.getCompanyCommunicationsDataLoading);
  public addressDetailsFormState$: Observable<FormGroupState<Address>> = this.appState$.select(store.getCompanyAddressesDetails);
  public addressDetailsCurrentFormState: FormGroupState<Address>;
  public addressList$: Observable<Array<Address>> = this.appState$.select(store.getCompanyAddressesData);
  public addressListLoading$: Observable<boolean> = this.appState$.select(store.getCompanyAddressesLoading);
  public isCommunicationsDataDetailViewVisible: boolean = false;
  public isAddressDataDetailViewVisible: boolean = false;
  public existMainAddress = false;
  public isCurrentAddressMainAddress = false;
  public companyContactId: string;
  public contactPersonsList$: Observable<Array<ContactPerson>> = this.appState$.select(store.getCompanyContactPersonsData);
  public contactPersonsListLoading$: Observable<boolean> = this.appState$.select(store.getCompanyContactPersonsLoading);
  public communicationsAgApi;
  public addressAgApi;

  private _communicationTypes: Array<CommunicationType> = new Array<CommunicationType>();
  private _mainAddress: Address;

  /**
   *  Company Sandbox constructor
   */
  constructor(
    protected appState$: Store<store.State>,
    protected actionsSubject: ActionsSubject,
    protected utilService: UtilService,
    protected router: Router,
    protected modalService: NgbModal,
    private ngZone: NgZone
  ) {
    super(appState$);

    this.addressList$.subscribe(addresses => {
      this._checkIfMainAddressExist(addresses);
    });

    this.actionsSubject
      .pipe(
        ofType(companyActions.loadCompanyDetailAddressDetailsSuccess),
        map((action: { payload: Address }) => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((address: Address) => {
        this._checkIfCurrentAddressIsMainAddress(address);
      });
  }

  public loadCompany(id: string): void {
    this.companyContactId = id;
    this.appState$.dispatch(companyActions.loadCompanyDetail({ payload: id }));
  }

  public loadCompanyAddresses(companyId: string): void {
    this.appState$.dispatch(companyActions.loadCompanyDetailAddresses({ payload: companyId }));
  }

  public loadCompanyDetailsAddressDetails(addressId: string): void {
    this.appState$.dispatch(companyActions.loadCompanyDetailAddressDetails({ payload_contactId: this.companyContactId, payload_addressId: addressId }));
  }

  public persistCompany(): void {
    if (this.companyCurrentFormState.isValid) {
      const newCompany = new Company(this.companyCurrentFormState.value);

      this.appState$.dispatch(
        companyActions.persistCompanyDetail({
          payload: newCompany,
        })
      );
      this.actionsSubject.pipe(ofType(companyActions.persistCompanyDetailSuccess), take(1), takeUntil(this._endSubscriptions$)).subscribe(() => {
        this.clearCompany();
        this.router.navigateByUrl(`/overview`);
      });
    } else {
      this.utilService.displayNotification('MandatoryFieldsNotFilled', 'alert');
    }
  }

  public clearCompany(): void {
    this.appState$.dispatch(new SetValueAction(companyDetailsFormReducer.FORM_ID, companyDetailsFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(companyDetailsFormReducer.FORM_ID));

    this.appState$.dispatch(new MarkAsTouchedAction(companyDetailsFormReducer.FORM_ID));
  }

  public loadCommunicationsData(companyId: string): void {
    this.actionsSubject
      .pipe(
        ofType(communicationTypesActions.loadCommunicationTypesSuccess),
        map((action: communicationTypesActions.ILoadCommunicationTypesSuccess) => action.payload),
        take(1),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((payload: Array<CommunicationType>) => {
        this._communicationTypes = payload;
        this.appState$.dispatch(companyActions.loadCompanyDetailCommunicationsData({ payload: companyId }));
      });

    this.actionsSubject
      .pipe(
        ofType(companyActions.loadCompanyDetailCommunicationsDataSuccess),
        map((action: companyActions.ILoadCompanyCommunicationsDataSuccess) => action.payload),
        take(1),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((communicationsData: Array<CommunicationsData>) => {
        if (this._communicationTypes) {
          for (let i = 0; i < this._communicationTypes.length; i++) {
            const ct = this._communicationTypes[i];
            const existingCommunicationsData: CommunicationsData = communicationsData.find(cd => cd.communicationTypeId == ct.id);
            ct.isDisabled = existingCommunicationsData ? true : false;
          }
        }
      });
  }

  public loadCommunicationsDataDetails(communicationsDataId: string): void {
    this.appState$.dispatch(
      companyActions.loadCompanyDetailCommunicationsDataDetails({ payload_contactId: this.companyContactId, payload_communicationsId: communicationsDataId })
    );
  }

  public persistCommunicationsData(): void {
    if (this.communicationsDataDetailsCurrentFormState.isValid) {
      const newCommunicationsData = new CommunicationsData(this.communicationsDataDetailsCurrentFormState.value);
      newCommunicationsData.contactId = newCommunicationsData.contactId !== null ? newCommunicationsData.contactId : this.companyContactId;

      this.appState$.dispatch(companyActions.persistCommunicationsDataDetail({ payload: newCommunicationsData }));
      this.actionsSubject.pipe(ofType(companyActions.persistCommunicationsDataDetailSuccess), take(1), takeUntil(this._endSubscriptions$)).subscribe(() => {
        this.closeCommunicationsDataDetail();
      });
    } else {
      this.utilService.displayNotification('MandatoryFieldsNotFilled', 'alert');
    }
  }

  public deleteCommunicationsData(communicationsData: CommunicationsData): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(companyActions.deleteCommunicationsData({ payload: communicationsData }));
        this.closeCommunicationsDataDetail();
      },
      () => {}
    );
  }

  public closeCommunicationsDataDetail(): void {
    this.loadCommunicationsData(this.companyContactId);
    this.clearCommunicationsData();
    this.isCommunicationsDataDetailViewVisible = false;
    this.communicationsAgApi.setDomLayout('autoHeight');
  }

  public newCommunicationsData(): void {
    this.clearCommunicationsData();
    this.appState$.dispatch(new MarkAsTouchedAction(companyCommunicationsDataDetailsFormReducer.FORM_ID));
  }

  public clearCommunicationsData(): void {
    this.appState$.dispatch(
      new SetValueAction(companyCommunicationsDataDetailsFormReducer.FORM_ID, companyCommunicationsDataDetailsFormReducer.INITIAL_STATE.value)
    );
    this.appState$.dispatch(new ResetAction(companyCommunicationsDataDetailsFormReducer.FORM_ID));
  }

  public persistAddress(): void {
    if (this.addressDetailsCurrentFormState.isValid) {
      const newAddress = new Address(this.addressDetailsCurrentFormState.value);
      newAddress.contactId = newAddress.contactId !== null ? newAddress.contactId : this.companyContactId;

      this.appState$.dispatch(companyActions.persistAddressDetail({ payload: newAddress }));
      this.actionsSubject.pipe(ofType(companyActions.persistAddressDetailSuccess), take(1), takeUntil(this._endSubscriptions$)).subscribe(() => {
        this.closeAddressDataDetail();
        this.isAddressDataDetailViewVisible = false;
      });
    } else {
      this.utilService.displayNotification('MandatoryFieldsNotFilled', 'alert');
    }
  }

  public deleteAddress(address: Address): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(companyActions.deleteAddress({ payload: address }));
        if (address.isMainAddress) {
          this.existMainAddress = false;
        }
        this.closeAddressDataDetail();
      },
      () => {}
    );
  }

  public closeAddressDataDetail(): void {
    this.clearAddressData();
    this.isAddressDataDetailViewVisible = false;
    this.addressAgApi.setDomLayout('autoHeight');
  }

  public newAddressData(): void {
    this.clearAddressData();
    this.appState$.dispatch(new MarkAsTouchedAction(companyAddressDetailsFormReducer.FORM_ID));
  }

  public clearAddressData(): void {
    this.appState$.dispatch(new SetValueAction(companyAddressDetailsFormReducer.FORM_ID, companyAddressDetailsFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(companyAddressDetailsFormReducer.FORM_ID));
  }

  public registerCompanyEvents(): void {
    // subscribes to formState
    this.companyFormState$
      .pipe(takeUntil(this._endSubscriptions$))
      .subscribe((formState: FormGroupState<Company>) => (this.companyCurrentFormState = formState));
  }

  public registerCommunicationsDataEvents(): void {
    // subscribes to formState
    this.communicationsDataDetailsFormState$
      .pipe(takeUntil(this._endSubscriptions$))
      .subscribe((formState: FormGroupState<CommunicationsData>) => (this.communicationsDataDetailsCurrentFormState = formState));
  }

  public registerAddressEvents(): void {
    // subscribes to formState
    this.addressDetailsFormState$
      .pipe(takeUntil(this._endSubscriptions$))
      .subscribe((formState: FormGroupState<Address>) => (this.addressDetailsCurrentFormState = formState));
  }

  public loadContactPersons(contactId: string): Observable<ILoadCompanyContactPersonsSuccess> {
    this.appState$.dispatch(companyActions.loadCompanyDetailContactPersons({ payload: contactId }));
    return this.actionsSubject.pipe(ofType(companyActions.loadCompanyDetailContactPersonsSuccess), take(1));
  }

  public newContactPerson() {
    this.router.navigate([`${Globals.PATH.COMPANY}/${this.companyContactId}/${Globals.PATH.CONTACT_PERSON}/${Globals.PATH.NEW}`]);
  }

  public deleteContactPerson(contactPerson: ContactPerson): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(contactPersonActions.deleteContactPerson({ payload: contactPerson }));
      },
      () => {}
    );
  }

  removeCurrentMainAddress(): void {
    this._mainAddress.isMainAddress = false;
    this.appState$.dispatch(companyActions.persistAddressDetail({ payload: this._mainAddress }));
  }

  private _checkIfMainAddressExist(addresses: Array<Address>): void {
    for (let i = 0; i < addresses.length; i++) {
      const address = addresses[i];
      if (address.isMainAddress) {
        this._mainAddress = address;
        this.existMainAddress = true;
        break;
      }
      this._mainAddress = null;
      this.existMainAddress = false;
    }
  }

  private _checkIfCurrentAddressIsMainAddress(address: Address): void {
    this.isCurrentAddressMainAddress = address.isMainAddress;
  }

  public navigateTo(url: any) {
    this.ngZone.run(() => this.router.navigate([url]));
  }
}
