/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyContactPersonListComponent } from '@pages/company/company-details/contact-person-list/contact-person-list.component';
import { of } from 'rxjs';

describe('ContactPersonListComponent', () => {
  let component: CompanyContactPersonListComponent;
  const companyDetailsSandbox = {
    deleteContactPerson() {},
  } as any;
  const router = {
    navigate() {},
  };

  beforeEach(() => {
    component = new CompanyContactPersonListComponent(companyDetailsSandbox, router as any);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize gridOptions context', () => {
    component.ngOnInit();
    expect(component.gridOptions.context.icons.edit).toBeTruthy();
    expect(component.gridOptions.context.icons.delete).toBeTruthy();
  });

  it('should deleteContactPerson in response to delete event', () => {
    const spy = spyOn(companyDetailsSandbox, 'deleteContactPerson');
    const event: any = { type: 'delete', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });
});
