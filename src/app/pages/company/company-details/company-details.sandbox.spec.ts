/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { CompanyDetailsSandBox } from '@pages/company/company-details/company-details.sandbox';
import { of } from 'rxjs';
import * as companyActions from '@shared/store/actions/company/company.action';
import { Company, CommunicationsData, Address } from '@shared/models';

describe('CompanyDetailsSandBox', () => {
  let component: CompanyDetailsSandBox;
  let utilService: any;
  let appState: any;
  let actionSubject: any;
  let router: any;
  let modalService: any;
  let ngZone: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi =  { setDomLayout() {} }
    router = { navigateByUrl() {} } as any;
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionSubject = { pipe: () => of(true) } as any;
    utilService = { displayNotification: () => {} } as any;
    modalService = { open() {} } as any;
    ngZone = { run() {} } as any;
  }));

  beforeEach(() => {
    component = new CompanyDetailsSandBox(appState, actionSubject, utilService, router, modalService, ngZone);
    component.communicationsAgApi = agApi;
    component.addressAgApi = agApi;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dispatch if load a company', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadCompany('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: companyActions.loadCompanyDetail.type }));
  });

  it('should call dispatch if persist a company', () => {
    const spy = spyOn(appState, 'dispatch');
    const routerSpy = spyOn(router, 'navigateByUrl');
    const company = new Company();
    company.companyName = 'test';
    component.companyCurrentFormState = { ...component.companyCurrentFormState, isValid: true, value: company };
    component.persistCompany();
    expect(spy).toHaveBeenCalledWith(Object({ payload: company, type: companyActions.persistCompanyDetail.type }));
    expect(routerSpy).toHaveBeenCalled();
  });

  it('should call error with displayNotification if for not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.companyCurrentFormState = { ...component.companyCurrentFormState, isValid: false };
    component.persistCompany();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should can register events and set the currentFormState', () => {
    component.registerCompanyEvents();
    expect(component.companyCurrentFormState).toBeDefined();
  });

  it('should call dispatch if load a company communications Data', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadCommunicationsData('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: companyActions.loadCompanyDetailCommunicationsData.type }));
  });

  it('should call dispatch if load a company communicationsData details', () => {
    const spy = spyOn(appState, 'dispatch');
    component.companyContactId = 'contactId';
    component.loadCommunicationsDataDetails('ID');
    expect(spy).toHaveBeenCalledWith(
      Object({ payload_contactId: 'contactId', payload_communicationsId: 'ID', type: companyActions.loadCompanyDetailCommunicationsDataDetails.type })
    );
  });

  it('should can register communicationsData events and set the communicationsDataDetailsCurrentFormState', () => {
    component.registerCommunicationsDataEvents();
    expect(component.communicationsDataDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if clearCommunicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearCommunicationsData();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if persist an external person communicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearCommunicationsData');
    const companyCommunicationsData = new CommunicationsData();
    companyCommunicationsData.communicationTypeType = 'test';
    companyCommunicationsData.contactId = 'id';

    component.communicationsDataDetailsCurrentFormState = {
      ...component.communicationsDataDetailsCurrentFormState,
      isValid: true,
      value: companyCommunicationsData,
    };
    component.persistCommunicationsData();
    expect(spy).toHaveBeenCalledWith(Object({ payload: companyCommunicationsData, type: companyActions.persistCommunicationsDataDetail.type }));
    expect(spy1).toHaveBeenCalled();
  });

  it('should call error with displayNotification if communicationsData form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.communicationsDataDetailsCurrentFormState = { ...component.communicationsDataDetailsCurrentFormState, isValid: false };
    component.persistCommunicationsData();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call clearCommunicationsData and negate isCommunicationsDataDetailViewVisible after call closeCommunicationsDataDetail', () => {
    const spy = spyOn(component, 'clearCommunicationsData');
    component.isCommunicationsDataDetailViewVisible = true;
    component.closeCommunicationsDataDetail();
    expect(spy).toHaveBeenCalled();
    expect(component.isCommunicationsDataDetailViewVisible).toBe(false);
  });

  it('should open modal before deleting an communicationsData', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.deleteCommunicationsData(new CommunicationsData());
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should call dispatch if load a company addresses', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadCompanyAddresses('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: companyActions.loadCompanyDetailAddresses.type }));
  });

  it('should call dispatch if load a company address details', () => {
    const spy = spyOn(appState, 'dispatch');
    component.companyContactId = 'contactId';
    component.loadCompanyDetailsAddressDetails('ID');
    expect(spy).toHaveBeenCalledWith(
      Object({ payload_contactId: 'contactId', payload_addressId: 'ID', type: companyActions.loadCompanyDetailAddressDetails.type })
    );
  });

  it('should can register address events and set the addressDetailsCurrentFormState', () => {
    component.registerAddressEvents();
    expect(component.addressDetailsCurrentFormState).toBeDefined();
  });

  it('should call dispatch if clearCompany', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearCompany();
    expect(spy).toHaveBeenCalledTimes(3);
  });

  it('should call dispatch if clearAddressData', () => {
    const spy = spyOn(appState, 'dispatch');
    (component as any).clearAddressData();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if persist a company address', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearAddressData');
    const companyAddress = new Address();
    companyAddress.addressTypeType = 'test';
    companyAddress.contactId = 'id';

    component.addressDetailsCurrentFormState = { ...component.addressDetailsCurrentFormState, isValid: true, value: companyAddress };
    component.persistAddress();
    expect(spy).toHaveBeenCalledWith(Object({ payload: companyAddress, type: companyActions.persistAddressDetail.type }));
    expect(spy1).toHaveBeenCalled();
  });

  it('should call error with displayNotification if address form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.addressDetailsCurrentFormState = { ...component.addressDetailsCurrentFormState, isValid: false };
    component.persistAddress();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call clearAddressData and negate isDetailViewVisible after call closeAddressDataDetail', () => {
    const spy = spyOn(component, 'clearAddressData');
    component.isAddressDataDetailViewVisible = true;
    component.closeAddressDataDetail();
    expect(spy).toHaveBeenCalled();
    expect(component.isAddressDataDetailViewVisible).toBe(false);
  });

  it('should open modal before deleting an address', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.deleteAddress(new Address());
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should dispatch appropriate action to load company contact persons', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadContactPersons('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: companyActions.loadCompanyDetailContactPersons.type }));
  });

  it('should call clear and markastouched if newAddressData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.newAddressData();
    expect(spy).toHaveBeenCalledTimes(3);
    expect(component.isCurrentAddressMainAddress).toBeFalsy();
  });

  it('should call clear and markastouched if newCommunicationsData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.newCommunicationsData();
    expect(spy).toHaveBeenCalledTimes(3);
  });
});
