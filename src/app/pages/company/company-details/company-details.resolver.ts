/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyDetailsSandBox } from '@pages/company/company-details/company-details.sandbox';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { CommunicationTypesSandbox } from '@pages/admin/communication-types/communication-types.sandbox';
import { AddressTypesSandbox } from '@pages/admin/address-types/address-types.sandbox';
import { UserModuleAssignmentSandBox } from '@app/shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';

@Injectable()
export class CompanyDetailsResolver implements Resolve<any> {
  constructor(
    private companySandbox: CompanyDetailsSandBox,
    private userModuleAssignmentSandbox: UserModuleAssignmentSandBox,
    private communicationTypesSandbox: CommunicationTypesSandbox,
    private addressTypesSandbox: AddressTypesSandbox
  ) {}

  /**
   * @param route
   */
  public resolve(route: ActivatedRouteSnapshot): void {
    const contactId: string = route.params['contactId'];
    if (contactId && contactId !== 'new') {
      this.companySandbox.loadCompany(contactId);
      this.companySandbox.loadCommunicationsData(contactId);
      this.companySandbox.loadCompanyAddresses(contactId);
      this.companySandbox.loadContactPersons(contactId);
      this.userModuleAssignmentSandbox.loadUserModuleAssignments(contactId);
      this.userModuleAssignmentSandbox.loadFilteredUserModuleTypes();
    } else {
      this.companySandbox.clearCompany();
    }

    this.communicationTypesSandbox.loadCommunicationTypes();
    this.addressTypesSandbox.loadAddressTypes();

    this.companySandbox.isCommunicationsDataDetailViewVisible = false;
  }
}
