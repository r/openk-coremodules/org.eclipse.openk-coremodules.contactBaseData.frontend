/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { CompanyAddressListComponent } from '@pages/company/company-details/address-list/address-list.component';
import { of } from 'rxjs/observable/of';

describe('AddressListComponent', () => {
  let component: CompanyAddressListComponent;
  let companySandbox: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi =  { setDomLayout() {} }
    companySandbox = {
      registerAddressEvents() {},
      registerCompanyEvents() {},
      endSubscriptions() {},
      deleteAddress() {},
      newAddressData() {},
    } as any;
    companySandbox.addressAgApi = agApi;
    //companySandbox.communicationsAgApi = { setDomLayout() {} };
  }));

  beforeEach(() => {
    component = new CompanyAddressListComponent(companySandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define gridOptions onInit', () => {
    component.ngOnInit();

    expect(component.gridOptions).toBeDefined();
    expect(component.gridOptions.context).toBeDefined();
  });

  it('should emit if BusEvents is edit ', () => {
    const spy = spyOn(component.companyDetailsIdLoaded, 'emit');
    const event: any = { type: 'edit', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should deleteAddress if BusEvents is delete', () => {
    const spy = spyOn(companySandbox, 'deleteAddress');
    const event: any = { type: 'delete', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should call newAddressData and close edit area if callcreateNewAddressForm', () => {
    const spy = spyOn(companySandbox, 'newAddressData');
    const spyEmit = spyOn(component.createNewCompany, 'emit');
    component.createNewAddressForm();

    expect(spy).toHaveBeenCalled();
    expect(spyEmit).toHaveBeenCalled();
  });
});
