/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpService, GET, Path, Adapter, PUT, Body, DefaultHeaders, POST, DELETE } from '@shared/asyncServices/http';
import { Observable } from 'rxjs';
import { CompanyService } from '@pages/company/company.service';
import { Company, CommunicationsData, ContactPerson, Address } from '@shared/models';

@Injectable()
@DefaultHeaders({
  Accept: 'application/json',
  'Content-Type': 'application/json',
})
export class CompanyApiClient extends HttpService {
  /**
   * Retrieves company details by a given contactId
   *
   * @param contactId
   */
  @GET('/companies/{contactId}')
  @Adapter(CompanyService.companyDetailsAdapter)
  public getCompanyDetails(@Path('contactId') contactId: string): Observable<Company> {
    return null;
  }

  /**
   * Change the company details by a given contactId
   *
   * @param contactId
   * @param editedCompany
   */
  @PUT('/companies/{contactId}')
  @Adapter(CompanyService.companyDetailsAdapter)
  public putCompanyDetails(@Path('contactId') contactId: string, @Body() editedCompany: Company): Observable<Company> {
    return null;
  }

  /**
   * Saves new company details.
   *
   * @param newCompany
   */
  @POST('/companies')
  @Adapter(CompanyService.companyDetailsAdapter)
  public postCompanyDetails(@Body() newCompany: Company): Observable<Company> {
    return null;
  }

  /**
   * Retrieves communications data by a given contactId
   *
   * @param contactId
   */
  @GET('/contacts/{contactId}/communications')
  @Adapter(CompanyService.communicationsDataAdapter)
  public getCommunicationsData(@Path('contactId') contactId: string): Observable<CommunicationsData[]> {
    return null;
  }

  /**
   * Retrieves communications data details by a given contactId and communicationId
   *
   * @param contactId
   * @param communicationId
   */
  @GET('/contacts/{contactId}/communications/{communicationId}')
  @Adapter(CompanyService.communicationsDataDetailsAdapter)
  public getCommunicationsDataDetails(@Path('contactId') contactId: string, @Path('communicationId') communicationId: string): Observable<CommunicationsData> {
    return null;
  }

  /**
   * Change the communications data details by a given communicationId
   *
   * @param contactId
   * @param communicationId
   * @param editedCommunicationsData
   */
  @PUT('/contacts/{contactId}/communications/{communicationId}')
  @Adapter(CompanyService.communicationsDataDetailsAdapter)
  public putCommunicationsDataDetails(
    @Path('contactId') contactId: string,
    @Path('communicationId') communicationId: string,
    @Body() editedCommunicationsData: CommunicationsData
  ): Observable<CommunicationsData> {
    return null;
  }

  /**
   * Saves new communications details.
   *
   * @param contactId
   * @param newcommunicationsData
   */
  @POST('/contacts/{contactId}/communications')
  @Adapter(CompanyService.communicationsDataDetailsAdapter)
  public postCommunicationsDataDetails(
    @Path('contactId') contactId: string,
    @Body() newcommunicationsData: CommunicationsData
  ): Observable<CommunicationsData> {
    return null;
  }

  /**
   * Deletes by a given id
   *
   * @param contactId
   * @param communicationId
   */
  @DELETE('/contacts/{contactId}/communications/{communicationId}')
  @Adapter(CompanyService.communicationsDataDetailsAdapter)
  public deleteCommunicationsData(@Path('contactId') contactId: string, @Path('communicationId') communicationId: string): Observable<void> {
    return null;
  }

  /**
   * Retrieves contact details addresses by a given contactId
   *
   * @param contactId
   */
  @GET('/contacts/{contactId}/addresses')
  @Adapter(CompanyService.addressesAdapter)
  public getAddresses(@Path('contactId') contactId: string): Observable<Address[]> {
    return null;
  }

  /**
   * Retrieves contact details addresses details by a given contactId and addressId
   *
   * @param contactId
   */
  @GET('/contacts/{contactId}/addresses/{addressId}')
  @Adapter(CompanyService.addressesDetailsAdapter)
  public getAddressesDetails(@Path('contactId') contactId: string, @Path('addressId') addressId: string): Observable<Address> {
    return null;
  }

  /**
   * Change the address details by a given addressId
   *
   * @param addressId
   * @param editedAddress
   */
  @PUT('/contacts/{contactId}/addresses/{addressId}')
  @Adapter(CompanyService.addressesDetailsAdapter)
  public putAddressDetails(@Path('contactId') contactId: string, @Path('addressId') addressId: string, @Body() editedAddress: Address): Observable<Address> {
    return null;
  }

  /**
   * Saves new address details.
   *
   * @param newAddress
   */
  @POST('/contacts/{contactId}/addresses')
  @Adapter(CompanyService.addressesDetailsAdapter)
  public postAddressDetails(@Path('contactId') contactId: string, @Body() newExternalPersonAddress: Address): Observable<Address> {
    return null;
  }

  /**
   * Deletes by a given addressId
   *
   * @param id
   */
  @DELETE('/contacts/{contactId}/addresses/{addressId}')
  @Adapter(CompanyService.addressesDetailsAdapter)
  public deleteAddress(@Path('contactId') contactId: string, @Path('addressId') addressId: string): Observable<void> {
    return null;
  }

  /**
   * Retrieves contact persons by a given contactId
   *
   * @param contactId
   */
  @GET('/companies/{contactId}/contact-persons')
  @Adapter(CompanyService.contactPersonsAdapter)
  public getContactPersons(@Path('contactId') contactId: string): Observable<ContactPerson[]> {
    return null;
  }

  /**
   * Retrieves contact person details by a given contactId
   *
   * @param contactId
   */
  @GET('/contact-persons/{contactId}')
  @Adapter(CompanyService.contactPersonDetailsAdapter)
  public getContactPersonsDetails(@Path('contactId') contactId: string): Observable<ContactPerson> {
    return null;
  }
  /**
   * Save changed the contact person details by a given contactId
   *
   * @param contactId
   * @param person
   */
  @PUT('/contact-persons/{contactId}')
  @Adapter(CompanyService.contactPersonDetailsAdapter)
  public putContactPersonDetails(@Path('contactId') contactId: string, @Body() person: ContactPerson): Observable<ContactPerson> {
    return null;
  }
  /**
   * Saves new contact person details.
   *
   * @param person
   */
  @POST('/contact-persons')
  @Adapter(CompanyService.contactPersonDetailsAdapter)
  public postContactPersonDetails(@Body() person: ContactPerson): Observable<ContactPerson> {
    return null;
  }

  /**
   * Deletes contact person.
   */
  @DELETE('/contact-persons/{contactId}')
  @Adapter(CompanyService.contactPersonDetailsAdapter)
  public deleteContactPersonDetails(@Path('contactId') contactId: string): Observable<void> {
    return null;
  }
}
