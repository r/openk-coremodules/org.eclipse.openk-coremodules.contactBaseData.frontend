/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { LogoutPageSandbox } from '@pages/logout/logout/logout.sandbox';
import { of } from 'rxjs';

describe('LogoutPageSandbox', () => {
  let component: LogoutPageSandbox;
  let appState: any;
  let actionSubject: any;
  let router: any;

  beforeEach(() => {
    router = { navigateByUrl() {} } as any;
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionSubject = { pipe: () => of(true) } as any;

    component = new LogoutPageSandbox(appState, actionSubject, router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should clear storage, user and got o loggedout page if call logout', () => {
    const spy = spyOn(appState, 'dispatch');
    const spyClearStorage = spyOn(component, 'clearStorage');
    const spyRemoveUser = spyOn(component, 'removeUser');
    const spyNavigate = spyOn(router, 'navigateByUrl');

    component.logout();

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spyClearStorage).toHaveBeenCalled();
    expect(spyRemoveUser).toHaveBeenCalled();
    expect(spyNavigate).toHaveBeenCalledWith('/loggedout');
  });

  it('should call naviagetByUrl with /overview', () => {
    const spy = spyOn(router, 'navigateByUrl');
    component.goToOverview();

    expect(spy).toHaveBeenCalledWith('/overview');
  });
});
