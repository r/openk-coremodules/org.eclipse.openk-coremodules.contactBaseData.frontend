/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AdminModule } from '@pages/admin/admin.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpModule } from '@angular/http';
// Angular core modules
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule, APP_BASE_HREF, PlatformLocation } from '@angular/common';

// Routes
import { AppRoutingModule } from '@app/app-routing.module';

// Modules
import { AppComponent } from '@app/app.component';
import { HttpServiceModule } from '@shared/asyncServices/http/http.module';
import { UtilityModule } from '@shared/utility';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

//Feature Modules
import { ContactsModule } from '@pages/contacts/contacts.module';
import { LogoutModule } from '@pages/logout/logout.module';

// Store
import { adminReducers, companyReducers, personsReducers, reducers } from '@shared/store';

// Guards
import { AdminGuard } from '@shared/guards/admin.guard';

// Services
import { ConfigService } from './app-config.service';

// Third party libraries
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ContainersModule } from '@shared/containers';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { VersionModule } from './shared/components/version-info/version.module';

/**
 * Get the BaseHref which is defined in the index.html ( <base href="/"> ).
 * The "base href" in the index.html can be set via ng nuild for different enviroments.
 * Example: ng build --prod --base-href ../contactdatabase/
 */

export function getBaseHref(platformLocation: PlatformLocation): string {
  return platformLocation.getBaseHrefFromDOM();
}

/**
 * Calling functions or calling new is not supported in metadata when using AoT.
 * The work-around is to introduce an exported function.
 *
 * The reason for this limitation is that the AoT compiler needs to generate the code that calls the factory
 * and there is no way to import a lambda from a module, you can only import an exported symbol.
 */

export function configServiceFactory(config: ConfigService) {
  return () => config.load();
}
@NgModule({
  declarations: [AppComponent],
  imports: [
    // Angular core dependencies
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    CommonModule,
    // Third party modules
    AngularFontAwesomeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, 'i18n/', '.json'),
        deps: [HttpClient],
      },
    }),
    SimpleNotificationsModule.forRoot(),
    // App custom dependencies
    HttpServiceModule.forRoot(),
    UtilityModule.forRoot(),
    EffectsModule.forRoot([]),
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),

    StoreModule.forFeature('admin', adminReducers),
    StoreModule.forFeature('person', personsReducers),
    StoreModule.forFeature('company', companyReducers),
    ContainersModule,
    ContactsModule,
    AppRoutingModule,
    NgbModalModule,
    LogoutModule,
    VersionModule
  ],
  providers: [
    AdminGuard,
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [ConfigService],
      multi: true,
    },
    {
      provide: APP_BASE_HREF,
      useFactory: getBaseHref,
      deps: [PlatformLocation],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
