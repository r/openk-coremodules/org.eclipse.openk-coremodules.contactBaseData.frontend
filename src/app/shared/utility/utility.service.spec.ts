 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { UtilService } from '@shared/utility';

describe('UtilService', () => {
  let component: UtilService;
  let translateService: any;
  let notificationService: any;
  let configService: any;

  beforeEach(async(() => {
    translateService = {
      instant() {},
    } as any;

    notificationService = {
      info: () => ({}),
      error: () => ({}),
      success: () => ({}),
      alert: () => ({}),
    } as any;

    configService = {
      get: () => ({options: null}),
    } as any;
  }));

  beforeEach(() => {
    component = new UtilService(translateService, notificationService, configService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show info notifications after call displayNotification', () => {
    const messageTranslationCode = 'hello';
    const type = 'info';
    const titleTranslationCode = 'title';
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });

  it('should show error notifications after call displayNotification', () => {
    const messageTranslationCode = 'hello';
    const type = 'error';
    const titleTranslationCode = 'title';
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });

  it('should show success notifications after call displayNotification', () => {
    const messageTranslationCode = 'hello';
    const type = 'success';
    const titleTranslationCode = 'title';
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });

  it('should show alert notifications after call displayNotification', () => {
    const messageTranslationCode = 'hello';
    const type = 'alert';
    const titleTranslationCode = 'title';
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });

  it('should show notifications after call displayNotification without titleTranslationCode', () => {
    const messageTranslationCode = 'hello';
    const type = 'alert';
    const titleTranslationCode = null;
    const spyTranslation = spyOn(translateService, 'instant');

    component.displayNotification(messageTranslationCode, type, titleTranslationCode);
    expect(spyTranslation).toHaveBeenCalledWith(messageTranslationCode);
  });
});
