/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Router } from '@angular/router';
import { NgrxValueConverter, NgrxValueConverters } from 'ngrx-forms';

/**
 * Returns formated date based on given culture
 *
 * @param dateString
 * @param culture
 */
export function localeDateString(dateString: string, culture: string = 'en-EN'): string {
  let date = new Date(dateString);
  return date.toLocaleDateString(culture);
}

/**
 * Convert a date object into an ISO-String format
 */
export const dateValueConverter: NgrxValueConverter<any | null, string | null> = {
  convertViewToStateValue(value) {
    if (value === null) {
      return null;
    }

    // the value provided by the date picker is in local time but we want UTC so we recreate the date as UTC
    value = new Date(Date.UTC(value.year, value.month - 1, value.day));
    return NgrxValueConverters.dateToISOString.convertViewToStateValue(value);
  },
  convertStateToViewValue(value) {
    if (value === null) {
      return null;
    }

    const date = new Date(value);
    return { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
  },
};

export function navigateToOverview(router: Router): Promise<boolean> {
  return router.navigateByUrl('/overview');
}
