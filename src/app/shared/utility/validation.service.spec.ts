 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { ValidationService } from '@shared/utility';

describe('ValidationService', () => {
  let component: ValidationService;

  beforeEach(async(() => {
  }));

  beforeEach(() => {
    component = new ValidationService();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return null if the mail is valid', () => {
    let formControl: any;
    formControl = {value: 'test@test.de'};

    const retsult = component.validateEmail(formControl);
    expect(retsult).toBe(null);
  });

  it('should return error message if the mail is not valid', () => {
    let formControl: any;
    formControl = {value: 'test&test.de'};

    const retsult = component.validateEmail(formControl);
    expect(retsult).not.toBe(null);
  });

  it('should return null if the value is numeric', () => {
    let formControl: any;
    formControl = {value: 222};

    const retsult = component.numericRequired(formControl);
    expect(retsult).toBe(null);
  });

  it('should return error message if the value is not numeric', () => {
    let formControl: any;
    formControl = {value: 'test'};

    const retsult = component.numericRequired(formControl);
    expect(retsult).not.toBe(null);
  });
});
