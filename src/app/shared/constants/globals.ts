/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class Globals {
  static FRONTEND_VERSION = '1.1.1';

  static CONTACT_TYPE = class ContactType {
    static INTERNAL_PERSON = 'interne Person';
    static EXTERNAL_PERSON = 'externe Person';
    static COMPANY = 'Unternehmen';
    static CONTACT_PERSON = 'Ansprechpartner';
    static UNKNOWN = 'unbekannter Kontakttyp';
  };

  static CONTACT_TYPE_ID = class ContactTypeId {
    static CONTACT_PERSON = '1CP';
    static EXTERNAL_PERSON = '2EP';
    static INTERNAL_PERSON = '3IP';
    static COMPANY = '4CO';
  };

  static PATH = class Path {
    static PERSONS = 'persons';
    static EXTERNAL = 'external';
    static INTERNAL = 'internal';
    static CONTACT = 'contact';
    static COMPANY = 'company';
    static CONTACT_PERSON = 'contact-person';
    static NEW = 'new';
  };

  static LOCALE_TEXT = {
    contains: 'enthält',
    notContains: 'enthält nicht',
    startsWith: 'beginnt mit',
    endsWith: 'endet mit',
    andCondition: 'UND',
    orCondition: 'ODER',
    equals: 'ist gleich',
    notEqual: 'ist nicht gleich',
  };

  static SESSSION_STORAGE_KEYS = {
    contactListSearchFilterKey: 'contactListSearchFilter',
    contactListColumnStateKey: 'contactListColumnState',
  };
}
