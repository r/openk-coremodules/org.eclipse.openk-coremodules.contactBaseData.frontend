 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
/**
 * Pipe to transform a string to a date
 */
@Pipe({ name: 'stringToDate' })
export class StringToDatePipe implements PipeTransform {
    /**
     * Constructor
     */
    constructor() {
    }
    /**
     * Transform a date that is passed as string into a date
     * @param value The date passed as string
     * @returns {Date} The Date object
     */
    transform(value: string, format: string = ''): Date {
        if (value === null || value === undefined || value === '') {
            return null;
        }
        const ret = new Date(value);
        if (ret + '' === 'Invalid Date') {
            return null;
        } else {
            return ret;
        }
    }
}
