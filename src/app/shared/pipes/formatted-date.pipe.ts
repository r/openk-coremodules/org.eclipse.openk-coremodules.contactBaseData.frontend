 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';
/**
 * Pipe to transform a string to a date
 */
@Pipe({name: 'formattedDate'})
export class FormattedDatePipe implements PipeTransform {
    /**
     * Constructor
     */
    constructor() {
    }
    /**
     * Transform a date that is passed as string into a date
     * @param value The date passed as string
     * @param format The date passed as string
     */
   transform(value: any, format: string = ''): string {
        // Try and parse the passed value.
        if ( value === undefined) {
            return null;
        }
        const momentDate = moment(value, 'DD.MM.YYYY');

        // If moment didn't understand the value, return it unformatted.
        if (!momentDate.isValid()) {
            return value;
        }

        // Otherwise, return the date formatted as requested.
        if (format) {
            return momentDate.format(format);
        }
        return momentDate.format('DD.MM.YYYY');
    }
}
