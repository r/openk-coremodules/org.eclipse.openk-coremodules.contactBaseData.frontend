 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { TestBed } from '@angular/core/testing';
import { StringToDatePipe } from './string-to-date.pipe';

describe('Test StringToDatePipe ', () => {
    const pipe = new  StringToDatePipe();

  it('should perform correct with different inputs ', () => {
      expect( pipe.transform('xxx') ).toBeNull();
      expect( pipe.transform( null) ).toBeNull();
      expect( pipe.transform( '' ) ).toBeNull();
      expect( pipe.transform( undefined ) ).toBeNull();

      const dateTest = pipe.transform( '2017-11-19T13:16:06');
      expect( dateTest.getFullYear() ).toBe( 2017 );
      expect( dateTest.getMonth() ).toBe( 10 ); // month in jscript is zero based!
      expect( dateTest.getDate() ).toBe( 19 );
      expect( dateTest.getMinutes() ).toBe( 16 );
      expect( dateTest.getSeconds() ).toBe( 6 );
  });
});
