 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'formattedTimestamp'
})
export class FormattedTimestampPipe implements PipeTransform {
    transform(value: any, format: string = ''): string {

        if (value === '') {
            return '';
        }

        if (value === 'xxx') {
            return null;
        }

        if (value === null) {
            return '';
        }

        if (value === undefined) {
            return '';
        }

        // Try and parse the passed value.
        const momentDate = moment(value);

        // If moment didn't understand the value, return it unformatted.
        if (!momentDate.isValid()) {
            return value;
        }

        // Otherwise, return the date formatted as requested.
        if (format) {
            return momentDate.format(format);
        }
        return momentDate.format('DD.MM.YYYY HH:mm');
    }
}
