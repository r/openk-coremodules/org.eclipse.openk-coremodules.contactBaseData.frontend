 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
	Pipe,
	PipeTransform
} 							from '@angular/core';
import {
	DomSanitizer,
	SafeHtml
}    						from '@angular/platform-browser';

@Pipe({
  name: 'sanitizeHtml'
})
export class SanitizeHtmlPipe implements PipeTransform  {
  constructor(private sanitizer: DomSanitizer){}

  transform(v: string) : SafeHtml {
    return this.sanitizer.bypassSecurityTrustHtml(v);
  }
}
