 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { FormattedDatePipe } from './formatted-date.pipe';

describe('Test FormattedDatePipe ', () => {
    const pipe = new FormattedDatePipe();

    it('should perform correct with different inputs ', () => {
        expect(pipe.transform('xxx')).toBe('xxx');
        expect(pipe.transform(null)).toBe(null);
        expect(pipe.transform('')).toBe('');
        expect(pipe.transform(undefined)).toBeNull();

        const dateTest = pipe.transform('19.11.2017 13:16');
        expect(dateTest.match('[0-3][0-9]\.[0-1][0-9]\.[0-9]{4}')).toBeTruthy();

    });
});
