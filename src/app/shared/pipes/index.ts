/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { SanitizeHtmlPipe } from '@shared/pipes/sanitizeHtml.pipe';
import { StringToDatePipe } from '@shared/pipes/string-to-date.pipe';
import { FormattedTimestampPipe } from '@shared/pipes/formatted-timestamp.pipe';
import { UniquePipe } from '@shared/pipes/unique.pipe';
import { FormattedDatePipe } from '@shared/pipes/formatted-date.pipe';
import { NgbDateCustomParserFormatter } from '@shared/pipes/ngb-date-custom-parser-formatter';

export const PIPES = [UniquePipe, SanitizeHtmlPipe, StringToDatePipe, StringToDatePipe, FormattedDatePipe, FormattedTimestampPipe];

@NgModule({
  imports: [],
  declarations: PIPES,
  providers: [NgbDateCustomParserFormatter],
  exports: PIPES,
})
export class PipesModule {}
