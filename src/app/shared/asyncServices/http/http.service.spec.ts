 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { HttpService } from '.';
import { async, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ConfigService } from '@app/app-config.service';
import { of } from 'rxjs/observable/of';
import { HttpAdapter } from './http.adapter';

describe('HttpService', () => {


  let responseHandlerMock: any;
  let httpMock: any;
  let configserviceMock: any;  

  let httpService: HttpService;
  beforeEach(() => {
    
    configserviceMock = { get:()=> ( { baseUrl: 'testBaseUrl'} ) } as any;
    httpService = new HttpService(httpMock, configserviceMock, responseHandlerMock);

    });

  it('should call getBaseUrl', () => {      
    const spy1 = spyOn(configserviceMock, 'get').and.callThrough();
    (httpService as any).getBaseUrl();            
    expect(spy1).toHaveBeenCalled();
  });

  it('should call getDefaultHeaders', () => {          
    const spy1 = spyOn((httpService as any), 'getDefaultHeaders').and.callThrough();          
    let response = (httpService as any).getDefaultHeaders();          
    expect(response).toBeNull();
    expect(spy1).toHaveBeenCalled();
  });

  it('should call getDefaultHeaders', () => {       
    const observableRes = of( {} as any );
    const spy1 = spyOn((httpService as any), 'responseInterceptor').and.callThrough();          
    const spy2 = spyOn(HttpAdapter, 'baseAdapter');          
    let observable = (httpService as any).responseInterceptor(observableRes);
    expect(spy1).toHaveBeenCalled();
  });

});
