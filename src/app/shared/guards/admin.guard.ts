/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as store from '@shared/store';
import { User } from '@shared/models/user';
import { PermissionsModel } from '@shared/models/permissions.model';
import { takeUntil, take } from 'rxjs/operators';

@Injectable()
export class AdminGuard implements CanActivate {
  protected _endSubscriptions$: Subject<boolean> = new Subject();
  constructor(private _router: Router, private _appState$: Store<store.State>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this._checkAdminRight();
  }

  private _checkAdminRight(): Observable<boolean> {
    const isAdmin$: Observable<boolean> = this._appState$
      .select(store.getUser)
      .pipe(takeUntil(this._endSubscriptions$), take(1))
      .map((user: User) => new PermissionsModel(user.roles).admin);
    isAdmin$.subscribe(isAdmin => !isAdmin && this._router.navigate(['/overview']));
    return isAdmin$.pipe(take(1));
  }
}
