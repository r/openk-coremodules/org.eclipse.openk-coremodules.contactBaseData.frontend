 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit } from '@angular/core';
import { AgFilterComponent } from 'ag-grid-angular';
import { IDoesFilterPassParams, IFilterParams, RowNode, RowDataChangedEvent } from 'ag-grid-community';

@Component({
  selector: 'filter-cell',
  templateUrl: './set-filter.component.html',
  styleUrls: ['./set-filter.component.css'],
})
export class SetFilterComponent implements AgFilterComponent {
  private _params: IFilterParams;
  private _valueGetter: (rowNode: RowNode) => any;
  public setItems: any = {};
  public gridHeight: number;
  public filterText: string = '';
  public selectAllChecked = true;
  private filtererdItems: any = {};
  filterCheckboxList(text: string) {
    text
      .toLowerCase()
      .split(' ')
      .forEach(filterWord => {
        for (var key in this.setItems) {
          if (
            key
              .toString()
              .toLowerCase()
              .indexOf(filterWord) < 0
          ) {
            this.filtererdItems[key] = this.setItems[key];
            delete this.setItems[key];
          } else {
            for (var k in this.filtererdItems) {
              if (
                k
                  .toString()
                  .toLowerCase()
                  .indexOf(filterWord) >= 0
              ) {
                this.setItems[k] = this.filtererdItems[k];
              }
            }
          }
        }
      });
    this._params.filterChangedCallback();
  }

  notifyFilter(event: Event) {
    this.setItems[event.target['id']].checked = event.target['checked'];
    let allSelected = true;
    for (var k in this.setItems) {
      if (!this.setItems[k].checked) {
        allSelected = this.setItems[k].checked;
      }
    }
    this.selectAllChecked = allSelected;
    this._params.filterChangedCallback();
  }
  isFilterActive(): boolean {
    return !this.selectAllChecked;
  }
  doesFilterPass(params: IDoesFilterPassParams): boolean {
    const itemKey = this._valueGetter(params.node);
    return this.setItems[itemKey] !== undefined && this.setItems[itemKey].checked;
  }
  getModel() {
    return { values: this.setItems };
  }
  setModel(model: any): void {
    this.setItems = model ? model.values : [];
  }

  agInit(params: IFilterParams): void {
    this._params = params;
    this._valueGetter = params.valueGetter;
    const allRows = params.api.getModel()['rowsToDisplay'] || [];
    const setItems = allRows.map(row => {
      return this._valueGetter(row);
    });
    setItems.forEach(title => {
      this.setItems[title] = { visible: true, checked: true };
    });

    this._params.api['gridOptionsWrapper'].gridOptions = {
      ...this._params.api['gridOptionsWrapper'].gridOptions,
      onGridSizeChanged: (event: any) => {
        this.gridHeight = event.clientHeight;
      },
      onRowDataChanged: (event: RowDataChangedEvent) => {
        const setItems = (event.api.getModel()['rowsToDisplay'] || []).map(row => {
          return this._valueGetter(row);
        });
        setItems.forEach(title => {
          this.setItems[title] = !this.setItems[title].checked ? this.setItems[title] : { ...this.setItems[title].checked, checked: true };
        });
      },
    };
  }
  public selectAll(event: Event) {
    this.filterText = '';
    for (var k in this.filtererdItems) {
      this.setItems[k] = this.filtererdItems[k];
    }
    for (var k in this.setItems) {
      if (this.setItems.hasOwnProperty(k)) {
        this.setItems[k].checked = event.target['checked'];
      }
    }
    this.selectAllChecked = event.target['checked'];
    this._params.filterChangedCallback();
  }
  constructor() {}
}
