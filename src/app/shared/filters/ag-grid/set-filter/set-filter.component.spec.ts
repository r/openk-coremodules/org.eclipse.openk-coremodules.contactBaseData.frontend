 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
 import { async, ComponentFixture, TestBed } from '@angular/core/testing';
 import { SetFilterComponent } from './set-filter.component';
import { SystemIntegratorERPPage } from '../../../../../../e2e/app.po';

 describe('SetFilterComponent', () => {
   let component: SetFilterComponent;


   beforeEach(() => {
       component = new SetFilterComponent();

   });

   it('should create', () => {
     expect(component).toBeTruthy();
   });

   it('should filterCheckboxList with setItems', () => {
    component.setItems = ['AAA', 'BBB'];
    const compAnonym = component as any;
    compAnonym._params = { filterChangedCallback() { }};
    component.filterCheckboxList('X');

    expect(compAnonym.filtererdItems[0]).toBe('AAA');

    component.setItems = ['AAA', 'BBB'];
    component.filterCheckboxList('0 b');
    expect(compAnonym.filtererdItems[0]).toBe('AAA');

  });

  it('should return isFilterActive', () => {
    component.selectAllChecked = false;
    expect(component.isFilterActive()).toBeTruthy();
  });
  
  it('should return model', () => {
    component.setItems = 666;
    expect(component.getModel().values).toBe( 666 );
  });
     
  it('should set model 1', () => {
    component.setItems = 666;
    component.setModel(null);
    expect(component.setItems).toEqual( [] );
  });
     
  it('should set model 2', () => {
    component.setItems = 666;
    component.setModel( { values: 333 });
    expect(component.setItems).toEqual( 333 );
  });  

 });
