 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SetFilterComponent } from '@shared/filters/ag-grid/set-filter/set-filter.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [SetFilterComponent],
  exports:[SetFilterComponent],
  imports: [
    FormsModule,
    CommonModule,
  ]
})
export class FiltersModule { }
