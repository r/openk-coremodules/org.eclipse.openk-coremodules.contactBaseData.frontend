/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, Output, Input, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { navigateToOverview } from '@shared/utility';
import { Globals } from '@shared/constants/globals';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  @Input() userImage: string;
  @Input() userName: string;

  @Output() logout: EventEmitter<any> = new EventEmitter();

  constructor(public router: Router) {}

  public navigateToOverview(): void {
    // reset contacts state
    sessionStorage.clear();

    navigateToOverview(this.router).then(this._winLocReload);
  }

  public navigateToAdminPage(): void {
    this.router.navigate(['/admin']);
  }

  private _winLocReload(): void {
    window.location.reload();
  }
}
