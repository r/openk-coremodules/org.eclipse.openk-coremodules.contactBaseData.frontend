/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { IconCellRendererComponent } from '@shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { BoolCellRendererComponent } from '@shared/components/cell-renderer/bool-cell-renderer/bool-cell-renderer.component';

export const ADDRESS_LIST_COLDEF = [
  {
    field: 'addressTypeType',
    colId: 'addressTypeType',
    headerName: 'Address.AddressType',
    sortable: true,
    flex: 3
  },
  {
    field: 'isMainAddress',
    colId: 'isMainAddress',
    headerName: 'Address.MainAddress',
    sortable: true,
    cellRendererFramework: BoolCellRendererComponent,
    flex: 1,
    minWidth: 100
  },
  {
    field: 'postcode',
    colId: 'postcode',
    headerName: 'Address.Postcode',
    sortable: true,
    flex: 2
  },
  {
    field: 'community',
    colId: 'community',
    headerName: 'Address.Community',
    sortable: true,
    flex: 4
  },
  {
    field: 'communitySuffix',
    colId: 'communitySuffix',
    headerName: 'Address.CommunitySuffix',
    sortable: true,
    flex: 4
  },
  {
    field: 'street',
    colId: 'street',
    headerName: 'Address.Street',
    sortable: true,
    flex: 4
  },
  {
    field: 'housenumber',
    colId: 'housenumber',
    headerName: 'Address.Housenumber',
    sortable: true,
    flex: 1,
    minWidth: 100
  },
  {
    field: 'note',
    colId: 'note',
    headerName: 'Address.Note',
    sortable: true,
    flex: 4
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 110,
    minWidth: 110,
    lockPosition: true,
    sortable: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    cellRendererFramework: IconCellRendererComponent,
  },
];
