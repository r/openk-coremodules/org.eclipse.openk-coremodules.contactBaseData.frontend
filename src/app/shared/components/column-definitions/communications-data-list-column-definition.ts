/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { IconCellRendererComponent } from '@shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';

export const COMMUNICATIONS_DATA_LIST_COLDEF = [
  {
    field: 'communicationTypeType',
    colId: 'communicationTypeType',
    headerName: 'CommunicationsData.CommunicationType',
    sortable: true,
    minWidth: 120,
    maxWidth: 320,
    flex: 1
  },
  {
    field: 'communicationData',
    colId: 'communicationData',
    headerName: 'CommunicationsData.CommunicationData',
    sortable: true,
    minWidth: 50,
    flex: 3
  },
  {
    field: 'note',
    colId: 'note',
    headerName: 'CommunicationsData.Note',
    sortable: true,
    minWidth: 50,
    flex: 3
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    minWidth: 120,
    maxWidth: 120,
    lockPosition: true,
    suppressSizeToFit: true,
    suppressMenu: true,
    sortable: false,
    cellRendererFramework: IconCellRendererComponent,
  },
];
