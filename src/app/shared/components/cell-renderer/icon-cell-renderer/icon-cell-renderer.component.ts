/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';
import { CommunicationType } from '@shared/models';

@Component({
  selector: 'app-icon-cell-renderer',
  templateUrl: './icon-cell-renderer.component.html',
  styleUrls: ['./icon-cell-renderer.component.scss'],
})
export class IconCellRendererComponent implements AgRendererComponent {
  public editIcon = null;
  public deleteIcon = null;
  public readonlyIcon = null;
  public editFlag = null;

  public params: any;

  public agInit(params: ICellRendererParams): void {
    this.params = params;
    this.editFlag = params.data['editable'];
    if (params.context && params.context.icons) {
      const contextIcons = params.context.icons;
      if (params.data instanceof CommunicationType && this.editFlag !== null) {
        // communication types
        this.editIcon = !contextIcons || !this.editFlag ? false : !!contextIcons.edit;
        this.readonlyIcon = !contextIcons || this.editFlag ? false : !!contextIcons.readonly;
        this.deleteIcon = !contextIcons || !this.editFlag ? false : !!contextIcons.delete;
      } else {
        // default case
        this.editIcon = !!contextIcons && !!contextIcons.edit;
        this.readonlyIcon = !!contextIcons && !!contextIcons.readonly;
        this.deleteIcon = !!contextIcons && !!contextIcons.delete;
      }
    }
  }

  public refresh(): boolean {
    return false;
  }

  public clicked(eventType: string) {
    this.params.context.eventSubject.next({ type: eventType, data: this.params.data });
  }
}
