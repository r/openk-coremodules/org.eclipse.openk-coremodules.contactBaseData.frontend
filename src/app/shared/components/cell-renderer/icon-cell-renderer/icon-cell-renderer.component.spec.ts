import { CommunicationType } from '@shared/models';
 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { IconCellRendererComponent } from '@shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { ICellRendererParams } from 'ag-grid-community';

describe('IconCellRendererComponent', () => {
  let component: IconCellRendererComponent;

  beforeEach(() => {
    component = new IconCellRendererComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return false when calling refesh', () => {
    expect(component.refresh()).toBeFalsy();
  });

  it('should set icon properties appropriate when calling agInit with default params  + icons undefined', () => {
    let p: ICellRendererParams = {data: {editable: true},
                                  context: {icons: {}}
                                 } as any;
    component.agInit(p);
    expect(component.editIcon).toBeFalsy();
    expect(component.readonlyIcon).toBeFalsy();
    expect(component.deleteIcon).toBeFalsy();
  });

  it('should set icon properties appropriate when calling agInit with default params + icons true', () => {
    let p: ICellRendererParams = {data: {editable: true},
                                  context: {icons: {edit: true, readonly: true, delete: true}}
                                 } as any;
    component.agInit(p);
    expect(component.editIcon).toBeTruthy();
    expect(component.readonlyIcon).toBeTruthy();
    expect(component.deleteIcon).toBeTruthy();
  });

  it('should set icon properties appropriate when calling agInit with CommunicationType params + icons undefined', () => {
    let p: ICellRendererParams = {data: new CommunicationType({editable: true}),
                                  context: {icons: {}}
                                 } as any;
    component.agInit(p);
    expect(component.editIcon).toBeFalsy();
    expect(component.readonlyIcon).toBeFalsy();
    expect(component.deleteIcon).toBeFalsy();
  });

  it('should set icon properties appropriate when calling agInit with CommunicationType params + editable true', () => {
    let p: ICellRendererParams = {data: new CommunicationType({editable: true}),
                                  context: {icons: {edit: true, readonly: true, delete: true}}
                                 } as any;
    component.agInit(p);
    expect(component.editIcon).toBeTruthy();
    expect(component.readonlyIcon).toBeFalsy();
    expect(component.deleteIcon).toBeTruthy();
  });

  it('should set icon properties appropriate when calling agInit with CommunicationType params + editable false', () => {
    let p: ICellRendererParams = {data: new CommunicationType({editable: false}),
                                  context: {icons: {edit: true, readonly: true, delete: true}}
                                 } as any;
    component.agInit(p);
    expect(component.editIcon).toBeFalsy();
    expect(component.readonlyIcon).toBeTruthy();
  });

});
