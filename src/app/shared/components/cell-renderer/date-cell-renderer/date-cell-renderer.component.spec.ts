/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DateCellRendererComponent } from '@shared/components/cell-renderer/date-cell-renderer/date-cell-renderer.component';
import { ICellRendererParams } from 'ag-grid-community';

describe('DateCellRendererComponent', () => {
  let component: DateCellRendererComponent;

  beforeEach(() => {
    component = new DateCellRendererComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set date / isDate when calling agInit with Date param', () => {
    let p: ICellRendererParams = { value: new Date() } as any;
    component.agInit(p);
    expect(component.date).toBe(p.value);
    expect(component.isDate).toBeTruthy();
  });

  it('should set date / isDate when calling refesh with Date param', () => {
    let p: ICellRendererParams = { value: new Date() } as any;
    component.refresh(p);
    expect(component.date).toBe(p.value);
    expect(component.isDate).toBeTruthy();
  });
});
