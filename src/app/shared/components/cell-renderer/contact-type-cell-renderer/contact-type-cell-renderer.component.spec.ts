/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Globals } from '@shared/constants/globals';
import { ContactTypeCellRendererComponent } from './contact-type-cell-renderer.component';

describe('ContactTypeCellRendererComponent', () => {
  let component: ContactTypeCellRendererComponent;
  let fixture: ComponentFixture<ContactTypeCellRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ContactTypeCellRendererComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactTypeCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component).toBeDefined();
  });

  it('should display EXTERNAL_PERSON for 2EP value', () => {
    component.agInit({ value: '2EP' });
    expect(component.contactType).toEqual(Globals.CONTACT_TYPE.EXTERNAL_PERSON);
  });

  it('should display INTERNAL_PERSON for 3IP value', () => {
    component.agInit({ value: '3IP' });
    expect(component.contactType).toEqual(Globals.CONTACT_TYPE.INTERNAL_PERSON);
  });

  it('should display COMPANY for 4CO value', () => {
    component.agInit({ value: '4CO' });
    expect(component.contactType).toEqual(Globals.CONTACT_TYPE.COMPANY);
  });

  it('should display CONTACT_PERSON for 1CP value', () => {
    component.agInit({ value: '1CP' });
    expect(component.contactType).toEqual(Globals.CONTACT_TYPE.CONTACT_PERSON);
  });

  it('should display UNKNOWN for Unknown value', () => {
    component.agInit({ value: 'X' });
    expect(component.contactType).toEqual(Globals.CONTACT_TYPE.UNKNOWN);
  });
});
