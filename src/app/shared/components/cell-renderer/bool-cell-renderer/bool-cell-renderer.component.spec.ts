/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BoolCellRendererComponent } from './bool-cell-renderer.component';
import { ICellRendererParams } from 'ag-grid-community';

describe('BoolCellRendererComponent', () => {
  let component: BoolCellRendererComponent;

  beforeEach(() => {
    component = new BoolCellRendererComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return false when calling refesh', () => {
    expect(component.refresh()).toBeFalsy();
  });

  it('should set params', () => {
    let p: ICellRendererParams = { data: 'x' } as any;
    component.agInit(p);
    expect(component.params).toBe(p);
  });
});
