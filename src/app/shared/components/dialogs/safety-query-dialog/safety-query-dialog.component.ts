 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-safety-query-dialog',
  templateUrl: './safety-query-dialog.component.html',
  styleUrls: ['./safety-query-dialog.component.scss']
})
export class SafetyQueryDialogComponent implements OnInit {

  @Input() title;
  @Input() warning1;
  @Input() warning2;
  @Input() showExclamationMarkIcon;

  constructor(public activeModal: NgbActiveModal) {}

   ngOnInit() {
  }

}
