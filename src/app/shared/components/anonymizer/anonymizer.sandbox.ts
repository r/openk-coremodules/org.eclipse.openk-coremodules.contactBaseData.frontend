/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import { Store, ActionsSubject } from '@ngrx/store';
import * as store from '@shared/store';
import * as contactsActions from '@shared/store/actions/contacts.action';
import { ofType } from '@ngrx/effects';
import { takeUntil, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { Observable } from 'rxjs';
import { InternalPerson, UserModuleAssignment } from '@app/shared/models';

@Injectable()
export class AnonymizerSandbox extends BaseSandbox {
  public userModuleAssignmentsDataDetails$: Observable<Array<UserModuleAssignment>> = this.appState$.select(store.getUserModuleAssignmentsData);
  private _userModuls: UserModuleAssignment[] = [];

  constructor(protected appState$: Store<store.State>, protected actionsSubject: ActionsSubject, protected router: Router, protected modalService: NgbModal) {
    super(appState$);
  }
  public init() {
    this.userModuleAssignmentsDataDetails$.subscribe((userModuls: UserModuleAssignment[]) => {
      this._userModuls = userModuls;
    });
    this.actionsSubject.pipe(ofType(contactsActions.anonymizeContactSuccess), takeUntil(this._endSubscriptions$)).subscribe(() => {
      this.router.navigateByUrl(`/overview`);
    });
  }

  public anonymizeContact(contactId: string): void {
    let noExclamationMark: boolean = true;

    this._userModuls.forEach(item => console.log(item.deletionLockUntil));
    noExclamationMark = this._userModuls.every(this.hasNoActiveAssignments);

    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.anonymize';
    if (noExclamationMark) {
      modalRef.componentInstance.warning2 = 'ConfirmDialog.AnonymizationWarning';
      modalRef.componentInstance.showExclamationMarkIcon = false;
    } else {
      modalRef.componentInstance.warning1 = 'ConfirmDialog.AssignmentWarning';
      modalRef.componentInstance.warning2 = 'ConfirmDialog.AnonymizationWarning';
      modalRef.componentInstance.showExclamationMarkIcon = true;
    }

    modalRef.result.then(
      () => {
        this.appState$.dispatch(contactsActions.anonymizeContact({ payload: contactId }));
      },
      () => {}
    );
  }

  private hasNoActiveAssignments(element, index, array) {
    if(element.deletionLockUntil == null){
        return true;
    }
    else{
      let dateNow: number = Date.now();
      let dateUntil: number = Date.parse(element.deletionLockUntil)
      return dateUntil < dateNow;
    }
  }
}
