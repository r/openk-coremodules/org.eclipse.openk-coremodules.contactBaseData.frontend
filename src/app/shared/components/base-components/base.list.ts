/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridOptions } from 'ag-grid-community';
import { Subject } from 'rxjs';

export abstract class BaseList {
  public events$: Subject<any> = new Subject();
  public noRowsTemplate: string;
  public loadingTemplate: string;

  public gridOptions: GridOptions = {
    context: {
      eventSubject: this.events$,
    },
    defaultColDef: {
      filter: false,
    },
    suppressLoadingOverlay: true,
  };

  protected pageSize: number = 3;

  constructor() {
    this.noRowsTemplate = `<span>Keine Einträge</span>`;
  }
}
