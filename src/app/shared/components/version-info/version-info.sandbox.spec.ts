 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Store, ActionsSubject } from '@ngrx/store';
import { State } from '@shared/store';
import { async } from '@angular/core/testing';
import { VersionSandbox } from './version-info.sandbox';
import * as versionActions from '@shared/store/actions/version/version.action';
import { of } from 'rxjs';

describe('VersionInfoSandbox', () => {
  let appState: Store<State>;
  let actionSubject: ActionsSubject;
  let component: VersionSandbox;

  beforeEach(async(() => {
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionSubject = { pipe: () => of(true) } as any;
    spyOn(appState, 'dispatch').and.callFake(() => {});

  }));

  beforeEach(() => {
    component = new VersionSandbox(appState, actionSubject);
  });

  it('should create VersionInfoSandbox', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch loadVersion Action via loadVersion()', () => {
    component.loadVersion();
    expect(appState.dispatch).toHaveBeenCalledWith(versionActions.version());
  });

});
