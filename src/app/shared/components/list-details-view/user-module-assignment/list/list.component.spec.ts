/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { UserModuleAssignmentDataListComponent } from '@shared/components/list-details-view/user-module-assignment/list/list.component';
import { of } from 'rxjs/observable/of';

describe('ListComponent', () => {
  let component: UserModuleAssignmentDataListComponent;
  let userModuleAssignmentSandBox: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi =  { setDomLayout() {} }
    userModuleAssignmentSandBox = {
      registerUserModuleAssignmentEvents() {},
      deleteUserModuleAssignment() {},
      clearUserModuleAssignmentData() {},
    } as any;
    userModuleAssignmentSandBox.userModuleAgApi = agApi;
  }));

  beforeEach(() => {
    component = new UserModuleAssignmentDataListComponent(userModuleAssignmentSandBox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define gridOptions onInit', () => {
    component.ngOnInit();

    expect(component.gridOptions).toBeDefined();
    expect(component.gridOptions.context).toBeDefined();
  });

  it('should emit if BusEvents is edit ', () => {
    const spy = spyOn(component.userModuleAssignmentDetailsLoaded, 'emit');
    const event: any = { type: 'edit', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should deleteUserModuleAssignment if BusEvents is delete', () => {
    const spy = spyOn(userModuleAssignmentSandBox, 'deleteUserModuleAssignment');
    const event: any = { type: 'delete', data: { id: 'id' } };
    component.gridOptions.context.eventSubject = of(event);
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should clearUserModuleAssignmentData and close edit area if callcreateNewUserModuleAssignmentForm', () => {
    const spy = spyOn(userModuleAssignmentSandBox, 'clearUserModuleAssignmentData');
    const spyEmit = spyOn(component.createNewUserModuleAssignment, 'emit');
    component.createNewUserModuleAssignmentForm();

    expect(spy).toHaveBeenCalled();
    expect(spyEmit).toHaveBeenCalled();
  });
});
