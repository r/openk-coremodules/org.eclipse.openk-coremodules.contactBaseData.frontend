/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { UserModuleAssignmentDataDetailsComponent } from '@shared/components/list-details-view/user-module-assignment/details/details.component';

describe('DetailsComponent', () => {
  let component: UserModuleAssignmentDataDetailsComponent;
  let userModuleAssignmentSandbox: any;

  beforeEach(async(() => {
    userModuleAssignmentSandbox = {
      registerUserModuleAssignmentEvents() {},
      newUserModuleAssignmentData() {},
    } as any;
  }));

  beforeEach(() => {
    component = new UserModuleAssignmentDataDetailsComponent(userModuleAssignmentSandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call registerUserModuleAssignmentEvents and clearUserModuleAssignment onInit', () => {
    const spy1 = spyOn(userModuleAssignmentSandbox, 'registerUserModuleAssignmentEvents');
    const spy2 = spyOn(userModuleAssignmentSandbox, 'newUserModuleAssignmentData');
    component.ngOnInit();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });
});
