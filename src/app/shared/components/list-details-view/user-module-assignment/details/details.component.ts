/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-module-assignment-data-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class UserModuleAssignmentDataDetailsComponent implements OnInit {
  @Input() createOrEditUserModuleAssignmentData: string;

  constructor(public userModuleAssignmentSandbox: UserModuleAssignmentSandBox) {}

  ngOnInit() {
    this.userModuleAssignmentSandbox.registerUserModuleAssignmentEvents();
    this.userModuleAssignmentSandbox.newUserModuleAssignmentData();
  }

  reset(resetProperty: string) {
    this.userModuleAssignmentSandbox.clearUserModuleAssignmentPropertyData(resetProperty);
  }
}
