/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';

import { of } from 'rxjs';
import * as userModuleAssignmentActions from '@shared/store/actions/user-module-assignment/user-module-assignment.action';
import { UserModuleAssignment } from '@shared/models';

describe('UserModuleAssignmentSandBox', () => {
  let component: UserModuleAssignmentSandBox;
  let utilService: any;
  let appState: any;
  let actionSubject: any;
  let router: any;
  let modalService: any;
  let agApi: any;

  beforeEach(async(() => {
    agApi =  { setDomLayout() {} }
    router = { navigateByUrl() {} } as any;
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionSubject = { pipe: () => of(true) } as any;
    utilService = { displayNotification: () => {} } as any;
    modalService = { open() {} } as any;
  }));

  beforeEach(() => {
    component = new UserModuleAssignmentSandBox(appState, actionSubject, utilService, router, modalService);
    component.userModuleAgApi = agApi;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dispatch if load user module assignment Data', () => {
    const spy = spyOn(appState, 'dispatch');
    component.loadUserModuleAssignments('ID');
    expect(spy).toHaveBeenCalledWith(Object({ payload: 'ID', type: userModuleAssignmentActions.loadUserModuleAssignments.type }));
  });

  it('should call dispatch if load user module assignment Data details', () => {
    const spy = spyOn(appState, 'dispatch');
    const exampleUserModuleAssignment: UserModuleAssignment = {
      id: 'contactId',
      contactId: '',
      modulName: '',
      assignmentDate: '2021-01-19T00:00:00.000+0000',
      expiringDate: '2021-01-19T00:00:00.000+0000',
      deletionLockUntil: '',
      assignmentNote: '',
    };
    component.loadUserModuleAssignmentDetails(exampleUserModuleAssignment);
    expect(spy).toHaveBeenCalledWith(Object({ payload: exampleUserModuleAssignment, type: userModuleAssignmentActions.loadUserModuleAssignmentDetails.type }));
  });

  it('should can register userModuleAssignmentData events and set the currentFormState', () => {
    component.registerUserModuleAssignmentEvents();
    expect(component.currentFormState).toBeDefined();
  });

  it('should call dispatch if clearUserModuleAssignmentData', () => {
    const spy = spyOn(appState, 'dispatch');
    component.clearUserModuleAssignmentData();
    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call dispatch if persist an user module assignment data', () => {
    const spy = spyOn(appState, 'dispatch');
    const spy1 = spyOn(component, 'clearUserModuleAssignmentData');
    const userModuleAssignmentData = new UserModuleAssignment();
    userModuleAssignmentData.assignmentNote = 'test';
    userModuleAssignmentData.contactId = 'id';

    component.currentFormState = { ...component.currentFormState, isValid: true, value: userModuleAssignmentData };
    component.persistUserModuleAssignment();
    expect(spy).toHaveBeenCalledWith(Object({ payload: userModuleAssignmentData, type: userModuleAssignmentActions.persistUserModuleAssignmentDetail.type }));
    expect(spy1).toHaveBeenCalled();
  });

  it('should call error with displayNotification if userModuleAssignmentData form not valid', () => {
    const utilSpy = spyOn(utilService, 'displayNotification');
    component.currentFormState = { ...component.currentFormState, isValid: false };
    component.persistUserModuleAssignment();
    expect(utilSpy).toHaveBeenCalled();
  });

  it('should call clearUserModuleAssignmentData and negate isUserModuleAssignmentDataDetailViewVisible after call closeUserModuleAssignmentDataDetail', () => {
    const spy = spyOn(component, 'clearUserModuleAssignmentData');
    component.isUserModuleAssignmentDataDetailViewVisible = true;
    component.closeUserModuleAssignmentDataDetail();
    expect(spy).toHaveBeenCalled();
    expect(component.isUserModuleAssignmentDataDetailViewVisible).toBe(false);
  });

  it('should open modal before deleting an userModuleAssignmentData', () => {
    spyOn(component['modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    component.deleteUserModuleAssignment(new UserModuleAssignment());
    expect(modalService.open).toHaveBeenCalled();
  });
});
