/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpService, GET, Path, Adapter, PUT, Body, DefaultHeaders, POST, DELETE, Query } from '@shared/asyncServices/http';
import { Observable } from 'rxjs';
import { PersonsService } from '@pages/persons/persons.service';
import { Address, UserModuleAssignment } from '@shared/models';
import { UserModuleAssignmentsService } from './user-module-assignments.service';
import { UserModuleType } from '@app/shared/models/user-module-assignment/user-module-type.model';

@Injectable()
@DefaultHeaders({
  Accept: 'application/json',
  'Content-Type': 'application/json',
})
export class UserModuleAssignmentApiClient extends HttpService {
  /**
   * Retrieves user module assignments by a given contactId
   *
   * @param contactId
   */
  @GET('/contacts/{contactId}/assignments')
  @Adapter(UserModuleAssignmentsService.userModuleAssignmentsAdapter)
  public getUserModuleAssignments(@Path('contactId') contactId: string): Observable<UserModuleAssignment[]> {
    return null;
  }

  /**
   * Retrieves user module assignment details by a given contactId and assignmentId
   *
   * @param contactId
   */
  @GET('/contacts/{contactId}/assignments/{assignmentId}')
  @Adapter(UserModuleAssignmentsService.userModuleAssignmentDetailsAdapter)
  public getUserModuleAssignmentsDetails(@Path('contactId') contactId: string, @Path('assignmentId') assignmentId: string): Observable<UserModuleAssignment> {
    return null;
  }

  /**
   * Change the user module assignment details by a given contactId
   *
   * @param contactId
   * @param editedUserModuleAssignment
   */
  @PUT('/contacts/{contactId}/assignments/{assignmentId}')
  @Adapter(UserModuleAssignmentsService.userModuleAssignmentDetailsAdapter)
  public putUserModuleAssignmentDetails(
    @Path('contactId') contactId: string,
    @Path('assignmentId') assignmentId: string,
    @Body() editedUserModuleAssignment: UserModuleAssignment
  ): Observable<UserModuleAssignment> {
    return null;
  }

  /**
   * Saves new user module assignment details.
   *
   * @param newUserModuleAssignment
   */
  @POST('/contacts/{contactId}/assignments')
  @Adapter(UserModuleAssignmentsService.userModuleAssignmentDetailsAdapter)
  public postUserModuleAssignmentDetails(
    @Path('contactId') contactId: string,
    @Body() newUserModuleAssignment: UserModuleAssignment
  ): Observable<UserModuleAssignment> {
    return null;
  }

  /**
   * Deletes by a given id
   *
   * @param id
   */
  @DELETE('/contacts/{contactId}/assignments/{assignmentId}')
  @Adapter(UserModuleAssignmentsService.userModuleAssignmentDetailsAdapter)
  public deleteUserModuleAssignment(@Path('contactId') contactId: string, @Path('assignmentId') assignmentId: string): Observable<void> {
    return null;
  }

  /**
   * Retrieves user module types data
   *
   */
  @GET('/user/modules')
  @Adapter(UserModuleAssignmentsService.userModuleTypesAdapter)
  public getUserModuleTypes(): Observable<UserModuleType[]> {
    return null;
  }
}
