/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { UserModuleAssignmentsService } from '@shared/components/list-details-view/user-module-assignment/user-module-assignments.service';
import { UserModuleAssignment, UserModuleType } from '@shared/models';

describe('UserModuleAssignmentService', () => {
  beforeEach(() => {});

  it('should transform user module assignment details', () => {
    const userModuleAssigment: any = { modulName: 'Heysterkamp' };
    const transContact = UserModuleAssignmentsService.userModuleAssignmentDetailsAdapter(userModuleAssigment);

    expect(transContact.modulName).toBe(userModuleAssigment.modulName);
  });

  it('should transform user module assignment list', () => {
    const response = [new UserModuleAssignment()];
    response[0].id = 'Herr';

    expect(UserModuleAssignmentsService.userModuleAssignmentsAdapter(response)[0].id).toBe('Herr');
  });

  it('should transform user module type list', () => {
    const response = [new UserModuleType()];
    response[0].color = 'red';

    expect(UserModuleAssignmentsService.userModuleTypesAdapter(response)[0].color).toBe('red');
  });
});
