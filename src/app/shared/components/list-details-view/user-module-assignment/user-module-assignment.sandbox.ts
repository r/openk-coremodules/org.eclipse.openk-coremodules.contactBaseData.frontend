/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BaseSandbox } from '@shared/sandbox/base.sandbox';
import { Injectable, Input } from '@angular/core';
import * as userModuleAssignmentActions from '@shared/store/actions/user-module-assignment/user-module-assignment.action';
import * as userModuleAssignmentDetailsFormReducer from '@shared/store/reducers/user-module-assignment/user-module-assignment-details-form.reducer';
import { FormGroupState, NgrxValueConverter } from 'ngrx-forms';
import { UserModuleAssignment, UserModuleType } from '@shared/models';
import { Observable } from 'rxjs';
import * as store from '@shared/store';
import { Store, ActionsSubject } from '@ngrx/store';
import { dateValueConverter, UtilService } from '@shared/utility';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SetValueAction, ResetAction } from 'ngrx-forms';
import { ofType } from '@ngrx/effects';
import { takeUntil, take, map } from 'rxjs/operators';
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { MarkAsTouchedAction } from 'ngrx-forms';
import { Moment } from 'moment';

@Injectable()
export class UserModuleAssignmentSandBox extends BaseSandbox {
  public formState$: Observable<FormGroupState<UserModuleAssignment>> = this.appState$.select(store.getUserModuleAssignmentDetails);
  public currentFormState: FormGroupState<UserModuleAssignment>;
  public userModuleAssignmentList$: Observable<Array<UserModuleAssignment>> = this.appState$.select(store.getUserModuleAssignmentsData);
  public userModuleAssignmentListLoading$: Observable<boolean> = this.appState$.select(store.getUserModuleAssignmentsLoading);
  public userModuleTypes$ = this.appState$.select(store.getUserModuleTypesData);

  public isUserModuleAssignmentDataDetailViewVisible: boolean = false;
  public contactId: string;
  public userModuleAgApi;

  private _userModuleTypes: Array<UserModuleType> = new Array<UserModuleType>();

  /**
   *  UserModuleAssignment Sandbox constructor
   */
  constructor(
    protected appState$: Store<store.State>,
    protected actionsSubject: ActionsSubject,
    protected utilService: UtilService,
    protected router: Router,
    protected modalService: NgbModal
  ) {
    super(appState$);
    this.loadFilteredUserModuleTypes();
  }

  public dateValueConverter: NgrxValueConverter<Date | null | Moment, string | null> = dateValueConverter;

  public loadUserModuleAssignments(contactId: string): void {
    this.contactId = contactId;
    this.actionsSubject
      .pipe(
        ofType(userModuleAssignmentActions.loadFilteredUserModuleTypesSuccess),
        map((action: userModuleAssignmentActions.ILoadUserModuleTypesSuccess) => action.payload),
        take(1),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((payload: Array<UserModuleType>) => {
        this._userModuleTypes = payload;
        this.appState$.dispatch(userModuleAssignmentActions.loadUserModuleAssignments({ payload: contactId }));
      });

    this.actionsSubject
      .pipe(
        ofType(userModuleAssignmentActions.loadUserModuleAssignmentsSuccess),
        map((action: userModuleAssignmentActions.ILoadUserModuleAssignmentSuccess) => action.payload),
        take(1),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((userModuleAssignmentData: Array<UserModuleAssignment>) => {
        if (this._userModuleTypes) {
          for (let i = 0; i < this._userModuleTypes.length; i++) {
            const umt = this._userModuleTypes[i];
            const existingUserModuleAssignmentsData: UserModuleAssignment = userModuleAssignmentData.find(umad => umad.modulName == umt.id);
            umt.isDisabled = existingUserModuleAssignmentsData ? true : false;
          }
        }
      });
  }

  public loadUserModuleAssignmentDetails(userModuleAssignment: UserModuleAssignment): void {
    this.appState$.dispatch(userModuleAssignmentActions.loadUserModuleAssignmentDetails({ payload: userModuleAssignment }));
  }

  public persistUserModuleAssignment(): void {
    if (this.currentFormState.isValid) {
      const newUserModuleAssignment = new UserModuleAssignment(this.currentFormState.value);
      newUserModuleAssignment.contactId = newUserModuleAssignment.contactId !== null ? newUserModuleAssignment.contactId : this.contactId;

      this.appState$.dispatch(userModuleAssignmentActions.persistUserModuleAssignmentDetail({ payload: newUserModuleAssignment }));
      this.actionsSubject
        .pipe(ofType(userModuleAssignmentActions.persistUserModuleAssignmentDetailSuccess), take(1), takeUntil(this._endSubscriptions$))
        .subscribe(() => {
          this.closeUserModuleAssignmentDataDetail();
        });
    } else {
      this.utilService.displayNotification('MandatoryFieldsNotFilled', 'alert');
    }
  }

  public deleteUserModuleAssignment(userModuleAssignment: UserModuleAssignment): void {
    const modalRef = this.modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.appState$.dispatch(userModuleAssignmentActions.deleteUserModuleAssignment({ payload: userModuleAssignment }));
        this.closeUserModuleAssignmentDataDetail();
      },
      () => {}
    );
  }

  public closeUserModuleAssignmentDataDetail(): void {
    this.loadUserModuleAssignments(this.contactId);
    this.clearUserModuleAssignmentData();
    this.isUserModuleAssignmentDataDetailViewVisible = false;
    this.userModuleAgApi.setDomLayout('autoHeight');
  }

  public newUserModuleAssignmentData(): void {
    this.clearUserModuleAssignmentData();

    this.appState$.dispatch(new MarkAsTouchedAction(userModuleAssignmentDetailsFormReducer.FORM_ID));
  }

  public clearUserModuleAssignmentData(): void {
    this.appState$.dispatch(new SetValueAction(userModuleAssignmentDetailsFormReducer.FORM_ID, userModuleAssignmentDetailsFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(userModuleAssignmentDetailsFormReducer.FORM_ID));
  }

  public clearUserModuleAssignmentPropertyData(property: string): void {
    switch (property) {
      case 'deletionLockUntil':
        this.appState$.dispatch(new SetValueAction(userModuleAssignmentDetailsFormReducer.INITIAL_STATE.controls.deletionLockUntil.id, null));
        break;
      case 'expiringDate':
        this.appState$.dispatch(new SetValueAction(userModuleAssignmentDetailsFormReducer.INITIAL_STATE.controls.expiringDate.id, null));
      default:
        break;
    }
  }

  public registerUserModuleAssignmentEvents(): void {
    // subscribes to formState
    this.formState$
      .pipe(takeUntil(this._endSubscriptions$))
      .subscribe((formState: FormGroupState<UserModuleAssignment>) => (this.currentFormState = formState));
  }

  public loadFilteredUserModuleTypes() {
    this.appState$.dispatch(userModuleAssignmentActions.loadFilteredUserModuleTypes({ payload: 'Kontaktstammdaten' }));
    return this.actionsSubject.pipe(ofType(userModuleAssignmentActions.loadFilteredUserModuleTypesSuccess), take(1));
  }
}
