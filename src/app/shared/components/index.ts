/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { SafetyQueryDialogComponent } from '@shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { BoolCellRendererComponent } from '@shared/components/cell-renderer/bool-cell-renderer/bool-cell-renderer.component';
import { IconCellRendererComponent } from '@shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { ContactTypeCellRendererComponent } from '@shared/components/cell-renderer/contact-type-cell-renderer/contact-type-cell-renderer.component';
import { PaginatorComponent } from '@shared/components/paginator/paginator.component';
import { LoadingSpinnerComponent } from '@shared/components/loading-spinner/loading-spinner.component';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { PipesModule } from '@shared/pipes';
import { TranslateModule } from '@ngx-translate/core';
import { SpinnerComponent } from '@shared/components/spinner/spinner.component';
import { HeaderComponent } from '@shared/components/header/header.component';
import { PageNotFoundComponent } from '@shared/components/pageNotFound/pageNotFound.component';
import { LoadingPlaceholderComponent } from '@shared/components/loadingPlaceholder/loadingPlaceholder.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ExpandableComponent } from '@shared/components/expandable/expandable.component';
import { UserModuleAssignmentDataDetailsComponent } from '@shared/components/list-details-view/user-module-assignment/details/details.component';
import { UserModuleAssignmentDataListComponent } from '@shared/components/list-details-view/user-module-assignment/list/list.component';
import { UserModuleAssignmentApiClient } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment-api-client';
import { UserModuleAssignmentSandBox } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment.sandbox';
import { UserModuleAssignmentsService } from '@shared/components/list-details-view/user-module-assignment/user-module-assignments.service';
import { StoreModule } from '@ngrx/store';
import { userModuleAssignmentReducers } from '@shared/store';
import { EffectsModule } from '@ngrx/effects';
import { UserModuleAssignmentEffect } from '@shared/store/effects/user-module-assignment/user-module-assignment.effect';
import { AgGridModule } from 'ag-grid-angular';
import { NgrxFormsModule } from 'ngrx-forms';
import { DirectivesModule } from '@shared/directives/index';
import { NgbDatepickerModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateCustomParserFormatter } from '@shared/pipes/ngb-date-custom-parser-formatter';
import { DateCellRendererComponent } from '@shared/components/cell-renderer/date-cell-renderer/date-cell-renderer.component';
import { AnonymizerComponent } from '@shared/components/anonymizer/anonymizer.component';
import { AnonymizerSandbox } from '@shared/components/anonymizer/anonymizer.sandbox';
import { VersionInfo } from './version-info/version-info.component';

export const COMPONENTS = [
  SpinnerComponent,
  HeaderComponent,
  PageNotFoundComponent,
  LoadingPlaceholderComponent,
  VersionInfo,
  LoadingSpinnerComponent,
  PaginatorComponent,
  ContactTypeCellRendererComponent,
  IconCellRendererComponent,
  SafetyQueryDialogComponent,
  ExpandableComponent,
  SafetyQueryDialogComponent,
  BoolCellRendererComponent,
  UserModuleAssignmentDataDetailsComponent,
  UserModuleAssignmentDataListComponent,
  DateCellRendererComponent,
  AnonymizerComponent,
];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    DirectivesModule,
    ReactiveFormsModule,
    RouterModule,
    NgrxFormsModule,
    FormsModule,
    AngularFontAwesomeModule,
    NgbDatepickerModule,
    AgGridModule.withComponents([]),
    StoreModule.forFeature('userModuleAssignmentData', userModuleAssignmentReducers),
    EffectsModule.forFeature([UserModuleAssignmentEffect]),

    PipesModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  entryComponents: [
    PaginatorComponent,
    ContactTypeCellRendererComponent,
    IconCellRendererComponent,
    SafetyQueryDialogComponent,
    BoolCellRendererComponent,
    DateCellRendererComponent,
  ],
  providers: [
    UserModuleAssignmentSandBox,
    UserModuleAssignmentApiClient,
    UserModuleAssignmentsService,
    DatePipe,
    { provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter },
    AnonymizerSandbox,
  ],
})
export class ComponentsModule {}
