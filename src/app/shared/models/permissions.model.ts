/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class PermissionsModel {
  public reader: boolean = false;
  public writer: boolean = false;
  public admin: boolean = false;

  /**
   *Creates an instance of Permission.
   * @param {*} [data=null]
   * @memberof Permission
   */
  public constructor(data: string[] = null) {
    if (!!data) {
      data.forEach((permissionItem: string) => {
        switch (permissionItem) {
          case 'kon-admin':
            this.admin = true;
            break;
          case 'kon-writer':
            this.writer = true;
            break;
          case 'kon-reader':
            if (!(this.admin || this.writer)) {
              this.reader = true;
            }
            break;
          default:
            break;
        }
      });
    }
  }
}
