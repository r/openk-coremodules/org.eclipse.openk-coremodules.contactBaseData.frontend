 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class Contact {

  public id: string = null;
  public name: string = null;
  public firstName: string = null;
  public lastName: string = null;
  public email: string = null;
  public contactType: string = null;
  public note: string = null;
  public street: string = null;
  public housenumber: string = null;
  public community: string = null;
  public companyName: string = null;
  public companyType: string = null;
  public fkContactId: number = null;
  public fkSalutationId: number = null;
  public salutationType: string = null;
  public department: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }
}
