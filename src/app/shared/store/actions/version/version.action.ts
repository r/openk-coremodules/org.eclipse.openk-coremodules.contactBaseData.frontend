/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';

export interface IVersionSuccess {
  payload: string;
}

export interface IVersionFail {
  payload: string;
}
export const version = createAction('[Version] Version');
export const versionSuccess = createAction('[Version] Version Success', props<IVersionSuccess>());
export const versionFail = createAction('[Version] Version Fail', props<IVersionFail>());
