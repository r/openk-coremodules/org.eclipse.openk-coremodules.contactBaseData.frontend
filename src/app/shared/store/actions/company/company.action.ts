/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { Company, CommunicationsData, ContactPerson, Address } from '@shared/models';

export interface ILoadCompanyCommunicationsDataSuccess {
  payload: Array<CommunicationsData>;
}

export interface ILoadCompanyCommunicationsDataFail {
  payload: string;
}

export interface ILoadCompanyContactPersonsSuccess {
  payload: Array<ContactPerson>;
}

export interface ILoadCompanyContactPersonsFail {
  payload: string;
}

export interface ILoadCompanyAddressesSuccess {
  payload: Array<Address>;
}

export interface ILoadCompanyAddressesFail {
  payload: string;
}

export const loadCompanyDetail = createAction('[Company Details] Load', props<{ payload: string }>());

export const loadCompanyDetailSuccess = createAction('[Company Details] Load Success', props<{ payload: Company }>());

export const loadCompanyDetailFail = createAction('[Company Details] Load Fail', props<{ payload: string }>());

export const persistCompanyDetail = createAction('[Company Details] Persist', props<{ payload: Company }>());

export const persistCompanyDetailSuccess = createAction('[Company Details] Persist Success', props<{ payload: Company }>());

export const persistCompanyDetailFail = createAction('[Company Details] Persist Fail', props<{ payload: string }>());

export const loadCompanyDetailCommunicationsData = createAction('[Company CommunicationsData] Load', props<{ payload: string }>());
export const loadCompanyDetailCommunicationsDataSuccess = createAction(
  '[Company CommunicationsData] Load Success',
  props<ILoadCompanyCommunicationsDataSuccess>()
);
export const loadCompanyDetailCommunicationsDataFail = createAction('[Company CommunicationsData] Load Fail', props<ILoadCompanyCommunicationsDataFail>());

export const loadCompanyDetailCommunicationsDataDetails = createAction(
  '[Company CommunicationsData Details] Load',
  props<{ payload_contactId: string; payload_communicationsId: string }>()
);
export const loadCompanyDetailCommunicationsDataDetailsSuccess = createAction(
  '[Company CommunicationsData Details] Load Success',
  props<{ payload: CommunicationsData }>()
);
export const loadCompanyDetailCommunicationsDataDetailsFail = createAction('[Company CommunicationsData Details] Load Fail', props<{ payload: string }>());

export const persistCommunicationsDataDetail = createAction('[Company CommunicationsData Details] Persist', props<{ payload: CommunicationsData }>());
export const persistCommunicationsDataDetailSuccess = createAction(
  '[Company CommunicationsData Details] Persist Success',
  props<{ payload: CommunicationsData }>()
);
export const persistCommunicationsDataDetailFail = createAction('[Company CommunicationsData Details] Persist Fail', props<{ payload: string }>());

export const deleteCommunicationsData = createAction('[Company CommunicationsData Details] Delete', props<{ payload: CommunicationsData }>());
export const deleteCommunicationsDataSuccess = createAction('[Company CommunicationsData Details] Delete Success');
export const deleteCommunicationsDataFail = createAction('[Company CommunicationsData Details] Delete Fail', props<{ payload: string }>());

export const loadCompanyDetailAddresses = createAction('[CompanyAddresses] Load', props<{ payload: string }>());
export const loadCompanyDetailAddressesSuccess = createAction('[CompanyAddresses] Load Success', props<ILoadCompanyAddressesSuccess>());
export const loadCompanyDetailAddressesFail = createAction('[CompanyAddresses] Load Fail', props<ILoadCompanyAddressesFail>());

export const loadCompanyDetailAddressDetails = createAction(
  '[CompanyAddresses Details] Load',
  props<{ payload_contactId: string; payload_addressId: string }>()
);
export const loadCompanyDetailAddressDetailsSuccess = createAction('[CompanyAddresses Details] Load Success', props<{ payload: Address }>());
export const loadCompanyDetailAddressDetailsFail = createAction('[CompanyAddresses Details] Load Fail', props<{ payload: string }>());

export const persistAddressDetail = createAction('[CompanyAddress Details] Persist', props<{ payload: Address }>());
export const persistAddressDetailSuccess = createAction('[CompanyAddress Details] Persist Success', props<{ payload: Address }>());
export const persistAddressDetailFail = createAction('[CompanyAddress Details] Persist Fail', props<{ payload: string }>());

export const deleteAddress = createAction('[CompanyAddress Details] Delete', props<{ payload: Address }>());
export const deleteAddressSuccess = createAction('[CompanyAddress Details] Delete Success');
export const deleteAddressFail = createAction('[CompanyAddress Details] Delete Fail', props<{ payload: string }>());

export const loadCompanyDetailContactPersons = createAction('[Company ContactPersons] Load', props<{ payload: string }>());
export const loadCompanyDetailContactPersonsSuccess = createAction('[Company ContactPersons] Load Success', props<ILoadCompanyContactPersonsSuccess>());
export const loadCompanyDetailContactPersonsFail = createAction('[Company ContactPersons] Load Fail', props<ILoadCompanyContactPersonsFail>());
