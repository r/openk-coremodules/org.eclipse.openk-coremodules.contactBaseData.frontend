 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { User } from '@shared/models/user';

interface ISetLanguageProps {
  payload: string;
}
interface ISetCultureProps {
  payload: string;
}
interface ISetUserProps {
  payload: User;
}

export const setLanguage = createAction(
  '[Settings] SetLanguage',
  props<ISetLanguageProps>()
);

export const setCulture = createAction(
  '[Settings] SetCulture',
  props<ISetCultureProps>()
);

export const setUser = createAction(
  '[Settings] SetUser',
  props<ISetUserProps>()
);
