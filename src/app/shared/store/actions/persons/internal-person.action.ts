/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { InternalPerson, CommunicationsData } from '@shared/models';
import { InternalPersonInterface } from '@shared/models/persons/internal-person-arguments.interface';
import { Address } from '@shared/models';

export interface ILoadInternalPersonsForUidSuccess {
  payload: Array<InternalPerson>;
}

export interface ILoadInternalPersonsForUidFail {
  payload: string;
}

export interface ILoadInternalPersonsForUserrefSuccess {
  payload: Array<InternalPerson>;
}

export interface ILoadInternalPersonsForUserrefFail {
  payload: string;
}

export interface ILoadInternalPersonDetailsSuccess {
  payload: InternalPerson;
}

export interface ILoadInternalPersonDetailsFail {
  payload: string;
}

export interface ILoadInternalPersonCommunicationsDataSuccess {
  payload: Array<CommunicationsData>;
}

export interface ILoadInternalPersonCommunicationsDataFail {
  payload: string;
}

export interface ILoadInternalPersonAddressesSuccess {
  payload: Array<Address>;
}
export interface ILoadInternalPersonAddressesFail {
  payload: string;
}

export const loadInternalPersonDetail = createAction('[InternalPerson Details] Load', props<{ payload: string }>());

export const loadInternalPersonDetailSuccess = createAction('[InternalPerson Details] Load Success', props<ILoadInternalPersonDetailsSuccess>());

export const loadInternalPersonDetailFail = createAction('[InternalPerson Details] Load Fail', props<ILoadInternalPersonDetailsFail>());

export const persistInternalPersonDetail = createAction('[InternalPerson Details] Persist', props<{ payload: InternalPerson }>());

export const persistInternalPersonDetailSuccess = createAction('[InternalPerson Details] Persist Success', props<{ payload: InternalPerson }>());

export const persistInternalPersonDetailFail = createAction('[InternalPerson Details] Persist Fail', props<{ payload: string }>());

export const loadInternalPersonsForUid = createAction('[InternalPersons For Uid] Load', props<{ payload: InternalPersonInterface }>());

export const loadInternalPersonsForUidSuccess = createAction('[InternalPersons For Uid] Load Success', props<ILoadInternalPersonsForUidSuccess>());

export const loadInternalPersonsForUidFail = createAction('[InternalPersons For Uid] Load Fail', props<ILoadInternalPersonsForUidFail>());

export const loadInternalPersonsForUserref = createAction('[InternalPersons For Userref] Load', props<{ payload: InternalPersonInterface }>());

export const loadInternalPersonsForUserrefSuccess = createAction('[InternalPersons For Userref] Load Success', props<ILoadInternalPersonsForUserrefSuccess>());

export const loadInternalPersonsForUserrefFail = createAction('[InternalPersons For Userref] Load Fail', props<ILoadInternalPersonsForUserrefFail>());

export const loadInternalPersonDetailCommunicationsData = createAction('[InternalPerson CommunicationsData] Load', props<{ payload: string }>());

export const loadInternalPersonDetailCommunicationsDataSuccess = createAction(
  '[InternalPerson CommunicationsData] Load Success',
  props<ILoadInternalPersonCommunicationsDataSuccess>()
);

export const loadInternalPersonDetailCommunicationsDataFail = createAction(
  '[InternalPerson CommunicationsData] Load Fail',
  props<ILoadInternalPersonCommunicationsDataFail>()
);

export const loadInternalPersonDetailCommunicationsDataDetails = createAction(
  '[InternalPerson CommunicationsData Details] Load',
  props<{ payload_contactId: string; payload_communicationsId: string }>()
);

export const loadInternalPersonDetailCommunicationsDataDetailsSuccess = createAction(
  '[InternalPerson CommunicationsData Details] Load Success',
  props<{ payload: CommunicationsData }>()
);

export const loadInternalPersonDetailCommunicationsDataDetailsFail = createAction(
  '[InternalPerson CommunicationsData Details] Load Fail',
  props<{ payload: string }>()
);

export const persistCommunicationsDataDetail = createAction('[InternalPerson CommunicationsData Details] Persist', props<{ payload: CommunicationsData }>());

export const persistCommunicationsDataDetailSuccess = createAction(
  '[InternalPerson CommunicationsData Details] Persist Success',
  props<{ payload: CommunicationsData }>()
);

export const persistCommunicationsDataDetailFail = createAction('[InternalPerson CommunicationsData Details] Persist Fail', props<{ payload: string }>());

export const deleteCommunicationsData = createAction('[InternalPerson CommunicationsData Details] Delete', props<{ payload: CommunicationsData }>());
export const deleteCommunicationsDataSuccess = createAction('[InternalPerson CommunicationsData Details] Delete Success');
export const deleteCommunicationsDataFail = createAction('[InternalPerson CommunicationsData Details] Delete Fail', props<{ payload: string }>());

export const loadInternalPersonDetailAddresses = createAction('[InternalPersonAddresses] Load', props<{ payload: string }>());

export const loadInternalPersonDetailAddressesSuccess = createAction('[InternalPersonAddresses] Load Success', props<ILoadInternalPersonAddressesSuccess>());

export const loadInternalPersonDetailAddressesFail = createAction('[InternalPersonAddresses] Load Fail', props<ILoadInternalPersonAddressesFail>());

export const loadInternalPersonDetailAddressDetails = createAction(
  '[InternalPersonAddresses Details] Load',
  props<{ payload_contactId: string; payload_addressId: string }>()
);

export const loadInternalPersonDetailAddressDetailsSuccess = createAction('[InternalPersonAddresses Details] Load Success', props<{ payload: Address }>());

export const loadInternalPersonDetailAddressDetailsFail = createAction('[InternalPersonAddresses Details] Load Fail', props<{ payload: string }>());

export const persistAddressDetail = createAction('[InternalPersonAddress Details] Persist', props<{ payload: Address }>());

export const persistAddressDetailSuccess = createAction('[InternalPersonAddress Details] Persist Success', props<{ payload: Address }>());

export const persistAddressDetailFail = createAction('[InternalPersonAddress Details] Persist Fail', props<{ payload: string }>());

export const deleteAddress = createAction('[InternalPersonAddress Details] Delete', props<{ payload: Address }>());
export const deleteAddressSuccess = createAction('[InternalPersonAddress Details] Delete Success');
export const deleteAddressFail = createAction('[InternalPersonAddress Details] Delete Fail', props<{ payload: string }>());
