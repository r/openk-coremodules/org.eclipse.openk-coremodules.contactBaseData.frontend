/********************************************************************************
* Copyright (c) 2020 Contributors to the Eclipse Foundation
*
* See the NOTICE file(s) distributed with this work for additional
* information regarding copyright ownership.
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License v. 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { ExternalPerson, CommunicationsData, Address } from '@shared/models';

export interface  ILoadExternalPersonAddressesSuccess {
  payload: Array<Address>;
}
export interface ILoadExternalPersonAddressesFail {
  payload: string;
}
export interface  ILoadExternalPersonCommunicationsDataSuccess {
  payload: Array<CommunicationsData>;
}
export interface ILoadExternalPersonCommunicationsDataFail {
  payload: string;
}

export const loadExternalPersonDetail = createAction(
  '[ExternalPerson Details] Load',
  props<{ payload: string }>()
);

export const loadExternalPersonDetailSuccess = createAction(
  '[ExternalPerson Details] Load Success',
  props<{ payload: ExternalPerson }>()
);

export const loadExternalPersonDetailFail = createAction(
  '[ExternalPerson Details] Load Fail',
  props<{ payload: string }>()
);

export const persistExternalPersonDetail = createAction(
  '[ExternalPerson Details] Persist',
  props<{ payload: ExternalPerson }>()
);

export const persistExternalPersonDetailSuccess = createAction(
  '[ExternalPerson Details] Persist Success',
  props<{ payload: ExternalPerson }>()
);

export const persistExternalPersonDetailFail = createAction(
  '[ExternalPerson Details] Persist Fail',
  props<{ payload: string }>()
);

export const loadExternalPersonDetailAddresses = createAction(
  '[ExternalPersonAddresses] Load',
  props<{ payload: string }>()
);

export const loadExternalPersonDetailAddressesSuccess = createAction(
  '[ExternalPersonAddresses] Load Success',
  props<ILoadExternalPersonAddressesSuccess>()
);

export const loadExternalPersonDetailAddressesFail = createAction(
  '[ExternalPersonAddresses] Load Fail',
  props<ILoadExternalPersonAddressesFail>()
);

export const loadExternalPersonDetailAddressDetails = createAction(
  '[ExternalPersonAddresses Details] Load',
  props<{ payload_contactId: string, payload_addressId: string }>()
);

export const loadExternalPersonDetailAddressDetailsSuccess = createAction(
  '[ExternalPersonAddresses Details] Load Success',
  props<{ payload: Address }>()
);

export const loadExternalPersonDetailAddressDetailsFail = createAction(
  '[ExternalPersonAddresses Details] Load Fail',
  props<{ payload: string }>()
);

export const persistAddressDetail = createAction(
  '[ExternalPersonAddress Details] Persist',
  props<{ payload: Address }>()
);

export const persistAddressDetailSuccess = createAction(
  '[ExternalPersonAddress Details] Persist Success',
  props<{ payload: Address }>()
);

export const persistAddressDetailFail = createAction(
  '[ExternalPersonAddress Details] Persist Fail',
  props<{ payload: string }>()
);

export const deleteAddress = createAction(
  '[ExternalPersonAddress Details] Delete',
  props<{ payload: Address }>()
);
export const deleteAddressSuccess = createAction(
  '[ExternalPersonAddress Details] Delete Success'
);
export const deleteAddressFail = createAction(
  '[ExternalPersonAddress Details] Delete Fail',
  props<{ payload: string }>()
);

export const loadExternalPersonDetailCommunicationsData = createAction(
  '[ExternalPerson CommunicationsData] Load',
  props<{ payload: string }>()
);

export const loadExternalPersonDetailCommunicationsDataSuccess = createAction(
  '[ExternalPerson CommunicationsData] Load Success',
  props<ILoadExternalPersonCommunicationsDataSuccess>()
);

export const loadExternalPersonDetailCommunicationsDataFail = createAction(
  '[ExternalPerson CommunicationsData] Load Fail',
  props<ILoadExternalPersonCommunicationsDataFail>()
);

export const loadExternalPersonDetailCommunicationsDataDetails = createAction(
  '[ExternalPerson CommunicationsData Details] Load',
  props<{ payload_contactId: string, payload_communicationsId: string }>()
);

export const loadExternalPersonDetailCommunicationsDataDetailsSuccess = createAction(
  '[ExternalPerson CommunicationsData Details] Load Success',
  props<{ payload: CommunicationsData }>()
);

export const loadExternalPersonDetailCommunicationsDataDetailsFail = createAction(
  '[ExternalPerson CommunicationsData Details] Load Fail',
  props<{ payload: string }>()
);

export const persistCommunicationsDataDetail = createAction(
  '[ExternalPerson CommunicationsData Details] Persist',
  props<{ payload: CommunicationsData }>()
);

export const persistCommunicationsDataDetailSuccess = createAction(
  '[ExternalPerson CommunicationsData Details] Persist Success',
  props<{ payload: CommunicationsData }>()
);

export const persistCommunicationsDataDetailFail = createAction(
  '[ExternalPerson CommunicationsData Details] Persist Fail',
  props<{ payload: string }>()
);

export const deleteCommunicationsData = createAction(
  '[ExternalPerson CommunicationsData Details] Delete',
  props<{ payload: CommunicationsData }>()
);
export const deleteCommunicationsDataSuccess = createAction(
  '[ExternalPerson CommunicationsData Details] Delete Success'
);
export const deleteCommunicationsDataFail = createAction(
  '[ExternalPerson CommunicationsData Details] Delete Fail',
  props<{ payload: string }>()
);
