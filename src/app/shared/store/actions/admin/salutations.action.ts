 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { Salutation } from '@shared/models';

export interface ILoadSalutationsSuccess {
  payload: Array<Salutation>;
}
export interface ILoadSalutationsFail {
  payload: string;
}

export const loadSalutations = createAction('[Salutations] Load');
export const loadSalutationsSuccess = createAction(
  '[Salutations] Load Success',
  props<ILoadSalutationsSuccess>());
export const loadSalutationsFail = createAction(
  '[Salutations] Load Fail',
  props<ILoadSalutationsFail>());

export const loadSalutation = createAction(
  '[Salutation Details] Load',
  props<{ payload: string }>()
);
export const loadSalutationSuccess = createAction(
  '[Salutation Details] Load Success',
  props<{ payload: Salutation }>()
);
export const loadSalutationFail = createAction(
  '[Salutation Details] Load Fail',
  props<{ payload: string }>()
);

export const saveSalutation = createAction(
  '[Salutation Details] Save',
  props<{ payload: Salutation }>()
);
export const saveSalutationSuccess = createAction(
  '[Salutation Details] Save Success',
  props<{ payload: Salutation }>()
);
export const saveSalutationFail = createAction(
  '[Salutation Details] Save Fail',
  props<{ payload: string }>()
);

export const deleteSalutation = createAction(
  '[Salutation Details] Delete',
  props<{ payload: string }>()
);
export const deleteSalutationSuccess = createAction(
  '[Salutation Details] Delete Success'
);
export const deleteSalutationFail = createAction(
  '[Salutation Details] Delete Fail',
  props<{ payload: string }>()
);
