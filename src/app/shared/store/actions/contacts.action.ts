/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { Contact } from '@shared/models';
import { PageRequestInterface } from '@shared/models/page/page-request.interface';
import { PageModel } from '@shared/models/page/page.model';

export interface ILoadContactsPage {
  payload: PageRequestInterface;
}
export interface ILoadContactsPageSuccess {
  payload: PageModel<Contact>;
}

export interface ILoadContactsPageFail {
  payload: string;
}
export const loadContactsPage = createAction('[Contacts] Load Page', props<ILoadContactsPage>());
export const loadContactsPageSuccess = createAction('[Contacts] Load Page Success', props<ILoadContactsPageSuccess>());
export const loadContactsPageFail = createAction('[Contacts] Load Page Fail', props<ILoadContactsPageFail>());

export const anonymizeContact = createAction('[Contact] Anonymize', props<{ payload: string }>());
export const anonymizeContactSuccess = createAction('[Contact] Anonymize Success');
export const anonymizeContactFail = createAction('[Contact] Anonymize Fail', props<{ payload: string }>());
