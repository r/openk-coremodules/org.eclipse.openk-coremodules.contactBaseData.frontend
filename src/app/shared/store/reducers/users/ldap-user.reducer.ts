/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as ldapUserActions from '@shared/store/actions/users/ldap-user.action';
import { LdapUser } from '@shared/models';
import { Action, createReducer, on } from '@ngrx/store';

export interface State {
  loading: boolean;
  loaded: boolean;
  failed: boolean;
  data: Array<LdapUser>;
}

export const INITIAL_STATE: State = {
  loading: false,
  loaded: false,
  failed: false,
  data: [],
};
export const ldapUserReducer = createReducer(
  INITIAL_STATE,
  on(ldapUserActions.loadLdapUsers, (state: State | undefined, action: Action) => {
    return {
      ...state,
      loading: true,
      loaded: false,
      failed: false,
      data: [],
    };
  }),
  on(ldapUserActions.loadLdapUsersSuccess, (state: State | undefined, action: Action) => {
    return {
      ...state,
      loading: false,
      loaded: true,
      failed: false,
      data: action['payload'],
    };
  }),
  on(ldapUserActions.loadLdapUsersFail, (state: State | undefined, action: Action) => {
    return {
      ...state,
      loaded: false,
      loading: false,
      failed: true,
      data: [],
    };
  })
);
export function reducer(state = INITIAL_STATE, action: Action): State {
  return ldapUserReducer(state, action);
}

export const getData = (state: State) => state.data;
export const getLoading = (state: State) => state.loading;
export const getLoaded = (state: State) => state.loaded;
export const getFailed = (state: State) => state.failed;
