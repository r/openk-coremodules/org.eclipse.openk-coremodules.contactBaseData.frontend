/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as companyActions from '@shared/store/actions/company/company.action';
import {
  createFormGroupState,
  createFormStateReducerWithUpdate,
  updateGroup,
  FormGroupState,
  FormState,
  SetValueAction,
  validate,
} from 'ngrx-forms';
import { Address } from '@app/shared/models/address/address.model';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'companyAddressDetailForm';

export const INITIAL_STATE: FormGroupState<Address> = createFormGroupState<
Address
>(FORM_ID, new Address());


export const validateForm: ActionReducer<FormState<Address>> = createFormStateReducerWithUpdate<Address>(
  updateGroup<Address>(
    {
        addressTypeId: validate(required),
    }
  )
);


export function reducer(
  state: FormGroupState<Address> = INITIAL_STATE,
  action: Action
): FormGroupState<Address> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }

  if (action.type === companyActions.loadCompanyDetailAddressDetailsSuccess.type) {
    const companyAddress: Address = <Address>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, companyAddress);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}
export const getFormState = (state: FormGroupState<Address>) => state;

