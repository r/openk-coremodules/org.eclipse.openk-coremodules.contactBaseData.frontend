/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as contactPersonActions from '@shared/store/actions/company/contact-person.actions';
import { createFormGroupState, createFormStateReducerWithUpdate, updateGroup, FormGroupState, FormState, SetValueAction, validate } from 'ngrx-forms';
import { CommunicationsData } from '@shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'contactPersonCommunicationsDataDetailForm';

export const INITIAL_STATE: FormGroupState<CommunicationsData> = createFormGroupState<CommunicationsData>(FORM_ID, new CommunicationsData());

export const validateForm: ActionReducer<FormState<CommunicationsData>> = createFormStateReducerWithUpdate<CommunicationsData>(
  updateGroup<CommunicationsData>({
    communicationTypeId: validate(required),
  })
);

export function reducer(state: FormGroupState<CommunicationsData> = INITIAL_STATE, action: Action): FormGroupState<CommunicationsData> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }

  if (action.type === contactPersonActions.loadContactPersonDetailCommunicationsDataDetailsSuccess.type) {
    const communicationsData: CommunicationsData = <CommunicationsData>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, communicationsData);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}
export const getFormState = (state: FormGroupState<CommunicationsData>) => state;
