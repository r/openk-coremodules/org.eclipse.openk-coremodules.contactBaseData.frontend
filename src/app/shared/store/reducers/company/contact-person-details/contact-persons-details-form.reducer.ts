/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as contactPersonActions from '@shared/store/actions/company/contact-person.actions';
import { createFormGroupState, createFormStateReducerWithUpdate, updateGroup, FormGroupState, FormState, SetValueAction, validate } from 'ngrx-forms';
import { ContactPerson } from '@shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'contactPersonDetailsForm';

export const INITIAL_STATE: FormGroupState<ContactPerson> = createFormGroupState<ContactPerson>(FORM_ID, new ContactPerson());

export const validateForm: ActionReducer<FormState<ContactPerson>> = createFormStateReducerWithUpdate<ContactPerson>(
  updateGroup<ContactPerson>({
    lastName: validate(required),
    personTypeId: validate(required),
  })
);

export function reducer(state: FormGroupState<ContactPerson> = INITIAL_STATE, action: Action): FormGroupState<ContactPerson> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }

  if (action.type === contactPersonActions.loadContactPersonDetailSuccess.type) {
    const person: ContactPerson = <ContactPerson>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, person);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}

export const getFormState = (state: FormGroupState<ContactPerson>) => state;
