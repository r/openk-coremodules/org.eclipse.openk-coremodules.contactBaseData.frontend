/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as fromReducer from '@shared/store/reducers/admin/address-types-details-form.reducer';
import { AddressType } from '@shared/models';
import { Action } from '@ngrx/store';
import { FormGroupState } from 'ngrx-forms';
import * as addressTypesActions from '@shared/store/actions/admin/address-types.action';

describe('AddressType Detail Form reducer', () => {
  const { INITIAL_STATE } = fromReducer;

  it('should return the default state when no state is given', () => {
    const action: Action = { type: '' };
    const state: FormGroupState<AddressType> = fromReducer.reducer(undefined, action);

    expect(state).toBe(INITIAL_STATE);
  });

  it('should return the initial state when action is not found', () => {
    const action: Action = { type: '' };
    const state: FormGroupState<AddressType> = fromReducer.reducer(INITIAL_STATE, action);

    expect(state).toBe(INITIAL_STATE);
  });

  it('should return the initial state when no action set', () => {
    const action: Action = null;
    const state: FormGroupState<AddressType> = fromReducer.reducer(INITIAL_STATE, action);

    expect(state).toBe(INITIAL_STATE);
  });

  it('should return state via getFormState', () => {
    const action: Action = null;
    const state: FormGroupState<AddressType> = fromReducer.reducer(INITIAL_STATE, action);
    expect(fromReducer.getFormState(state)).toBe(state);
  });

  it('should set id', () => {
    const action: Action = { type: addressTypesActions.loadAddressTypeSuccess.type };
    action['payload']={id: 'x'};
    const state: FormGroupState<AddressType> = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.controls.id).toBeDefined();
  });

});
