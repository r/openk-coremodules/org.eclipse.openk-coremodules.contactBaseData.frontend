/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as personTypesActions from '@shared/store/actions/admin/person-types.action';
import {
  createFormGroupState,
  createFormStateReducerWithUpdate,
  updateGroup,
  FormGroupState,
  FormState,
  SetValueAction,
  validate,
} from 'ngrx-forms';
import { PersonType } from '@shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'personTypeDetailForm';

export const INITIAL_STATE: FormGroupState<PersonType> = createFormGroupState<
  PersonType
>(FORM_ID, new PersonType());

export const validateForm: ActionReducer<FormState<PersonType>> = createFormStateReducerWithUpdate<PersonType>(
  updateGroup<PersonType>(
    {
      type: validate(required),
      description: validate(required),
    }
  )
);

export function reducer(
  state: FormGroupState<PersonType> = INITIAL_STATE,
  action: Action
): FormGroupState<PersonType> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }
  if (action.type === personTypesActions.loadPersonTypeSuccess.type) {
    const item: PersonType = <PersonType>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, item);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}
export const getFormState = (state: FormGroupState<PersonType>) => state;

