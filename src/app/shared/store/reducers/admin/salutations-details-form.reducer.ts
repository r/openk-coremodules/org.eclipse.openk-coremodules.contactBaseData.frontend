 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as salutationActions from '@shared/store/actions/admin/salutations.action';
import {
  createFormGroupState,
  createFormStateReducerWithUpdate,
  updateGroup,
  FormGroupState,
  FormState,
  SetValueAction,
  validate,
} from 'ngrx-forms';
import { Salutation } from '@shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'salutationDetailForm';

export const INITIAL_STATE: FormGroupState<Salutation> = createFormGroupState<
  Salutation
>(FORM_ID, new Salutation());

export const validateForm: ActionReducer<FormState<Salutation>> = createFormStateReducerWithUpdate<Salutation>(
  updateGroup<Salutation>(
    {
      type: validate(required),
      description: validate(required),
    }
  )
);

export function reducer(
  state: FormGroupState<Salutation> = INITIAL_STATE,
  action: Action
): FormGroupState<Salutation> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }
  if (action.type === salutationActions.loadSalutationSuccess.type) {
    const salutation: Salutation = <Salutation>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, salutation);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}
export const getFormState = (state: FormGroupState<Salutation>) => state;

