/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Action, ActionReducer, INIT } from '@ngrx/store';
import * as communicationTypesActions from '@shared/store/actions/admin/communication-types.action';
import {
  createFormGroupState,
  createFormStateReducerWithUpdate,
  updateGroup,
  FormGroupState,
  FormState,
  SetValueAction,
  validate,
} from 'ngrx-forms';
import { CommunicationType } from '@shared/models';
import { required } from 'ngrx-forms/validation';

export const FORM_ID = 'communicationTypesDetailForm';

export const INITIAL_STATE: FormGroupState<CommunicationType> = createFormGroupState<
  CommunicationType
>(FORM_ID, new CommunicationType({editable: true}));

export const validateForm: ActionReducer<FormState<CommunicationType>> = createFormStateReducerWithUpdate<CommunicationType>(
  updateGroup<CommunicationType>(
    {
      type: validate(required),
      description: validate(required),
    }
  )
);

export function reducer(
  state: FormGroupState<CommunicationType> = INITIAL_STATE,
  action: Action
): FormGroupState<CommunicationType> {
  if (!action || action.type === INIT) {
    return INITIAL_STATE;
  }
  if (action.type === communicationTypesActions.loadCommunicationTypeSuccess.type) {
    const CommunicationType: CommunicationType = <CommunicationType>action['payload'];
    const setValueAction: SetValueAction<any> = new SetValueAction<any>(FORM_ID, CommunicationType);

    return validateForm(state, setValueAction);
  }

  return validateForm(state, action);
}
export const getFormState = (state: FormGroupState<CommunicationType>) => state;

