/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import * as settingsActions from '@shared/store/actions/settings.action';
import { createReducer, on, Action } from '@ngrx/store';
import { User } from '@shared/models/user';

export interface State {
  selectedLanguage: string;
  selectedCulture: string;
  availableLanguages: Array<any>;
  user: User;
}

export const INITIAL_STATE: State = {
  selectedLanguage: '',
  selectedCulture: '',
  availableLanguages: [{ code: 'de', name: 'DE', culture: 'de-DE' }],
  user: new User(),
};

export const settingsReducer = createReducer(
  INITIAL_STATE,
  on(settingsActions.setLanguage, (state: any, action: any) => {
    return { ...state, ...{ selectedLanguage: action['payload'] } };
  }),
  on(settingsActions.setCulture, (state: any, action: any) => {
    return { ...state, selectedCulture: action['payload'] };
  }),
  on(settingsActions.setUser, (state: any, action: any) => {
    return { ...state, user: action['payload'] };
  })
);

export function reducer(state: State = INITIAL_STATE, action: Action): State {
  return settingsReducer(state, action);
}

export const getSelectedLanguage = (state: State) => state.selectedLanguage;
export const getSelectedCulture = (state: State) => state.selectedCulture;
export const getAvailableLanguages = (state: State) => state.availableLanguages;
export const getUser = (state: State) => state.user;
