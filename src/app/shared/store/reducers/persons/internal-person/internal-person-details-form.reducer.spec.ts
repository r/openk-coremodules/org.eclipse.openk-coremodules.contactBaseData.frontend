 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as fromReducer from '@shared/store/reducers/persons/internal-person/internal-person-details-form.reducer';
import { InternalPerson } from '@shared/models';
import { Action } from '@ngrx/store';
import { FormGroupState } from 'ngrx-forms';


describe('External Person Form reducer', () => {
  const { INITIAL_STATE } = fromReducer;

  it('should return the default state when no state is given', () => {
    const action: Action = { type: 'testAction' };
    const state: FormGroupState<InternalPerson> = fromReducer.reducer(undefined, action);

    expect(state).toBe(INITIAL_STATE);
  });

  it('should return the initial state when action is not found', () => {
    const action: Action = { type: 'testAction' };
    const state: FormGroupState<InternalPerson> = fromReducer.reducer(INITIAL_STATE, action);

    expect(state).toBe(INITIAL_STATE);
  });

  it('should return the initial state when no action set', () => {
    const action: Action = null;
    const state: FormGroupState<InternalPerson> = fromReducer.reducer(INITIAL_STATE, action);

    expect(state).toBe(INITIAL_STATE);
  });
});
