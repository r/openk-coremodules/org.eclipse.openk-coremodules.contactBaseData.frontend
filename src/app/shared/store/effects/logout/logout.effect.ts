/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import * as logoutActions from '@shared/store/actions/logout/logout.action';
import { catchError, map, switchMap } from 'rxjs/operators';
import { LogoutApiClient } from '@pages/logout/logout-api-client';

@Injectable()
export class LogoutEffects {
  logout$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(logoutActions.logout),
      map(action => action['payload']),
      switchMap(() => {
        return this._logoutApiClient.logout().pipe(
          map(response => logoutActions.logoutSuccess({ payload: response })),
          catchError(error => of(logoutActions.logoutFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _logoutApiClient: LogoutApiClient) {}
}
