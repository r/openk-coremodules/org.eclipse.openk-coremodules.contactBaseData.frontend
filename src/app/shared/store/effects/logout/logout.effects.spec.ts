/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { take } from 'rxjs/operators';
import { Subject, of, throwError } from 'rxjs';
import * as logoutActions from '@shared/store/actions/logout/logout.action';
import { LogoutEffects } from '@shared/store/effects/logout/logout.effect';
import { LogoutApiClient } from '@pages/logout/logout-api-client';

describe('Logout Effects', () => {
  let effects: LogoutEffects;
  let actions$: Subject<any>;
  let apiClient: LogoutApiClient;

  beforeEach(() => {
    apiClient = {
      logout() {},
    } as any;
    actions$ = new Subject();
    effects = new LogoutEffects(actions$, apiClient);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal logoutSuccess after logout', done => {
    let request = 'test';
    spyOn(apiClient, 'logout').and.returnValue(of(request));
    effects.logout$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(logoutActions.logoutSuccess({ payload: request }));
    });
    done();
    actions$.next(logoutActions.logout());
  });

  it('should equal logoutFail after logout Error', done => {
    spyOn(apiClient, 'logout').and.returnValue(throwError('x'));
    effects.logout$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(logoutActions.logoutFail({ payload: 'x' }));
    });
    done();
    actions$.next(logoutActions.logout());
  });
});
