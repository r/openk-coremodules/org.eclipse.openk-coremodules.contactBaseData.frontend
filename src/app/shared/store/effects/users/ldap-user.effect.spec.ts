/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PersonsApiClient } from '@pages/persons/persons-api-client';
import { Subject, of, throwError } from 'rxjs';
import * as ldapUserActions from '@shared/store/actions/users/ldap-user.action';
import { LdapUser } from '@shared/models';
import { LdapUserEffect } from '@shared/store/effects/users/ldap-user.effect';

describe('LdapUserEffect Effects', () => {
  let effects: LdapUserEffect;
  let actions$: Subject<any>;
  let apiClient: PersonsApiClient;
  let apiResponse: any = null;

  beforeEach(() => {
    apiClient = {
      getLdapUsers() {},
    } as any;

    actions$ = new Subject();

    effects = new LdapUserEffect(actions$, apiClient);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadLdapUsersSuccess after loadLdapUsers', done => {
    apiResponse = [new LdapUser()];
    spyOn(apiClient, 'getLdapUsers').and.returnValue(of(apiResponse));
    effects.getLdapUsers$.pipe().subscribe(result => {
      expect(result).toEqual(ldapUserActions.loadLdapUsersSuccess({ payload: [new LdapUser()] }));
    });
    done();
    actions$.next(ldapUserActions.loadLdapUsers());
  });

  it('should equal loadLdapUsersFail after loadLdapUsers', done => {
    spyOn(apiClient, 'getLdapUsers').and.returnValue(throwError('error'));
    effects.getLdapUsers$.pipe().subscribe(result => {
      expect(result).toEqual(ldapUserActions.loadLdapUsersFail({ payload: 'error' }));
    });
    done();
    actions$.next(ldapUserActions.loadLdapUsers());
  });
});
