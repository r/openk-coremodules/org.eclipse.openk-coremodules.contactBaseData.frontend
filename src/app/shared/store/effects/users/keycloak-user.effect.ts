/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import * as keycloakUserActions from '@shared/store/actions/users/keycloak-user.action';
import { PersonsApiClient } from '@pages/persons/persons-api-client';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class KeycloakUserEffect {
  getKeycloakUsers$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(keycloakUserActions.loadKeycloakUsers),
      switchMap(() => {
        return this._personsApiClient.getKeycloakUsers().pipe(
          map(users => keycloakUserActions.loadKeycloakUsersSuccess({ payload: users })),
          catchError(error => of(keycloakUserActions.loadKeycloakUsersFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _personsApiClient: PersonsApiClient) {}
}
