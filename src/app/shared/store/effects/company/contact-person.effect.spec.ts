/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CompanyApiClient } from '@pages/company/company-api-client';
import { ContactPersonEffect } from '@shared/store/effects/company/contact-person.effect';
import { Subject, of, throwError } from 'rxjs';
import * as contactPersonActions from '@shared/store/actions/company/contact-person.actions';
import * as companyActions from '@shared/store/actions/company/company.action';
import { Company, CommunicationsData, Address, ContactPerson } from '@shared/models';

describe('ContactPerson Effects', () => {
  let effects: ContactPersonEffect;
  let actions$: Subject<any>;
  let apiClient: CompanyApiClient;
  let apiResponse: any = null;
  let store: any;

  beforeEach(() => {
    apiClient = {
      getContactPersonsDetails: () => {},
      putContactPersonDetails: () => {},
      postContactPersonDetails: () => {},
      getCompanyDetailsAddresses: () => {},
      getCompanyDetailsAddressesDetails: () => {},
      putAddressDetails: () => {},
      postAddressDetails: () => {},
      deleteAddress: () => {},
      getCommunicationsData: () => {},
      putCommunicationsDataDetails: () => {},
      postCommunicationsDataDetails: () => {},
      deleteCommunicationsData: () => {},
      deleteContactPersonDetails: () => {},
    } as any;

    actions$ = new Subject();

    store = {
      dispatch() {},
    };

    effects = new ContactPersonEffect(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadContactPersonDetail after getContactPersonsDetails', done => {
    apiResponse = new ContactPerson({ id: '1' });
    spyOn(apiClient, 'getContactPersonsDetails').and.returnValue(of(apiResponse));
    effects.getContactPersonsDetails$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.loadContactPersonDetailSuccess({ payload: new ContactPerson({ id: '1' }) }));
    });
    done();
    actions$.next(contactPersonActions.loadContactPersonDetail({ payload: '1' }));
  });

  it('should equal loadContactPersonDetailFail after getContactPersonsDetails error', done => {
    spyOn(apiClient, 'getContactPersonsDetails').and.returnValue(throwError('error'));
    effects.getContactPersonsDetails$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.loadContactPersonDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(contactPersonActions.loadContactPersonDetail({ payload: '1' }));
  });

  it('should equal putContactPersonDetails after saveContactPerson with given contactId', done => {
    apiResponse = new ContactPerson({ contactId: '1' });
    spyOn(apiClient, 'putContactPersonDetails').and.returnValue(of(apiResponse));
    effects.saveContactPerson$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.saveContactPersonDetailSuccess({ payload: new ContactPerson({ contactId: '1' }) }));
    });
    done();
    actions$.next(contactPersonActions.saveContactPersonDetail({ payload: new ContactPerson({ contactId: '1' }) }));
  });

  it('should equal postContactPersonDetails after saveContactPerson', done => {
    apiResponse = new ContactPerson();
    spyOn(apiClient, 'postContactPersonDetails').and.returnValue(of(apiResponse));
    effects.saveContactPerson$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.saveContactPersonDetailSuccess({ payload: new ContactPerson() }));
    });
    done();
    actions$.next(contactPersonActions.saveContactPersonDetail({ payload: new ContactPerson() }));
  });

  it('should equal saveContactPersonDetailFail after putCompanyDetails error', done => {
    spyOn(apiClient, 'putContactPersonDetails').and.returnValue(throwError('error'));
    effects.saveContactPerson$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.saveContactPersonDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(contactPersonActions.saveContactPersonDetail({ payload: new ContactPerson({ contactId: '1' }) }));
  });

  it('should equal .deleteContactPersonSuccess after deleteContactPerson', done => {
    apiResponse = new ContactPerson({ contactId: '1' });
    spyOn(apiClient, 'deleteContactPersonDetails').and.returnValue(of(apiResponse));
    effects.deleteContactPerson$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.deleteContactPersonSuccess());
    });
    done();
    actions$.next(contactPersonActions.deleteContactPerson({ payload: new ContactPerson({ contactId: '1' }) }));
  });

  it('should equal persistCommunicationsDataDetailSuccess after persistCommunicationsDataDetail with given contactId', done => {
    apiResponse = new CommunicationsData({ contactId: '1', id: '1' });
    spyOn(apiClient, 'putCommunicationsDataDetails').and.returnValue(of(apiResponse));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.persistCommunicationsDataDetailSuccess({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
    });
    done();
    actions$.next(contactPersonActions.persistCommunicationsDataDetail({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistCommunicationsDataDetailSuccess after persistCommunicationsDataDetail a new internal person', done => {
    apiResponse = new CommunicationsData();
    spyOn(apiClient, 'postCommunicationsDataDetails').and.returnValue(of(apiResponse));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.persistCommunicationsDataDetailSuccess({ payload: new CommunicationsData() }));
    });
    done();
    actions$.next(contactPersonActions.persistCommunicationsDataDetail({ payload: new CommunicationsData() }));
  });

  it('should equal persistCommunicationsDataDetailFail after putCommunicationsDataDetails', done => {
    spyOn(apiClient, 'putCommunicationsDataDetails').and.returnValue(throwError('error'));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.persistCommunicationsDataDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(contactPersonActions.persistCommunicationsDataDetail({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistCommunicationsDataDetailFail after postCommunicationsDataDetails', done => {
    spyOn(apiClient, 'postCommunicationsDataDetails').and.returnValue(throwError('error'));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.persistCommunicationsDataDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(contactPersonActions.persistCommunicationsDataDetail({ payload: new CommunicationsData() }));
  });

  it('should equal deleteCommunicationsDataSuccess after deleteCommunicationsData', done => {
    apiResponse = new CommunicationsData({ contactId: '1', id: '1' });
    spyOn(apiClient, 'deleteCommunicationsData').and.returnValue(of(apiResponse));
    effects.deleteCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.deleteCommunicationsDataSuccess());
    });
    done();
    actions$.next(contactPersonActions.deleteCommunicationsData({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
  });

  it('should equal deleteCommunicationsDataFail after deleteCommunicationsData', done => {
    spyOn(apiClient, 'deleteCommunicationsData').and.returnValue(throwError('error'));
    effects.deleteCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.deleteCommunicationsDataFail({ payload: 'error' }));
    });
    done();
    actions$.next(contactPersonActions.deleteCommunicationsData({ payload: new CommunicationsData({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistAddressDetailSuccess after persistAddressDetail with given contactId', done => {
    apiResponse = new Address({ contactId: '1', id: '1' });
    spyOn(apiClient, 'putAddressDetails').and.returnValue(of(apiResponse));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.persistAddressDetailSuccess({ payload: new Address({ contactId: '1', id: '1' }) }));
    });
    done();
    actions$.next(contactPersonActions.persistAddressDetail({ payload: new Address({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistAddressDetailSuccess after persistAddressDetail a new company', done => {
    apiResponse = new Address();
    spyOn(apiClient, 'postAddressDetails').and.returnValue(of(apiResponse));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.persistAddressDetailSuccess({ payload: new Address() }));
    });
    done();
    actions$.next(contactPersonActions.persistAddressDetail({ payload: new Address() }));
  });

  it('should equal persistAddressDetailFail after putAddressDetails', done => {
    spyOn(apiClient, 'putAddressDetails').and.returnValue(throwError('error'));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.persistAddressDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(contactPersonActions.persistAddressDetail({ payload: new Address({ contactId: '1', id: '1' }) }));
  });

  it('should equal persistAddressDetailFail after postAddressDetails', done => {
    spyOn(apiClient, 'postAddressDetails').and.returnValue(throwError('error'));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.persistAddressDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(contactPersonActions.persistAddressDetail({ payload: new Address() }));
  });

  it('should equal deleteAddressSuccess after deleteAddress', done => {
    apiResponse = new Address({ contactId: '1', id: '1' });
    spyOn(apiClient, 'deleteAddress').and.returnValue(of(apiResponse));
    effects.deleteAddress$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.deleteAddressSuccess());
    });
    done();
    actions$.next(contactPersonActions.deleteAddress({ payload: new Address({ contactId: '1', id: '1' }) }));
  });

  it('should equal deleteAddressFail after deleteAddress', done => {
    spyOn(apiClient, 'deleteAddress').and.returnValue(throwError('error'));
    effects.deleteAddress$.pipe().subscribe(result => {
      expect(result).toEqual(contactPersonActions.deleteAddressFail({ payload: 'error' }));
    });
    done();
    actions$.next(contactPersonActions.deleteAddress({ payload: new Address({ contactId: '1', id: '1' }) }));
  });
});
