/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';
import { catchError, map, switchMap } from 'rxjs/operators';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import * as companyActions from '@shared/store/actions/company/company.action';
import { CompanyApiClient } from '@pages/company/company-api-client';
import { Company, CommunicationsData, ContactPerson, Address } from '@shared/models';
import { Store } from '@ngrx/store';
import { State } from '@shared/store';

@Injectable()
export class CompanyEffect {
  /**
   * load external company details
   */
  getCompany$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.loadCompanyDetail),
      switchMap(action => {
        return this._companyApiClient
          .getCompanyDetails(action['payload'])
          .map((company: Company) => companyActions.loadCompanyDetailSuccess({ payload: company }))
          .catch(error => of(companyActions.loadCompanyDetailFail({ payload: error })));
      })
    )
  );

  /**
   * Load company's addresses details
   */
  getCompanyAddressDetails$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.loadCompanyDetailAddressDetails),
      switchMap(action => {
        return this._companyApiClient
          .getAddressesDetails(action.payload_contactId, action.payload_addressId)
          .map((companyAddressDetails: Address) => companyActions.loadCompanyDetailAddressDetailsSuccess({ payload: companyAddressDetails }))
          .catch(error => of(companyActions.loadCompanyDetailAddressDetailsFail({ payload: error })));
      })
    )
  );

  /**
   * load company details Addresses
   */
  getCompanyAddresses$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.loadCompanyDetailAddresses),
      switchMap((action: any) => {
        return this._companyApiClient.getAddresses(action['payload']).pipe(
          map((companyAddresses: Array<Address>) => companyActions.loadCompanyDetailAddressesSuccess({ payload: companyAddresses })),
          catchError(error => of(companyActions.loadCompanyDetailAddressesFail({ payload: error })))
        );
      })
    )
  );

  /**
   * persist new or edited company
   */
  persistCompany$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.persistCompanyDetail),
      map(action => action['payload']),
      switchMap((companyDetails: Company) => {
        return (companyDetails.contactId
          ? this._companyApiClient.putCompanyDetails(companyDetails.contactId, companyDetails)
          : this._companyApiClient.postCompanyDetails(companyDetails)
        ).pipe(
          map((company: Company) => {
            return companyActions.persistCompanyDetailSuccess({ payload: company });
          }),
          catchError(error => of(companyActions.persistCompanyDetailFail({ payload: error })))
        );
      })
    )
  );

  /**
   * persist new or edited address
   */
  persistAddress$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.persistAddressDetail),
      map(action => action['payload']),
      switchMap((address: Address) => {
        return (address.id
          ? this._companyApiClient.putAddressDetails(address.contactId, address.id, address)
          : this._companyApiClient.postAddressDetails(address.contactId, address)
        ).pipe(
          map(() => {
            this.store.dispatch(companyActions.loadCompanyDetailAddresses({ payload: address.contactId }));
            return companyActions.persistAddressDetailSuccess({ payload: address });
          }),
          catchError(error => of(companyActions.persistAddressDetailFail({ payload: error })))
        );
      })
    )
  );

  /**
   * delete address
   */
  deleteAddress$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.deleteAddress),
      map(action => action['payload']),
      switchMap((address: Address) => {
        return this._companyApiClient.deleteAddress(address.contactId, address.id).pipe(
          map(() => {
            this.store.dispatch(companyActions.loadCompanyDetailAddresses({ payload: address.contactId }));
            return companyActions.deleteAddressSuccess();
          }),
          catchError(error => of(companyActions.deleteAddressFail({ payload: error })))
        );
      })
    )
  );

  /**
   * load internal company details communications data
   */
  getCompanyCommunicationsData$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.loadCompanyDetailCommunicationsData),
      switchMap((action: any) => {
        return this._companyApiClient.getCommunicationsData(action['payload']).pipe(
          map((CompanyCommunicationsData: Array<CommunicationsData>) =>
            companyActions.loadCompanyDetailCommunicationsDataSuccess({ payload: CompanyCommunicationsData })
          ),
          catchError(error => of(companyActions.loadCompanyDetailCommunicationsDataFail({ payload: error })))
        );
      })
    )
  );

  /**
   * Load internal company's communications data details
   */
  getCompanyCommunicationsDataDetails$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.loadCompanyDetailCommunicationsDataDetails),
      switchMap(action => {
        return this._companyApiClient
          .getCommunicationsDataDetails(action.payload_contactId, action.payload_communicationsId)
          .map((extcompanyCommunicationsDataDetails: CommunicationsData) =>
            companyActions.loadCompanyDetailCommunicationsDataDetailsSuccess({ payload: extcompanyCommunicationsDataDetails })
          )
          .catch(error => of(companyActions.loadCompanyDetailCommunicationsDataDetailsFail({ payload: error })));
      })
    )
  );

  /**
   * persist new or edited communications data
   */
  persistCommunicationsData$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.persistCommunicationsDataDetail),
      map(action => action['payload']),
      switchMap((address: CommunicationsData) => {
        return (address.id
          ? this._companyApiClient.putCommunicationsDataDetails(address.contactId, address.id, address)
          : this._companyApiClient.postCommunicationsDataDetails(address.contactId, address)
        ).pipe(
          map(() => {
            this.store.dispatch(companyActions.loadCompanyDetailCommunicationsData({ payload: address.contactId }));
            return companyActions.persistCommunicationsDataDetailSuccess({ payload: address });
          }),
          catchError(error => of(companyActions.persistCommunicationsDataDetailFail({ payload: error })))
        );
      })
    )
  );

  /**
   * delete communications data
   */
  deleteCommunicationsData$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.deleteCommunicationsData),
      map(action => action['payload']),
      switchMap((address: CommunicationsData) => {
        return this._companyApiClient.deleteCommunicationsData(address.contactId, address.id).pipe(
          map(() => {
            this.store.dispatch(companyActions.loadCompanyDetailCommunicationsData({ payload: address.contactId }));
            return companyActions.deleteCommunicationsDataSuccess();
          }),
          catchError(error => of(companyActions.deleteCommunicationsDataFail({ payload: error })))
        );
      })
    )
  );

  /**
   * load company contact persons
   */
  getCompanyContactPersons$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(companyActions.loadCompanyDetailContactPersons),
      switchMap((action: any) => {
        return this._companyApiClient.getContactPersons(action['payload']).pipe(
          map((companyContactPersons: Array<ContactPerson>) => companyActions.loadCompanyDetailContactPersonsSuccess({ payload: companyContactPersons })),
          catchError(error => of(companyActions.loadCompanyDetailContactPersonsFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _companyApiClient: CompanyApiClient, private store: Store<State>) {}
}
