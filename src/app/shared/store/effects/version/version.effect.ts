/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import * as versionActions from '@shared/store/actions/version/version.action';
import { catchError, map, switchMap } from 'rxjs/operators';
import { VersionApiClient } from '@app/shared/components/version-info/version-api-client';

@Injectable()
export class VersionEffects {
  version$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(versionActions.version),
      map(action => action['payload']),
      switchMap(() => {
        return this._versionApiClient.version().pipe(
          map(response => versionActions.versionSuccess({ payload: response })),
          catchError(error => of(versionActions.versionFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _versionApiClient: VersionApiClient) {}
}
