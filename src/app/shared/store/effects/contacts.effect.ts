/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import * as contactsActions from '@shared/store/actions/contacts.action';
import { ContactsApiClient } from '@pages/contacts/contacts-api-client';
import { catchError, map, switchMap } from 'rxjs/operators';
import { PageRequestInterface } from '@shared/models/page/page-request.interface';

@Injectable()
export class ContactsEffects {
  /* paged list */
  getContactsPage$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(contactsActions.loadContactsPage),
      map(action => action['payload']),
      switchMap((request: PageRequestInterface) => {
        return this._contactsApiClient.getContacts(request).pipe(
          map(contacts => contactsActions.loadContactsPageSuccess({ payload: contacts })),
          catchError(error => of(contactsActions.loadContactsPageFail({ payload: error })))
        );
      })
    )
  );

  anonymizeContact$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(contactsActions.anonymizeContact),
      map(action => action['payload']),
      switchMap((id: string) => {
        return this._contactsApiClient.anonymizeContact(id).pipe(
          map(() => contactsActions.anonymizeContactSuccess()),
          catchError(error => of(contactsActions.anonymizeContactFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _contactsApiClient: ContactsApiClient) {}
}
