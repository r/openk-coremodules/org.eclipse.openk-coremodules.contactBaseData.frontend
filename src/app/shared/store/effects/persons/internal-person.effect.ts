/********************************************************************************
* Copyright (c) 2020 Contributors to the Eclipse Foundation
*
* See the NOTICE file(s) distributed with this work for additional
* information regarding copyright ownership.
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License v. 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';
import { catchError, map, switchMap } from 'rxjs/operators';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import * as internalPersonActions from '@shared/store/actions/persons/internal-person.action';
import { PersonsApiClient } from '@pages/persons/persons-api-client';
import { InternalPerson, CommunicationsData, Address } from '@shared/models';
import { InternalPersonInterface } from '@shared/models/persons/internal-person-arguments.interface';
import { Store } from '@ngrx/store';
import { State } from '@shared/store';

@Injectable()
export class InternalPersonEffect {
  /**
   * load internal person details
   */
  getInternalPerson$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.loadInternalPersonDetail),
      switchMap(action => {
        return this._personsApiClient
          .getInternalPersonDetails(action['payload'])
          .map((internalPerson: InternalPerson) => internalPersonActions.loadInternalPersonDetailSuccess({ payload: internalPerson }))
          .catch(error => of(internalPersonActions.loadInternalPersonDetailFail({ payload: error })));
      })
    )
  );

  /**
  * Load internal person's addresses details
  */
  getInternalPersonAddressDetails$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.loadInternalPersonDetailAddressDetails),
      switchMap(action => {
        return this._personsApiClient
          .getAddressesDetails(action.payload_contactId, action.payload_addressId)
          .map((internalPersonAdressDetails: Address) =>
            internalPersonActions.loadInternalPersonDetailAddressDetailsSuccess({ payload: internalPersonAdressDetails })
          )
          .catch(error =>
            of(internalPersonActions.loadInternalPersonDetailAddressDetailsFail({ payload: error }))
          );
      })
    )
  );

  /**
   * persist new or edited internal person
   */
  persistInternalPerson$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.persistInternalPersonDetail),
      map(action => action['payload']),
      switchMap((internalPersonDetails: InternalPerson) => {
        return (internalPersonDetails.contactId
          ? this._personsApiClient.putInternalPersonDetails(internalPersonDetails.contactId, internalPersonDetails)
          : this._personsApiClient.postInternalPersonDetails(internalPersonDetails)
        ).pipe(
          map((internalPerson: InternalPerson) => {
            return internalPersonActions.persistInternalPersonDetailSuccess({ payload: internalPerson });
          }),
          catchError(error => of(internalPersonActions.persistInternalPersonDetailFail({ payload: error })))
        );
      })
    )
  );

  /**
   * load internal persons for uid
   */
  getInternalPersonsForUid$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.loadInternalPersonsForUid),
      map(action => action['payload']),
      switchMap((request: InternalPersonInterface) => {
        return this._personsApiClient.getInternalPersons(request).pipe(
          map(persons => internalPersonActions.loadInternalPersonsForUidSuccess({ payload: persons })),
          catchError(error => of(internalPersonActions.loadInternalPersonsForUidFail({ payload: error })))
        );
      })
    )
  );

  /**
   * load internal persons for userref
   */
  getInternalPersonsForUserref$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.loadInternalPersonsForUserref),
      map(action => action['payload']),
      switchMap((request: InternalPersonInterface) => {
        return this._personsApiClient.getInternalPersons(request).pipe(
          map(persons => internalPersonActions.loadInternalPersonsForUserrefSuccess({ payload: persons })),
          catchError(error => of(internalPersonActions.loadInternalPersonsForUserrefFail({ payload: error })))
        );
      })
    )
  );

  /**
  * load internal person details Addresses
  */
  getInternalPersonAddresses$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.loadInternalPersonDetailAddresses),
      switchMap((action: any) => {
        return this._personsApiClient
          .getAddresses(action['payload']).pipe(
            map((internalPersonAddresses: Array<Address>) =>
              internalPersonActions.loadInternalPersonDetailAddressesSuccess({ payload: internalPersonAddresses })
            ),
            catchError(error =>
              of(internalPersonActions.loadInternalPersonDetailAddressesFail({ payload: error }))
            )
          );
      })
    )
  );

  /**
   * persist new or edited address
   */
  persistAddress$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.persistAddressDetail),
      map(action => action['payload']),
      switchMap((address: Address) => {
        return (address.id
          ? this._personsApiClient.putAddressDetails(address.contactId, address.id, address)
          : this._personsApiClient.postAddressDetails(address.contactId, address)
        ).pipe(
          map(() => {
            this.store.dispatch(internalPersonActions.loadInternalPersonDetailAddresses({ payload: address.contactId }));
            return internalPersonActions.persistAddressDetailSuccess({ payload: address });
          }),
          catchError(error => of(internalPersonActions.persistAddressDetailFail({ payload: error })))
        );
      })
    )
  );

  /**
   * delete address
   */
  deleteAddress$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.deleteAddress),
      map((action) => action['payload']),
      switchMap((address: Address) => {
        return this._personsApiClient.deleteAddress(address.contactId, address.id).pipe(
          map(() => {
            this.store.dispatch(internalPersonActions.loadInternalPersonDetailAddresses({ payload: address.contactId }));
            return internalPersonActions.deleteAddressSuccess();
          }),
          catchError(error => of(internalPersonActions.deleteAddressFail({ payload: error })))
        );
      })
    )
  );

  /**
  * load internal person details communications data
  */
  getInternalPersonCommunicationsData$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.loadInternalPersonDetailCommunicationsData),
      switchMap((action: any) => {
        return this._personsApiClient
          .getCommunicationsData(action['payload']).pipe(
            map((internalPersonCommunicationsData: Array<CommunicationsData>) =>
              internalPersonActions.loadInternalPersonDetailCommunicationsDataSuccess({ payload: internalPersonCommunicationsData })
            ),
            catchError(error =>
              of(internalPersonActions.loadInternalPersonDetailCommunicationsDataFail({ payload: error }))
            )
          );
      })
    )
  );

  /**
  * Load internal person's communications data details
  */
  getInternalPersonCommunicationsDataDetails$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.loadInternalPersonDetailCommunicationsDataDetails),
      switchMap(action => {
        return this._personsApiClient
          .getCommunicationsDataDetails(action.payload_contactId, action.payload_communicationsId)
          .map((extPersonCommunicationsDataDetails: CommunicationsData) =>
            internalPersonActions.loadInternalPersonDetailCommunicationsDataDetailsSuccess({ payload: extPersonCommunicationsDataDetails })
          )
          .catch(error =>
            of(internalPersonActions.loadInternalPersonDetailCommunicationsDataDetailsFail({ payload: error }))
          );
      })
    )
  );

  /**
   * persist new or edited communications data
   */
  persistCommunicationsData$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.persistCommunicationsDataDetail),
      map(action => action['payload']),
      switchMap((address: CommunicationsData) => {
        return (address.id
          ? this._personsApiClient.putCommunicationsDataDetails(address.contactId, address.id, address)
          : this._personsApiClient.postCommunicationsDataDetails(address.contactId, address)
        ).pipe(
          map(() => {
            this.store.dispatch(internalPersonActions.loadInternalPersonDetailCommunicationsData({ payload: address.contactId }));
            return internalPersonActions.persistCommunicationsDataDetailSuccess({ payload: address });
          }),
          catchError(error => of(internalPersonActions.persistCommunicationsDataDetailFail({ payload: error })))
        );
      })
    )
  );

  /**
   * delete communications data
   */
  deleteCommunicationsData$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(internalPersonActions.deleteCommunicationsData),
      map((action) => action['payload']),
      switchMap((address: CommunicationsData) => {
        return this._personsApiClient.deleteCommunicationsData(address.contactId, address.id).pipe(
          map(() => {
            this.store.dispatch(internalPersonActions.loadInternalPersonDetailCommunicationsData({ payload: address.contactId }));
            return internalPersonActions.deleteCommunicationsDataSuccess();
          }),
          catchError(error => of(internalPersonActions.deleteCommunicationsDataFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _personsApiClient: PersonsApiClient, private store: Store<State>) { }
}
