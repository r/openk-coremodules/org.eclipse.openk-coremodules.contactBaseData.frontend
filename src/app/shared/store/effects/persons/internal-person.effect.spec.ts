 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PersonsApiClient } from '@pages/persons/persons-api-client';
import { InternalPersonEffect } from "@shared/store/effects/persons/internal-person.effect";
import { Subject, of, throwError } from 'rxjs';
import * as internalPersonActions from '@shared/store/actions/persons/internal-person.action';
import { InternalPerson, CommunicationsData, Address } from '@shared/models';

describe('InternalPerson Effects', () => {
  let effects: InternalPersonEffect;
  let actions$: Subject<any>;
  let apiClient: PersonsApiClient;
  let apiResponse: any = null;
  let store: any;

  beforeEach(() => {
    apiClient = {
      getInternalPersonDetails() {},
      putInternalPersonDetails: () => {},
      postInternalPersonDetails: () => {},
      getInternalPersons: () => {},
      getAddressesDetails: () => {},
      putAddressDetails: () => {},
      postAddressDetails: () => {},
      deleteAddress: () => {},
      putCommunicationsDataDetails: () => {},
      postCommunicationsDataDetails: () => {},
      deleteCommunicationsData: () => {},
    } as any;

    actions$ = new Subject();

    store = {
      dispatch() {}
    }

    effects = new InternalPersonEffect(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadInternalPersonDetail after getInternalPerson', (done) => {
    apiResponse =  new InternalPerson({id: "1"});
    spyOn(apiClient, 'getInternalPersonDetails').and.returnValue(of(apiResponse));
    effects.getInternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.loadInternalPersonDetailSuccess({payload: new InternalPerson({id: "1"})}));
    });
    done();
    actions$.next(internalPersonActions.loadInternalPersonDetail({payload: "1"}));
  });

  it('should equal loadInternalPersonDetailFail after getInternalPerson', (done) => {
    spyOn(apiClient, 'getInternalPersonDetails').and.returnValue(throwError('error'));
    effects.getInternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.loadInternalPersonDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.loadInternalPersonDetail({payload: "1"}));
  });

  it('should equal putInternalPersonDetails after persistInternalPerson with given contactId', (done) => {
    apiResponse =  new InternalPerson({contactId: "1"});
    spyOn(apiClient, 'putInternalPersonDetails').and.returnValue(of(apiResponse));
    effects.persistInternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistInternalPersonDetailSuccess({payload: new InternalPerson({contactId: "1"})}));
    });
    done();
    actions$.next(internalPersonActions.persistInternalPersonDetail({payload: new InternalPerson({contactId: "1"})}));
  });

  it('should equal postInternalPersonDetails after persistInternalPerson a new internal person', (done) => {
    apiResponse =  new InternalPerson();
    spyOn(apiClient, 'postInternalPersonDetails').and.returnValue(of(apiResponse));
    effects.persistInternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistInternalPersonDetailSuccess({payload: new InternalPerson()}));
    });
    done();
    actions$.next(internalPersonActions.persistInternalPersonDetail({payload: new InternalPerson()}));
  });

  it('should equal persistInternalPersonDetailFail after putInternalPersonDetails', (done) => {
    spyOn(apiClient, 'putInternalPersonDetails').and.returnValue(throwError('error'));
    effects.persistInternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistInternalPersonDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.persistInternalPersonDetail({payload: new InternalPerson({contactId: "1"})}));
  });

  it('should equal persistInternalPersonDetailFail after postInternalPersonDetails', (done) => {
    spyOn(apiClient, 'postInternalPersonDetails').and.returnValue(throwError('error'));
    effects.persistInternalPerson$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistInternalPersonDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.persistInternalPersonDetail({payload: new InternalPerson()}));
  });

  it('should equal loadInternalPersonsForUidSuccess after loadInternalPersonsForUid', (done) => {
    apiResponse =  [new InternalPerson()];
    spyOn(apiClient, 'getInternalPersons').and.returnValue(of(apiResponse));
    effects.getInternalPersonsForUid$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.loadInternalPersonsForUidSuccess({payload: [new InternalPerson()]}));
    });
    done();
    actions$.next(internalPersonActions.loadInternalPersonsForUid({payload: new InternalPerson()}));
  });

  it('should equal loadInternalPersonsForUidFail after loadInternalPersonsForUid', (done) => {
    spyOn(apiClient, 'getInternalPersons').and.returnValue(throwError('error'));
    effects.getInternalPersonsForUid$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.loadInternalPersonsForUidFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.loadInternalPersonsForUid({payload: new InternalPerson()}));
  });

  it('should equal loadInternalPersonsForUserrefSuccess after loadInternalPersonsForUserref', (done) => {
    apiResponse =  [new InternalPerson()];
    spyOn(apiClient, 'getInternalPersons').and.returnValue(of(apiResponse));
    effects.getInternalPersonsForUserref$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.loadInternalPersonsForUserrefSuccess({payload: [new InternalPerson()]}));
    });
    done();
    actions$.next(internalPersonActions.loadInternalPersonsForUserref({payload: new InternalPerson()}));
  });

  it('should equal loadInternalPersonsForUserrefFail after loadInternalPersonsForUserref', (done) => {
    spyOn(apiClient, 'getInternalPersons').and.returnValue(throwError('error'));
    effects.getInternalPersonsForUserref$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.loadInternalPersonsForUserrefFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.loadInternalPersonsForUserref({payload: new InternalPerson()}));
  });

  it('should equal persistAddressDetailSuccess after persistAddressDetail with given contactId', (done) => {
    apiResponse =  new Address({contactId: "1", id: "1"});
    spyOn(apiClient, 'putAddressDetails').and.returnValue(of(apiResponse));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistAddressDetailSuccess({payload: new Address({contactId: "1", id: "1"})}));
    });
    done();
    actions$.next(internalPersonActions.persistAddressDetail({payload: new Address({contactId: "1", id: "1"})}));
  });

  it('should equal persistAddressDetailSuccess after persistAddressDetail a new external person', (done) => {
    apiResponse =  new Address();
    spyOn(apiClient, 'postAddressDetails').and.returnValue(of(apiResponse));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistAddressDetailSuccess({payload: new Address()}));
    });
    done();
    actions$.next(internalPersonActions.persistAddressDetail({payload: new Address()}));
  });

  it('should equal persistAddressDetailFail after putAddressDetails', (done) => {
    spyOn(apiClient, 'putAddressDetails').and.returnValue(throwError('error'));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistAddressDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.persistAddressDetail({payload: new Address({contactId: "1", id: "1"})}));
  });

  it('should equal persistAddressDetailFail after postAddressDetails', (done) => {
    spyOn(apiClient, 'postAddressDetails').and.returnValue(throwError('error'));
    effects.persistAddress$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistAddressDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.persistAddressDetail({payload: new Address ()}));
  });

  it('should equal deleteAddressSuccess after deleteAddress', (done) => {
    apiResponse =  new Address({contactId: "1", id: "1"});
    spyOn(apiClient, 'deleteAddress').and.returnValue(of(apiResponse));
    effects.deleteAddress$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.deleteAddressSuccess());
    });
    done();
    actions$.next(internalPersonActions.deleteAddress({payload: new Address ({contactId: "1", id: "1"})}));
  });

  it('should equal deleteAddressFail after deleteAddress', (done) => {
    spyOn(apiClient, 'deleteAddress').and.returnValue(throwError('error'));
    effects.deleteAddress$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.deleteAddressFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.deleteAddress({payload: new Address ({contactId: "1", id: "1"})}));
  });

  it('should equal persistCommunicationsDataDetailSuccess after persistCommunicationsDataDetail with given contactId', (done) => {
    apiResponse =  new CommunicationsData({contactId: "1", id: "1"});
    spyOn(apiClient, 'putCommunicationsDataDetails').and.returnValue(of(apiResponse));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistCommunicationsDataDetailSuccess({payload: new CommunicationsData({contactId: "1", id: "1"})}));
    });
    done();
    actions$.next(internalPersonActions.persistCommunicationsDataDetail({payload: new CommunicationsData({contactId: "1", id: "1"})}));
  });

  it('should equal persistCommunicationsDataDetailSuccess after persistCommunicationsDataDetail a new internal person', (done) => {
    apiResponse =  new CommunicationsData();
    spyOn(apiClient, 'postCommunicationsDataDetails').and.returnValue(of(apiResponse));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistCommunicationsDataDetailSuccess({payload: new CommunicationsData()}));
    });
    done();
    actions$.next(internalPersonActions.persistCommunicationsDataDetail({payload: new CommunicationsData()}));
  });

  it('should equal persistCommunicationsDataDetailFail after putCommunicationsDataDetails', (done) => {
    spyOn(apiClient, 'putCommunicationsDataDetails').and.returnValue(throwError('error'));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistCommunicationsDataDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.persistCommunicationsDataDetail({payload: new CommunicationsData({contactId: "1", id: "1"})}));
  });

  it('should equal persistCommunicationsDataDetailFail after postCommunicationsDataDetails', (done) => {
    spyOn(apiClient, 'postCommunicationsDataDetails').and.returnValue(throwError('error'));
    effects.persistCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.persistCommunicationsDataDetailFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.persistCommunicationsDataDetail({payload: new CommunicationsData ()}));
  });

  it('should equal deleteCommunicationsDataSuccess after deleteCommunicationsData', (done) => {
    apiResponse =  new CommunicationsData({contactId: "1", id: "1"});
    spyOn(apiClient, 'deleteCommunicationsData').and.returnValue(of(apiResponse));
    effects.deleteCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.deleteCommunicationsDataSuccess());
    });
    done();
    actions$.next(internalPersonActions.deleteCommunicationsData({payload: new CommunicationsData ({contactId: "1", id: "1"})}));
  });

  it('should equal deleteCommunicationsDataFail after deleteCommunicationsData', (done) => {
    spyOn(apiClient, 'deleteCommunicationsData').and.returnValue(throwError('error'));
    effects.deleteCommunicationsData$.pipe().subscribe(result => {
      expect(result).toEqual(internalPersonActions.deleteCommunicationsDataFail({payload: 'error'}));
    });
    done();
    actions$.next(internalPersonActions.deleteCommunicationsData({payload: new CommunicationsData ({contactId: "1", id: "1"})}));
  });
});
