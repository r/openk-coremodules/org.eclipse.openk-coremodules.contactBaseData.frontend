/********************************************************************************
* Copyright (c) 2020 Contributors to the Eclipse Foundation
*
* See the NOTICE file(s) distributed with this work for additional
* information regarding copyright ownership.
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License v. 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';
import { catchError, map, switchMap } from 'rxjs/operators';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import * as externalPersonActions from '@shared/store/actions/persons/external-person.action';
import { PersonsApiClient } from '@pages/persons/persons-api-client';
import { ExternalPerson, Address, CommunicationsData } from '@shared/models';
import { Store } from '@ngrx/store';
import { State } from '@shared/store';

@Injectable()
export class ExternalPersonEffect {

  /**
   * load external person details
   */
  getExternalPerson$: any = createEffect(() =>
    this.actions$.pipe(
      ofType(externalPersonActions.loadExternalPersonDetail),
      switchMap(action => {
        return this._personsApiClient
          .getExternalPersonDetails(action['payload'])
          .map((externalPerson: ExternalPerson) => externalPersonActions.loadExternalPersonDetailSuccess({ payload: externalPerson }))
          .catch(error => of(externalPersonActions.loadExternalPersonDetailFail({ payload: error })));
      })
    )
  );

  /**
  * load external person details Addresses
  */
  getExternalPersonAddresses$: any = createEffect(() =>
    this.actions$.pipe(
      ofType(externalPersonActions.loadExternalPersonDetailAddresses),
      switchMap((action: any) => {
        return this._personsApiClient
          .getAddresses(action['payload']).pipe(
            map((externalPersonAddresses: Array<Address>) =>
              externalPersonActions.loadExternalPersonDetailAddressesSuccess({ payload: externalPersonAddresses })
            ),
            catchError(error =>
              of(externalPersonActions.loadExternalPersonDetailAddressesFail({ payload: error }))
            )
          );
      })
    )
  );

   /**
   * Load external person's addresses details
   */
  getExternalPersonAddressDetails$: any = createEffect(() =>
    this.actions$.pipe(
      ofType(externalPersonActions.loadExternalPersonDetailAddressDetails),
      switchMap(action => {
        return this._personsApiClient
          .getAddressesDetails(action.payload_contactId, action.payload_addressId)
          .map((extPersonAdressDetails: Address) =>
          externalPersonActions.loadExternalPersonDetailAddressDetailsSuccess({ payload: extPersonAdressDetails })
          )
          .catch(error =>
            of(externalPersonActions.loadExternalPersonDetailAddressDetailsFail({ payload: error }))
          );
      })
    )
  );

  /**
   * persist new or edited external person
   */
  persistExternalPerson$: any = createEffect(() =>
    this.actions$.pipe(
      ofType(externalPersonActions.persistExternalPersonDetail),
      map(action => action['payload']),
      switchMap((externalPersonDetails: ExternalPerson) => {
        return (externalPersonDetails.contactId
          ? this._personsApiClient.putExternalPersonDetails(externalPersonDetails.contactId, externalPersonDetails)
          : this._personsApiClient.postExternalPersonDetails(externalPersonDetails)
        ).pipe(
          map((externalPerson: ExternalPerson) => {
            return externalPersonActions.persistExternalPersonDetailSuccess({ payload: externalPerson });
          }),
          catchError(error => of(externalPersonActions.persistExternalPersonDetailFail({ payload: error })))
        );
      })
    )
  );

   /**
   * persist new or edited address
   */
  persistAddress$: any = createEffect(() =>
  this.actions$.pipe(
    ofType(externalPersonActions.persistAddressDetail),
    map(action => action['payload']),
    switchMap((address: Address) => {
      return (address.id
        ? this._personsApiClient.putAddressDetails(address.contactId, address.id, address)
        : this._personsApiClient.postAddressDetails(address.contactId, address)
      ).pipe(
        map(() => {
          this.store.dispatch(externalPersonActions.loadExternalPersonDetailAddresses({payload: address.contactId}));
          return externalPersonActions.persistAddressDetailSuccess({ payload: address });
        }),
        catchError(error => of(externalPersonActions.persistAddressDetailFail({ payload: error })))
      );
    })
  )
);

  /**
   * delete address
   */
  deleteAddress$: any = createEffect(() =>
      this.actions$.pipe(
      ofType(externalPersonActions.deleteAddress),
      map((action) => action['payload']),
      switchMap((address: Address) => {
        return this._personsApiClient.deleteAddress(address.contactId, address.id).pipe(
          map(() => {
            this.store.dispatch(externalPersonActions.loadExternalPersonDetailAddresses({payload: address.contactId}));
            return externalPersonActions.deleteAddressSuccess();
       }),
        catchError(error => of(externalPersonActions.deleteAddressFail({ payload: error })))
      );
    })
   )
  );

  /**
  * load external person details communications data
  */
 getExternalPersonCommunicationsData$: any = createEffect(() =>
 this.actions$.pipe(
   ofType(externalPersonActions.loadExternalPersonDetailCommunicationsData),
   switchMap((action: any) => {
     return this._personsApiClient
       .getCommunicationsData(action['payload']).pipe(
         map((externalPersonCommunicationsData: Array<CommunicationsData>) =>
           externalPersonActions.loadExternalPersonDetailCommunicationsDataSuccess({ payload: externalPersonCommunicationsData })
         ),
         catchError(error =>
           of(externalPersonActions.loadExternalPersonDetailCommunicationsDataFail({ payload: error }))
         )
       );
   })
 )
);

/**
* Load external person's communications data details
*/
getExternalPersonCommunicationsDataDetails$: any = createEffect(() =>
 this.actions$.pipe(
   ofType(externalPersonActions.loadExternalPersonDetailCommunicationsDataDetails),
   switchMap(action => {
     return this._personsApiClient
       .getCommunicationsDataDetails(action.payload_contactId, action.payload_communicationsId)
       .map((extPersonCommunicationsDataDetails: CommunicationsData) =>
       externalPersonActions.loadExternalPersonDetailCommunicationsDataDetailsSuccess({ payload: extPersonCommunicationsDataDetails })
       )
       .catch(error =>
         of(externalPersonActions.loadExternalPersonDetailCommunicationsDataDetailsFail({ payload: error }))
       );
   })
 )
);

/**
 * persist new or edited communications data
 */
persistCommunicationsData$: any = createEffect(() =>
  this.actions$.pipe(
    ofType(externalPersonActions.persistCommunicationsDataDetail),
    map(action => action['payload']),
    switchMap((address: CommunicationsData) => {
      return (address.id
        ? this._personsApiClient.putCommunicationsDataDetails(address.contactId, address.id, address)
        : this._personsApiClient.postCommunicationsDataDetails(address.contactId, address)
      ).pipe(
        map(() => {
          this.store.dispatch(externalPersonActions.loadExternalPersonDetailCommunicationsData({payload: address.contactId}));
          return externalPersonActions.persistCommunicationsDataDetailSuccess({ payload: address });
        }),
        catchError(error => of(externalPersonActions.persistCommunicationsDataDetailFail({ payload: error })))
      );
    })
  )
);

/**
 * delete communications data
 */
deleteCommunicationsData$: any = createEffect(() =>
  this.actions$.pipe(
    ofType(externalPersonActions.deleteCommunicationsData),
    map((action) => action['payload']),
    switchMap((address: CommunicationsData) => {
      return this._personsApiClient.deleteCommunicationsData(address.contactId, address.id).pipe(
        map(() => {
          this.store.dispatch(externalPersonActions.loadExternalPersonDetailCommunicationsData({payload: address.contactId}));
          return externalPersonActions.deleteCommunicationsDataSuccess();
      }),
      catchError(error => of(externalPersonActions.deleteCommunicationsDataFail({ payload: error })))
      );
    })
  )
);


  constructor(private actions$: Actions, private _personsApiClient: PersonsApiClient, private store: Store<State>) { }
}
