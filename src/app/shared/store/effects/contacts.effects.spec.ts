/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PageRequestInterface } from '@shared/models/page/page-request.interface';
import { take } from 'rxjs/operators';
import { ContactsEffects } from '@shared/store/effects/contacts.effect';
import { Subject, of, throwError, Observable } from 'rxjs';
import { Contact } from '@shared/models';
import * as contactsActions from '@shared/store/actions/contacts.action';
import { ContactsApiClient } from '@pages/contacts/contacts-api-client';
import { PageModel } from '@shared/models/page/page.model';

describe('Contacts Effects', () => {
  let effects: ContactsEffects;
  let actions$: Subject<any>;
  let apiClient: ContactsApiClient;
  let apiResponse = new PageModel<Contact>({
    content: [new Contact({ id: '1' })],
  }) as any;

  beforeEach(() => {
    apiClient = {
      getContacts() {},
      anonymizeContact() {},
    } as any;
    actions$ = new Subject();
    effects = new ContactsEffects(actions$, apiClient);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadContactsPageSuccess after getContactsPage', done => {
    let request = {
      payload: {
        pageNumber: 1,
        pageSize: 3,
      } as PageRequestInterface,
    };
    spyOn(apiClient, 'getContacts').and.returnValue(of(apiResponse));

    effects.getContactsPage$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(contactsActions.loadContactsPageSuccess({ payload: apiResponse }));
    });
    done();

    actions$.next(contactsActions.loadContactsPage(request));
  });

  it('should equal loadContactsPageFail after getContactsPage Error', done => {
    let request = {
      payload: {
        pageNumber: 1,
        pageSize: 3,
      } as PageRequestInterface,
    };
    spyOn(apiClient, 'getContacts').and.returnValue(throwError('x'));

    effects.getContactsPage$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(contactsActions.loadContactsPageFail({ payload: 'x' }));
    });
    done();

    actions$.next(contactsActions.loadContactsPage(request));
  });

  it('should successfully process on anonymizeContact action', done => {
    apiResponse = new Observable<void>();
    spyOn(apiClient, 'anonymizeContact').and.returnValue(of(apiResponse));
    effects.anonymizeContact$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(contactsActions.anonymizeContactSuccess());
    });
    done();
    actions$.next(contactsActions.anonymizeContact({ payload: 'x' }));
  });
});
