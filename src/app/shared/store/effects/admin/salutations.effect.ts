/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import * as salutationsActions from '@shared/store/actions/admin/salutations.action';
import { SalutationsApiClient } from '@pages/admin/salutations/salutations-api-client';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Salutation } from '@shared/models';
import { Store } from '@ngrx/store';

@Injectable()
export class SalutationsEffects {

  getSalutations$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(salutationsActions.loadSalutations),
      switchMap(() => {
        return this._salutationsApiClient.getSalutations().pipe(
          map(salutations => salutationsActions.loadSalutationsSuccess({ payload: salutations })),
          catchError(error => of(salutationsActions.loadSalutationsFail({ payload: error })))
        );
      })
    )
  );

  getSalutation$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(salutationsActions.loadSalutation),
      switchMap(action => {
        return this._salutationsApiClient
          .getSalutationDetails(action['payload'])
          .map((salutation: Salutation) =>
            salutationsActions.loadSalutationSuccess({ payload: salutation })
          )
          .catch(error =>
            of(salutationsActions.loadSalutationFail({ payload: error }))
          );
      })
    )
  );

  saveSalutation$: any = createEffect(() =>
    this._actions$.pipe(
    ofType(salutationsActions.saveSalutation),
    map(action => action['payload']),
    switchMap((payloadSalutation: Salutation) => {
      return (payloadSalutation.id ?
        this._salutationsApiClient.putSalutation(payloadSalutation.id, payloadSalutation) :
        this._salutationsApiClient.postSalutation(payloadSalutation)
        ).pipe(
        map((salutation: Salutation) => {
          this._store.dispatch(salutationsActions.loadSalutations());
          return salutationsActions.saveSalutationSuccess({ payload: salutation });
        }),
        catchError(error =>
          of(salutationsActions.saveSalutationFail({ payload: error }))
        )
      );
    })
    )
  );

  deleteSalutation$: any = createEffect(() =>
      this._actions$.pipe(
      ofType(salutationsActions.deleteSalutation),
      map((action) => action['payload']),
      switchMap((id: string) => {
        return this._salutationsApiClient.deleteSalutation(id).pipe(
          map(() => {
            this._store.dispatch(salutationsActions.loadSalutations());
            return salutationsActions.deleteSalutationSuccess();
       }),
        catchError(error => of(salutationsActions.deleteSalutationFail({ payload: error })))
      );
    })
   )
  );

  constructor(
    private _actions$: Actions,
    private _salutationsApiClient: SalutationsApiClient,
    private _store: Store<any>
  ) {}
}
