/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { take } from 'rxjs/operators';
import { PersonTypesEffects } from '@shared/store/effects/admin/person-types.effect';
import { Subject, of, throwError } from 'rxjs';
import { PersonType } from '@shared/models';
import * as personTypesActions from '@shared/store/actions/admin/person-types.action';
import { PersonTypesApiClient } from '@pages/admin/person-types/person-types-api-client';
import { Store } from '@ngrx/store';

describe('PersonTypes Effects', () => {

  let effects: PersonTypesEffects;
  let actions$: Subject<any>;
  let apiClient: PersonTypesApiClient;
  let store: Store<any>;
  let apiResponse: any = null;

  beforeEach(() => {
    apiClient = {
      getPersonTypes() {},
      getPersonTypeDetails() {},
      putPersonType() {},
      postPersonType() {},
      deletePersonType() {}
    } as any;
    store = {
      dispatch() {}
    } as any;
    actions$ = new Subject();
    effects = new PersonTypesEffects(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadPersonTypesSuccess after getPersonTypes', (done) => {
    apiResponse =  [new PersonType({id: "1"})];
    spyOn(apiClient, 'getPersonTypes').and.returnValue(of(apiResponse));
    effects.getPersonTypes$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(personTypesActions.loadPersonTypesSuccess({payload: apiResponse}));
    });
    done();
    actions$.next(personTypesActions.loadPersonTypes());
  });

  it('should equal loadPersonTypesFail after getPersonTypes Error', (done) => {
    spyOn(apiClient, 'getPersonTypes').and.returnValue(throwError('x'));
    effects.getPersonTypes$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(personTypesActions.loadPersonTypesFail({payload: 'x'}));
    });
    done();
    actions$.next(personTypesActions.loadPersonTypes());
  });

  it('should equal loadPersonTypeSuccess after getPersonType', (done) => {
    apiResponse =  new PersonType({id: "1"});
    spyOn(apiClient, 'getPersonTypeDetails').and.returnValue(of(apiResponse));
    effects.getPersonType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(personTypesActions.loadPersonTypeSuccess({payload: apiResponse}));
    });
    done();
    actions$.next(personTypesActions.loadPersonType({payload: "1"}));
  });

  it('should equal loadPersonTypeFail after getPersonType Error', (done) => {
    spyOn(apiClient, 'getPersonTypeDetails').and.returnValue(throwError('x'));
    effects.getPersonType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(personTypesActions.loadPersonTypeFail({payload: 'x'}));
    });
    done();
    actions$.next(personTypesActions.loadPersonType({payload: "1"}));
  });

  it('should equal persistPersonTypeSuccess after persistPersonType', (done) => {
    apiResponse =  new PersonType({id: "1"});
    spyOn(apiClient, 'putPersonType').and.returnValue(of(apiResponse));
    effects.savePersonType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(personTypesActions.savePersonTypeSuccess({payload: new PersonType({id: "1"})}));
    });
    done();
    actions$.next(personTypesActions.savePersonType({payload: new PersonType({id: "1"})}));
  });

  it('should equal saveCommunicationTypeFail after saveCommunicationType Error', (done) => {
    spyOn(apiClient, 'putPersonType').and.returnValue(throwError('x'));
    effects.savePersonType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(personTypesActions.savePersonTypeFail({payload: 'x'}));
    });
    done();
    actions$.next(personTypesActions.savePersonType({payload: new PersonType({id: "1"})}));
  });

  it('should equal deletePersonTypeSuccess after deletePersonType', (done) => {
    apiResponse =  new PersonType({id: "1"});
    spyOn(apiClient, 'deletePersonType').and.returnValue(of(null));
    effects.deletePersonType$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(personTypesActions.deletePersonTypeSuccess());
    });
    done();
    actions$.next(personTypesActions.deletePersonType({payload: "1"}));
  });

});
