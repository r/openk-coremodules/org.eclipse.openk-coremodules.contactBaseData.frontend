/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import * as communicationTypesActions from '@shared/store/actions/admin/communication-types.action';
import { CommunicationTypesApiClient } from '@pages/admin/communication-types/communication-types-api-client';
import { catchError, map, switchMap } from 'rxjs/operators';
import { CommunicationType } from '@shared/models';
import { Store } from '@ngrx/store';

@Injectable()
export class CommunicationTypesEffects {

  getCommunicationTypes$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(communicationTypesActions.loadCommunicationTypes),
      switchMap(() => {
        return this._communicationTypesApiClient.getCommunicationTypes().pipe(
          map(commTypes => communicationTypesActions.loadCommunicationTypesSuccess({ payload: commTypes })),
          catchError(error => of(communicationTypesActions.loadCommunicationTypesFail({ payload: error })))
        );
      })
    )
  );

  getCommunicationType$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(communicationTypesActions.loadCommunicationType),
      switchMap(action => {
        return this._communicationTypesApiClient
          .getCommunicationType(action['payload'])
          .map((item: CommunicationType ) =>
            communicationTypesActions.loadCommunicationTypeSuccess({ payload: item })
          )
          .catch(error =>
            of(communicationTypesActions.loadCommunicationTypeFail({ payload: error }))
          );
      })
    )
  );

  saveCommunicationType$: any = createEffect(() =>
    this._actions$.pipe(
    ofType(communicationTypesActions.saveCommunicationType),
    map(action => action['payload']),
    switchMap((payload: CommunicationType) => {
      return (payload.id ?
        this._communicationTypesApiClient.putCommunicationType(payload.id, payload) :
        this._communicationTypesApiClient.postCommunicationType(payload)
        ).pipe(
        map((item: CommunicationType) => {
          this._store.dispatch(communicationTypesActions.loadCommunicationTypes());
          return communicationTypesActions.saveCommunicationTypeSuccess({ payload: item });
        }),
        catchError(error =>
          of(communicationTypesActions.saveCommunicationTypeFail({ payload: error }))
        )
      );
    })
    )
  );

  deleteCommunicationType$: any = createEffect(() =>
      this._actions$.pipe(
      ofType(communicationTypesActions.deleteCommunicationType),
      map((action) => action['payload']),
      switchMap((id: string) => {
        return this._communicationTypesApiClient.deleteCommunicationType(id).pipe(
          map(() => {
            this._store.dispatch(communicationTypesActions.loadCommunicationTypes());
            return communicationTypesActions.deleteCommunicationTypeSuccess();
       }),
        catchError(error => of(communicationTypesActions.deleteCommunicationTypeFail({ payload: error })))
      );
    })
   )
  );

  constructor(
    private _actions$: Actions,
    private _communicationTypesApiClient: CommunicationTypesApiClient,
    private _store: Store<any>
  ) {}
}
