/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Subject, of, throwError } from 'rxjs';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { UserModuleAssignmentEffect } from '@shared/store/effects/user-module-assignment/user-module-assignment.effect';
import { UserModuleAssignmentApiClient } from '@shared/components/list-details-view/user-module-assignment/user-module-assignment-api-client';
import { UserModuleAssignment } from '@shared/models';
import * as userModuleAssignmentActions from '@shared/store/actions/user-module-assignment/user-module-assignment.action';

describe('UserModuleAssignment Effects', () => {
  let effects: UserModuleAssignmentEffect;
  let actions$: Subject<any>;
  let apiClient: UserModuleAssignmentApiClient;
  let store: Store<any>;
  let apiResponse: any = null;

  beforeEach(() => {
    apiClient = {
      getUserModuleAssignments() {},
      getUserModuleAssignmentsDetails() {},
      postUserModuleAssignmentDetails() {},
      putUserModuleAssignmentDetails() {},
      deleteUserModuleAssignment() {},
    } as any;
    store = {
      dispatch() {},
    } as any;
    actions$ = new Subject();
    effects = new UserModuleAssignmentEffect(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadUserModuleAssignmentsSuccess after getUserModuleAssignments', done => {
    apiResponse = [new UserModuleAssignment({ id: '1' })];
    spyOn(apiClient, 'getUserModuleAssignments').and.returnValue(of(apiResponse));
    effects.getUserModuleAssignments$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(userModuleAssignmentActions.loadUserModuleAssignmentsSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(userModuleAssignmentActions.loadUserModuleAssignments({ payload: '1' }));
  });

  it('should equal loadUserModuleAssignmentDetailsSuccess after getUserModuleAssignmentsDetails', done => {
    apiResponse = new UserModuleAssignment({ id: '1' });
    spyOn(apiClient, 'getUserModuleAssignmentsDetails').and.returnValue(of(apiResponse));
    effects.getUserModuleAssignmentDetails$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(userModuleAssignmentActions.loadUserModuleAssignmentDetailsSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(userModuleAssignmentActions.loadUserModuleAssignmentDetails({ payload: new UserModuleAssignment() }));
  });

  it('should equal persistUserModuleAssignmentDetailSuccess after putUserModuleAssignmentDetails', done => {
    apiResponse = new UserModuleAssignment({ id: '1', contactId: '2' });
    spyOn(apiClient, 'putUserModuleAssignmentDetails').and.returnValue(of(apiResponse));
    effects.persistUserModuleAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(userModuleAssignmentActions.persistUserModuleAssignmentDetailSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(userModuleAssignmentActions.persistUserModuleAssignmentDetail({ payload: new UserModuleAssignment({ id: '1', contactId: '2' }) }));
  });

  it('should equal deleteUserModuleAssignmentSuccess after postUserModuleAssignmentDetails', done => {
    spyOn(apiClient, 'deleteUserModuleAssignment').and.returnValue(of(null));
    effects.deleteUserModuleAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(userModuleAssignmentActions.deleteUserModuleAssignmentSuccess());
    });
    done();
    actions$.next(userModuleAssignmentActions.deleteUserModuleAssignment({ payload: new UserModuleAssignment({ id: '1', contactId: '2' }) }));
  });
});
