/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';
import { catchError, map, switchMap, filter } from 'rxjs/operators';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import * as userModuleAssignmentActions from '@shared/store/actions/user-module-assignment/user-module-assignment.action';
import { UserModuleAssignment } from '@shared/models';
import { Store } from '@ngrx/store';
import { State } from '@shared/store';
import { UserModuleAssignmentApiClient } from '@app/shared/components/list-details-view/user-module-assignment/user-module-assignment-api-client';

@Injectable()
export class UserModuleAssignmentEffect {
  /**
   * load user module assignments
   */
  getUserModuleAssignments$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(userModuleAssignmentActions.loadUserModuleAssignments),
      switchMap((action: any) => {
        return this._userModuleAssignmentApiClient.getUserModuleAssignments(action['payload']).pipe(
          map((userModuleAssignments: Array<UserModuleAssignment>) =>
            userModuleAssignmentActions.loadUserModuleAssignmentsSuccess({ payload: userModuleAssignments })
          ),
          catchError(error => of(userModuleAssignmentActions.loadUserModuleAssignmentsFail({ payload: error })))
        );
      })
    )
  );

  /**
   * Load user module assignments details
   */
  getUserModuleAssignmentDetails$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(userModuleAssignmentActions.loadUserModuleAssignmentDetails),
      switchMap(action => {
        return this._userModuleAssignmentApiClient
          .getUserModuleAssignmentsDetails(action.payload.contactId, action.payload.id)
          .map((userModuleAssignmentDetails: UserModuleAssignment) =>
            userModuleAssignmentActions.loadUserModuleAssignmentDetailsSuccess({ payload: userModuleAssignmentDetails })
          )
          .catch(error => of(userModuleAssignmentActions.loadUserModuleAssignmentDetailsFail({ payload: error })));
      })
    )
  );

  /**
   * persist new or edited user module assignment
   */
  persistUserModuleAssignment$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(userModuleAssignmentActions.persistUserModuleAssignmentDetail),
      map(action => action['payload']),
      switchMap((userModuleAssignment: UserModuleAssignment) => {
        return (userModuleAssignment.id
          ? this._userModuleAssignmentApiClient.putUserModuleAssignmentDetails(userModuleAssignment.contactId, userModuleAssignment.id, userModuleAssignment)
          : this._userModuleAssignmentApiClient.postUserModuleAssignmentDetails(userModuleAssignment.contactId, userModuleAssignment)
        ).pipe(
          map(() => {
            this.store.dispatch(userModuleAssignmentActions.loadUserModuleAssignments({ payload: userModuleAssignment.contactId }));
            return userModuleAssignmentActions.persistUserModuleAssignmentDetailSuccess({ payload: userModuleAssignment });
          }),
          catchError(error => of(userModuleAssignmentActions.persistUserModuleAssignmentDetailFail({ payload: error })))
        );
      })
    )
  );

  /**
   * delete user module Assignment
   */
  deleteUserModuleAssignment$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(userModuleAssignmentActions.deleteUserModuleAssignment),
      map(action => action['payload']),
      switchMap((userModuleAssignment: UserModuleAssignment) => {
        return this._userModuleAssignmentApiClient.deleteUserModuleAssignment(userModuleAssignment.contactId, userModuleAssignment.id).pipe(
          map(() => {
            this.store.dispatch(userModuleAssignmentActions.loadUserModuleAssignments({ payload: userModuleAssignment.contactId }));
            return userModuleAssignmentActions.deleteUserModuleAssignmentSuccess();
          }),
          catchError(error => of(userModuleAssignmentActions.deleteUserModuleAssignmentFail({ payload: error })))
        );
      })
    )
  );

  /**
   * load user module types without given string
   */
  getFilteredUserModuleTypes$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(userModuleAssignmentActions.loadFilteredUserModuleTypes),
      switchMap(action => {
        return this._userModuleAssignmentApiClient.getUserModuleTypes().pipe(
          map(userModuleTypes => userModuleTypes.filter(userModuleType => userModuleType.id !== action.payload)),
          map(item => userModuleAssignmentActions.loadFilteredUserModuleTypesSuccess({ payload: item })),
          catchError(error => of(userModuleAssignmentActions.loadFilteredUserModuleTypesFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _userModuleAssignmentApiClient: UserModuleAssignmentApiClient, private store: Store<State>) {}
}
