/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { localeDateString } from '@shared/utility';
import * as settingsActions from '@shared/store/actions/settings.action';
import { User } from '@shared/models/user';
import { PermissionsModel } from '@shared/models/permissions.model';
import { takeUntil } from 'rxjs/operators';
import * as store from '@shared/store';

export abstract class BaseSandbox {
  protected _endSubscriptions$: Subject<boolean> = new Subject();

  public culture$: Observable<string> = this.appState$.select(store.getSelectedCulture);
  private _permissions$: Observable<PermissionsModel> = this.appState$
    .select(store.getUser)
    .pipe(takeUntil(this._endSubscriptions$))
    .map((user: User) => new PermissionsModel(user.roles));
  public culture: string;
  public permissions: PermissionsModel;
  constructor(protected appState$: Store<store.State>) {
    this._permissions$.subscribe(permissions => (this.permissions = permissions));
  }

  public endSubscriptions(): void {
    this._endSubscriptions$.next(true);
  }

  /**
   * Formats date string based on selected culture
   *
   * @param value
   */
  public formatDate(value: string): string {
    return localeDateString(value, this.culture);
  }

  /**
   * Clears all data from local storage set at by
   * code during application rutime (user session)
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @memberof BaseSandbox
   */
  public clearStorage(): void {
    localStorage.clear();
  }

  /**
   * Removes user form redux store
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @memberof BaseSandbox
   */
  public removeUser(): void {
    this.appState$.dispatch(settingsActions.setUser(null));
  }
}
