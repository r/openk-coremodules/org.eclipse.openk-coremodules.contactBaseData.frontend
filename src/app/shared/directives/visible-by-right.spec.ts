/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { VisibleByRightDirective } from '@shared/directives/visible-by-right';
import { async } from '@angular/core/testing';
import { of } from 'rxjs/observable/of';

describe('VisibleByRightDirective', () => {
  let templateRef: any;
  let viewContainerRef: any;
  let appState: any;
  beforeEach(async(() => {
    viewContainerRef = {
      createEmbeddedView: () => {},
      clear: () => {},
      element: { nativeElement: { elements: [{ classList: {}, disabled: false }] } },
    };

    appState = {
      pipe: () => of(),
      dispatch: () => {},
      select: () => of({ roles: ['kon-reader'] }),
      map: () => of({ reader: true }),
    };
  }));

  it('should create an instance', () => {
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);
    expect(directive).toBeTruthy();
  });

  it('should create embedded view for admin role', () => {
    appState = {
      pipe: () => of(),
      dispatch: () => {},
      select: () => of({ roles: ['kon-admin'] }),
      map: () => of({ reader: true }),
    };
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);

    const spy = spyOn(directive['_viewContainer'], 'createEmbeddedView' as any);
    directive.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should create embedded view for other role than admin with an substitutional template', () => {
    appState = {
      pipe: () => of(),
      dispatch: () => {},
      select: () => of({ roles: ['kon-reader'] }),
      map: () => of({ reader: true }),
    };
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);
    const spy = spyOn(directive['_viewContainer'], 'createEmbeddedView' as any);
    directive.elseTemplate = {} as any;
    directive.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should create embedded view for other role than admin with an substitutional template', () => {
    appState = {
      pipe: () => of(),
      dispatch: () => {},
      select: () => of({ roles: ['kon-reader'] }),
      map: () => of({ reader: true }),
    };
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);
    const spy = spyOn(directive['_viewContainer'], 'clear' as any);
    directive.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });
});
