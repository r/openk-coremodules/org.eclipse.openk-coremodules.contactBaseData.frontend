/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Directive, ViewContainerRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as store from '@shared/store';
import { Observable, Subject } from 'rxjs';
import { User } from '@shared/models/user';
import { PermissionsModel } from '@shared/models/permissions.model';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: 'form',
})
export class FormDisableDirective implements AfterViewInit, OnDestroy {
  private _endSubscriptions$: Subject<boolean> = new Subject();
  private _permissions$: Observable<PermissionsModel> = this._appState$
    .select(store.getUser)
    .pipe(takeUntil(this._endSubscriptions$))
    .map((user: User) => {
      return new PermissionsModel(user.roles);
    });

  constructor(private _viewContainer: ViewContainerRef, private _appState$: Store<store.State>) {}

  ngAfterViewInit() {
    this._permissions$.subscribe(permissions => {
      this._traverseDOM(this._viewContainer.element.nativeElement.elements, permissions);
    });
  }
  ngOnDestroy() {
    this._endSubscriptions$.next(true);
  }
  private _traverseDOM(elements: any[], permissions: PermissionsModel): void {
    for (const element of elements) {
      const isCancelButton = !!Object.keys(element.classList || {}).find(
        item => element.classList[item] === 'cancel-button' || element.classList[item] === 'btn-sm'
      );
      element.disabled = element.disabled || (!isCancelButton && permissions.reader);

      this._traverseDOM(element.childNodes, permissions);
    }
  }
}
