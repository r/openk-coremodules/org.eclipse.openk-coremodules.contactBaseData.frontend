/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { FormDisableDirective } from '@shared/directives/form-disable.directive';
import { async } from '@angular/core/testing';
import { of } from 'rxjs/observable/of';

describe('FormDisableDirective', () => {
  let viewContainerRef: any;
  let appState: any;
  beforeEach(async(() => {
    viewContainerRef = {
      createEmbeddedView: () => {},
      clear: () => {},
      element: { nativeElement: { elements: [{ classList: {}, disabled: false, childNodes: [] }] } },
    };

    appState = {
      pipe: () => of(),
      dispatch: () => {},
      select: () => of({ roles: ['kon-reader'] }),
      map: () => of({ reader: true }),
    };
  }));

  it('should create an instance', () => {
    const directive = new FormDisableDirective(viewContainerRef as any, appState as any);
    expect(directive).toBeTruthy();
  });

  it('should traverse a DOM', () => {
    const directive = new FormDisableDirective(viewContainerRef as any, appState as any);
    const spy = spyOn(directive, '_traverseDOM' as any);
    directive.ngAfterViewInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should disable form element', () => {
    const directive = new FormDisableDirective(viewContainerRef as any, appState as any);
    directive.ngAfterViewInit();
    expect(viewContainerRef.element.nativeElement.elements[0].disabled).toBe(true);
  });
});
