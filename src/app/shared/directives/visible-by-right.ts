/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Directive, ViewContainerRef, OnInit, TemplateRef, Input, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as store from '@shared/store';
import { Observable, Subject } from 'rxjs';
import { User } from '@shared/models/user';
import { PermissionsModel } from '@shared/models/permissions.model';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[visibleByRight]',
})
export class VisibleByRightDirective implements OnInit, OnDestroy {
  @Input() elseTemplate: TemplateRef<any>;
  @Input() onlyForAdmin: boolean = false;
  private _endSubscriptions$: Subject<boolean> = new Subject();
  private _permissions$: Observable<PermissionsModel> = this._appState$
    .select(store.getUser)
    .pipe(takeUntil(this._endSubscriptions$))
    .map((user: User) => new PermissionsModel(user.roles));

  constructor(private _templateRef: TemplateRef<any>, private _viewContainer: ViewContainerRef, private _appState$: Store<store.State>) {}

  ngOnInit() {
    this._permissions$.subscribe(permissions => {
      if (permissions.admin || (!this.onlyForAdmin && permissions.writer)) {
        this._viewContainer.createEmbeddedView(this._templateRef);
      } else if (!!this.elseTemplate) {
        this._viewContainer.createEmbeddedView(this.elseTemplate);
      } else {
        this._viewContainer.clear();
      }
    });
  }

  ngOnDestroy() {
    this._endSubscriptions$.next(true);
  }
}
