import { EventEmitter, OnDestroy } from '@angular/core';
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { PaginatorComponent } from '@shared/components/paginator/paginator.component';
import { PageEvent } from '@shared/models/PageEvent';
import { PageModel } from '@shared/models/page/page.model';
import { AgGridAngular } from 'ag-grid-angular';
import { take, map, takeUntil } from 'rxjs/operators';
import { ServerSideModel } from '@shared/models/server-side.model';
import { Directive, Input, ComponentFactoryResolver, ViewContainerRef, ComponentFactory, OnInit, Output } from '@angular/core';
import { Store, ActionsSubject } from '@ngrx/store';

import * as store from '@shared/store';
import { PageRequestInterface } from '@shared/models/page/page-request.interface';
import { ofType } from '@ngrx/effects';
import { Subject } from 'rxjs';

/**
 * Realises serverside paging
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @class ServerSideDirective
 * @implements {OnInit}
 */
@Directive({
  selector: 'ag-grid-angular[serverside]',
  host: {},
})
export class ServerSideDirective implements OnInit, OnDestroy {
  private _currentPageNumber: number;
  private _queryParameter: any;
  public endOfSubscribtion = new Subject<boolean>();

  private _matPaginator: PaginatorComponent;

  @Output()
  public currentPageNumber: EventEmitter<number> = new EventEmitter();

  @Input()
  public serverside: ServerSideModel;

  @Input()
  public set queryParameter(query: any) {
    this._currentPageNumber = 1;
    this.appState$.dispatch(
      this.serverside.loadAction({
        payload: {
          queryParameter: query,
          pageSize: this.serverside.pageSize,
        } as PageRequestInterface,
      })
    );

    this._queryParameter = query;
  }

  /**
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @param {Store<store.State>} appState$
   * @param {ComponentFactoryResolver} _resolver
   * @param {ViewContainerRef} _viewContainerRef
   * @param {ActionsSubject} _actionsSubject
   * @param {AgGridAngular} _agGrid
   * @memberof ServerSideDirective
   */
  constructor(
    protected appState$: Store<store.State>,
    private _resolver: ComponentFactoryResolver,
    private _viewContainerRef: ViewContainerRef,
    private _actionsSubject: ActionsSubject,
    private _agGrid: AgGridAngular
  ) {}

  ngOnDestroy(): void {
    this.endOfSubscribtion.next(true);
  }

  /**
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @memberof ServerSideDirective
   */
  ngOnInit() {
    this._currentPageNumber = this.serverside.pageNumber;
    this._retrievePage({ pageIndex: this.serverside.pageNumber });
    this._actionsSubject
      .pipe(
        ofType(this.serverside.successAction.type),
        map(item => item.payload),
        takeUntil(this.endOfSubscribtion)
      )
      .subscribe((pagedItem: PageModel<any>) => {
        if (!this._matPaginator) {
          const paginator: ComponentFactory<PaginatorComponent> = this._resolver.resolveComponentFactory(PaginatorComponent);
          this._matPaginator = this._viewContainerRef.createComponent(paginator).instance;
        }
        if (this._matPaginator.totalPages !== pagedItem.totalPages) {
          this._matPaginator.totalPages = pagedItem.totalPages;
        }
        this._matPaginator.currentPageNumber = this._currentPageNumber;
        this._matPaginator.length = pagedItem.totalElements;
        this._matPaginator.pageSize = pagedItem.pageable.pageSize || 1;
        this._matPaginator.hidePageSize = false;
        this._matPaginator.page.pipe(take(1)).subscribe((selectedPage: PageEvent) => {
          this._retrievePage(selectedPage);
        });
        if (!!this._agGrid.api) {
          this._agGrid.api.setRowData(pagedItem.content);
        } else {
          this._agGrid.rowData = pagedItem.content;
        }
      });
  }

  /**
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @private
   * @param {PageEvent} [event]
   * @memberof ServerSideDirective
   */
  private _retrievePage(event?: PageEvent) {
    this._currentPageNumber = event.pageIndex;
    this.currentPageNumber.emit(event.pageIndex);
    this.appState$.dispatch(
      this.serverside.loadAction({
        payload: {
          pageNumber: (event && event.pageIndex) || 0,
          pageSize: this.serverside.pageSize,
          queryParameter: this._queryParameter,
        } as PageRequestInterface,
      })
    );
  }
}
