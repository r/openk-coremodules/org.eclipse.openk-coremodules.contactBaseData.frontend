/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Directive, OnInit, Input, OnDestroy } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
@Directive({
  selector: '[columnDefs]',
})
export class TranslateColumnDefinitionsDirective implements OnInit, OnDestroy {
  private _languageChangeSubscription: Subscription;

  @Input()
  public columnDefs: any;

  constructor(public agGrid: AgGridAngular, public _translationService: TranslateService) {}
  ngOnInit() {
    this.agGrid.columnDefs = this._getTranslatedColumnDefinitions();
    this._languageChangeSubscription = this._translationService.onLangChange.subscribe(() => {
      const coldefs: any = this._getTranslatedColumnDefinitions();
      this.agGrid.api.setColumnDefs(coldefs);
    });
  }

  public ngOnDestroy() {
    this._languageChangeSubscription.unsubscribe();
  }

  private _getTranslatedColumnDefinitions(): any {
    return this.columnDefs.map((columnDefinition: any) => {
      return {
        ...columnDefinition,
        headerName: this._translationService.instant(columnDefinition.headerName),
      };
    });
  }
}
