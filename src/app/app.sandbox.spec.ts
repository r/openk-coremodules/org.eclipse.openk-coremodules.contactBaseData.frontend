/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { TestBed } from '@angular/core/testing';
import { User } from '@shared/models/user';
import { of } from 'rxjs';
import { AppSandbox } from './app.sandbox';
import { ConfigService } from './app-config.service';
import { TranslateService } from '@ngx-translate/core';

let configServiceStub = {
  get: a => {
    switch (a) {
      case 'notifications':
        return {
          options: {
            global: '123',
          },
        };

      case 'localization':
        return {
          languages: [{ code: 'de', name: 'DE', culture: 'de-DE' }],
          defaultLanguage: 'de',
        };

      default:
        return {};
    }
  },
  map: () => ({}),
};

describe('AppSandbox', () => {
  let component: AppSandbox;
  let appState;
  let translateService: TranslateService;

  TestBed.configureTestingModule({
    providers: [{ provide: ConfigService, useValue: configServiceStub }],
  });

  beforeEach(() => {
    appState = {
      pipe: () => of(1),
      dispatch: () => {},
      select: () => of(1),
    };
    translateService = {
      addLangs() {},
      setDefaultLang() {},
      use() {},
    } as any;

    component = new AppSandbox(appState, translateService as any, configServiceStub as any);
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should call setupLanguage()', () => {
    spyOn(configServiceStub, 'get')
      .withArgs('localization')
      .and.callThrough();
    const spy = spyOn(appState, 'dispatch');
    component.setupLanguage();
    expect(spy).toHaveBeenCalled();
  });

  it('should call getNotificationOptions()', () => {
    spyOn(configServiceStub, 'get')
      .withArgs('notifications')
      .and.callThrough();
    component.getNotificationOptions();
    expect(component.getNotificationOptions()).toEqual({ global: '123' });
  });

  it('should call setUser() with given user', () => {
    const spy = spyOn(appState, 'dispatch');
    const user: User = new User();
    component.setUser(user);
    expect(spy).toHaveBeenCalledWith({ payload: user, type: '[Settings] SetUser' });
  });

  it('should call setUser() without given user', () => {
    const spy = spyOn(appState, 'dispatch');
    component.setUser();
    expect(spy).toHaveBeenCalled();
  });
});
