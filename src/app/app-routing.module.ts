/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '@shared/components/pageNotFound/pageNotFound.component';
import { LogoutPageComponent } from '@pages/logout/logout/logout.component';
import { LoggedoutPageComponent } from '@pages/logout/loggedout/loggedout.component';
import { Globals } from './shared/constants/globals';

const PATH = Globals.PATH;
const appRoutes: Routes = [
  { path: '', redirectTo: '/overview', pathMatch: 'full' },
  {
    path: PATH.COMPANY,
    loadChildren: './pages/company/company.module#CompanyModule',
  },
  {
    path: 'personsexternal',
    loadChildren: './pages/persons/persons.module#PersonsModule',
  },
  {
    path: 'personsinternal',
    loadChildren: './pages/persons/persons.module#PersonsModule',
  },
  {
    path: 'admin',
    loadChildren: './pages/admin/admin.module#AdminModule',
  },
  {
    path: 'logout',
    component: LogoutPageComponent,
  },
  {
    path: 'loggedout',
    component: LoggedoutPageComponent,
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
