 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, RequestOptions } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common'; 

@Injectable()
export class ConfigService {
  private config: Object;
  private env: Object;  

  constructor(private http: Http, 
    @Inject(APP_BASE_HREF) private baseHref: string) {}

  /**
   * Loads the environment config file first. Reads the environment variable from the file
   * and based on that loads the appropriate configuration file - development or production
   */
  load() {    
    return new Promise((resolve, reject) => {
      const headers = new Headers({
        Accept: 'application/json',
        'Content-Type': 'application/json',
        DataType: 'application/json'
      });

      this.http
        .get(this.baseHref + 'config/env.json')
        .map(res => res.json())
        .subscribe(env_data => {
          this.env = env_data;

          this.http
            .get(this.baseHref + 'config/' + env_data.env + '.json')
            .map(res => res.json())
            .catch((error: any) => {
              return Observable.throw(error.json().error || 'Server error');
            })
            .subscribe(data => {
              this.config = data;
              resolve(true);
            });
        });
    });
  }

  /**
   * Returns environment variable based on given key
   *
   * @param key
   */
  getEnv(key: any) {
    return this.env[key];
  }

  /**
   * Returns configuration value based on given key
   *
   * @param key
   */
  get(key: any) {
    return this.config[key];
  }
}
