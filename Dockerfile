 #  /********************************************************************************
 #  * Copyright (c) 2020 Contributors to the Eclipse Foundation
 #  *
 #  * See the NOTICE file(s) distributed with this work for additional
 #  * information regarding copyright ownership.
 #  *
 #  * This program and the accompanying materials are made available under the
 #  * terms of the Eclipse Public License v. 2.0 which is available at
 #  * http://www.eclipse.org/legal/epl-2.0.
 #  *
 #  * SPDX-License-Identifier: EPL-2.0
 #  ********************************************************************************/

 # NODEJS BASE IMAGE
FROM node:10.16.3 as build

# set working directory
WORKDIR /app

# ADD `/app/node_modules/.bin` TO $PATH
ENV PATH /app/node_modules/.bin:$PATH

# SET NPM PROXY
RUN npm config set proxy http://webproxy3.pta.de:8080
RUN npm config set https-proxy http://webproxy3.pta.de:8080

# INSTALL AND CACHE APP DEPENDENCIES
COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@7.3.9

# ADD APP
COPY . /app

# EXPOSE PORT 4200
EXPOSE 4200

# RUN APPLICATION
CMD ["npm", "run", "start-in-docker"]
